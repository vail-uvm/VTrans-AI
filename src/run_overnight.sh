#!/usr/bin/env bash

# A temporary script used for running multiple consecutive experiments unattended (i.e. overnight)

source ~/anaconda3/etc/profile.d/conda.sh
#conda activate vtrans_ai_36
conda activate base
#bash tracker/track_detections.sh
#bash tracker/tracker_evaluate.sh
#shutdown
#pm-suspend
#pm-hibernate

#python_path="/home/vision/anaconda3/bin/python"

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

python tracker/pure_image_similarity_metric.py -d=/home/vision/Documents/no_sign_images -e=3 -m=IMAGE_COS -b=2
#for segment in data/rawdata/*/*_detections.csv
#do
#  python tracker/track.py $segment --tracker_type=ARTS_Image -c=.9
#done
pm-suspend
