import os
import errno
from PIL import Image
from six import raise_from
from lxml import etree as ET

import _register
__methods__ = []
register_method = _register.register_method(__methods__)


@register_method
def __get_element(self, parent, child, cast=None):
    """ Get a given tag from an XML file """

    element = parent.find(child)

    if element is None:
        raise ValueError('--Error: No such tag \'{}\''.format(child))

    if cast is not None:
        try:
            return cast(element.text)
        except ValueError as e:
            raise_from(ValueError(
                '--Error: illegal value for \'{}\': {}'.format(child, e)), None)

    return element


@register_method
def __parse_sign(self, obj, class_tag='name'):
    """ Parse all metadata from a given xml object """
    sign = {}
    location = self.__get_element(obj, 'location')
    bndbox = self.__get_element(obj, 'bndbox')
    att = self.__get_element(obj, 'attributes')

    sign['name'] = self.__get_element(obj, class_tag, cast=str)
    sign['truncated'] = self.__get_element(obj, 'truncated', cast=int)
    sign['difficult'] = self.__get_element(obj, 'difficult', cast=int)
    sign['latitude'] = self.__get_element(location, 'latitude', cast=float)
    sign['longitude'] = self.__get_element(location, 'longitude', cast=float)
    sign['xmin'] = round(self.__get_element(bndbox, 'xmin', cast=int))
    sign['ymin'] = round(self.__get_element(bndbox, 'ymin', cast=int))
    sign['xmax'] = round(self.__get_element(bndbox, 'xmax', cast=int))
    sign['ymax'] = round(self.__get_element(bndbox, 'ymax', cast=int))
    sign['ymax'] = round(self.__get_element(bndbox, 'ymax', cast=int))
    sign['assembly'] = self.__get_element(att, 'assembly', cast=str)
    sign['side'] = self.__get_element(att, 'side', cast=str)

    sign['id'] = '{name}_{lat}_{lon}'.format(
        name=sign['name'],
        lat=sign['latitude'],
        lon=sign['longitude']
    )

    return sign


@register_method
def __parse_xml(self, root, class_tag='name'):
    """ Parse all metadata from a given xml file """

    xml_tree = {}
    try:
        img_size = self.__get_element(root, 'size')
        location = self.__get_element(root, 'Location')
        xml_tree['folder'] = self.__get_element(root, 'folder', cast=str)
        xml_tree['filename'] = self.__get_element(root, 'filename', cast=str)
        try:
            xml_tree['annotated'] = self.__get_element(root, 'annotated', cast=str)
        except ValueError:
            xml_tree['annotated'] = 'True'
        xml_tree['width'] = self.__get_element(img_size, 'width', cast=int)
        xml_tree['height'] = self.__get_element(img_size, 'height', cast=int)
        xml_tree['depth'] = self.__get_element(img_size, 'depth', cast=int)
        xml_tree['Latitude'] = self.__get_element(location, 'Latitude', cast=float)
        xml_tree['Longitude'] = self.__get_element(location, 'Longitude', cast=float)
        xml_tree['Altitude'] = self.__get_element(location, 'Altitude', cast=float)
        xml_tree['signs'] = []
        for i, obj in enumerate(root.iter('object')):
            sign = self.__parse_sign(obj, class_tag)

            # check if bndbox is out of boundary
            sign['xmin'] = 0 if sign['xmin'] < 0 else sign['xmin']
            sign['ymin'] = 0 if sign['ymin'] < 0 else sign['ymin']
            sign['xmax'] = xml_tree['width'] if sign['xmax'] > xml_tree['width'] else sign['xmax']
            sign['ymax'] = xml_tree['height'] if sign['ymax'] > xml_tree['height'] else sign['ymax']

            # skip too blury (too small) signs
            h_sign = int(sign['ymax'] - sign['ymin'])
            w_sign = int(sign['xmax'] - sign['xmin'])
            if self.blur_threshold > 0 and (w_sign < self.blur_threshold or h_sign < self.blur_threshold):
                continue

            # skip sign if it's too far away
            dist = self.__calc_distance(
                cam_gps=[xml_tree['Latitude'], xml_tree['Longitude']],
                obj_gps=[sign['latitude'], sign['longitude']]
            )
            if self.dist_threshold > -1 and dist > self.dist_threshold:
                continue

            # drop out annotations with unknown sign-types
            if self.drop_unknown and str(sign['name']).upper() == 'UNKNOWN':
                continue

            xml_tree['signs'].append(sign)

        return xml_tree

    # flag corrupted xml file to replace it with a new one
    except ValueError as e:
        raise e
        # return {}


@register_method
def __create_xml(self, root_dir, xml_path, annotated=True):
    """ Create a generic xml file for a corresponding image """

    dir_path = os.path.dirname(xml_path)
    fname, ext = os.path.splitext(os.path.basename(xml_path))

    # get corresponding image path
    img_path = os.path.join(
        dir_path.replace(self.annotations_path, self.images_path),
        fname + self.image_extension
    )

    # load image in memory and extract its metadata
    img = Image.open(img_path)
    lat, lon, alt = self.__extract_cam_gps(img_path)
    cam_dom = self.__extract_cam_dom(img_path)

    # define xml tree structure
    root = ET.Element('annotation')
    ET.SubElement(root, 'folder').text = os.path.dirname(os.path.relpath(xml_path, root_dir))
    ET.SubElement(root, 'filename').text = fname+self.image_extension
    ET.SubElement(root, 'annotated').text = str(annotated)
    size = ET.SubElement(root, 'size')
    ET.SubElement(size, 'width').text = str(img.width)
    ET.SubElement(size, 'height').text = str(img.height)
    ET.SubElement(size, 'depth').text = str(3)
    gps = ET.SubElement(root, 'Location')
    ET.SubElement(gps, 'Latitude').text = str(lat)
    ET.SubElement(gps, 'Longitude').text = str(lon)
    ET.SubElement(gps, 'Altitude').text = str(alt)
    ET.SubElement(gps, 'Cam_DOM').text = str(cam_dom)

    tree = ET.ElementTree(root)

    if not os.path.exists(dir_path):
        try:
            os.makedirs(dir_path)
        except OSError as exc:  # to avoid race condition
            if exc.errno != errno.EEXIST:
                raise

    tree.write(xml_path, pretty_print=True)


@register_method
def __fill_xml(self, root_dir, file_path, xml_map):
    """ Fill a given xml file with predefined xml-map """

    # define a new xml tree structure
    fname, ext = os.path.splitext(os.path.basename(file_path))

    # get the camera DOM
    dir_path = os.path.dirname(file_path)
    img_path = os.path.join(
        dir_path.replace(self.annotations_path, self.images_path),
        fname + self.image_extension
    )
    cam_dom = self.__extract_cam_dom(img_path)

    root = ET.Element('annotation')
    ET.SubElement(root, 'folder').text = os.path.dirname(os.path.relpath(file_path, root_dir))
    ET.SubElement(root, 'filename').text = str(fname+self.image_extension)
    ET.SubElement(root, 'annotated').text = str(xml_map['annotated'])
    size = ET.SubElement(root, 'size')
    ET.SubElement(size, 'width').text = str(xml_map['width'])
    ET.SubElement(size, 'height').text = str(xml_map['height'])
    ET.SubElement(size, 'depth').text = str(xml_map['depth'])
    car_gps = ET.SubElement(root, 'Location')
    ET.SubElement(car_gps, 'Latitude').text = str(xml_map['Latitude'])
    ET.SubElement(car_gps, 'Longitude').text = str(xml_map['Longitude'])
    ET.SubElement(car_gps, 'Altitude').text = str(xml_map['Altitude'])
    ET.SubElement(car_gps, 'Cam_DOM').text = str(cam_dom)

    # verify that each sign has a unique ID
    xml_map['signs'] = self.__uniquify_ids(xml_map['signs'])

    # build xml tree
    for sign in xml_map['signs']:
        obj = ET.SubElement(root, 'object')
        ET.SubElement(obj, 'id').text = str(sign['id']).upper()
        ET.SubElement(obj, 'name').text = str(sign['name']).upper()
        ET.SubElement(obj, 'truncated').text = str(sign['truncated'])
        ET.SubElement(obj, 'difficult').text = str(sign['difficult'])
        gps = ET.SubElement(obj, 'location')
        ET.SubElement(gps, 'latitude').text = str(sign['latitude'])
        ET.SubElement(gps, 'longitude').text = str(sign['longitude'])
        box = ET.SubElement(obj, 'bndbox')
        ET.SubElement(box, 'xmin').text = str(sign['xmin'])
        ET.SubElement(box, 'ymin').text = str(sign['ymin'])
        ET.SubElement(box, 'xmax').text = str(sign['xmax'])
        ET.SubElement(box, 'ymax').text = str(sign['ymax'])
        att = ET.SubElement(obj, 'attributes')
        ET.SubElement(att, 'assembly').text = str(sign['assembly'])
        ET.SubElement(att, 'side').text = str(sign['side'])

    tree = ET.ElementTree(root)
    tree.write(file_path, pretty_print=True)
