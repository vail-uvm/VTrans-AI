import os
import csv
import errno
from PIL import Image
from math import sin, cos, sqrt, atan2, radians

import _register
__methods__ = []
register_method = _register.register_method(__methods__)


@register_method
def __convert_to_cam_dom(self, dom_values):
    """Returns the camera direction of movement in degrees using the "GPS Tack" from the EXIF data."""

    return dom_values[0] / dom_values[1]


@register_method
def __convert_to_degrees(self, gps_value):
    """ Convert the GPS coordinates stored in the EXIF to degrees in float format """

    d = float(gps_value[0][0]) / float(gps_value[0][1])
    m = float(gps_value[1][0]) / float(gps_value[1][1])
    s = float(gps_value[2][0]) / float(gps_value[2][1])
    return d + (m / 60.0) + (s / 3600.0)


@register_method
def __calc_distance(self, cam_gps, obj_gps):
    """ Calculate the distance between two GPS coordinates """

    # approximate radius of earth in meters
    R = 6371000.0

    lat1 = radians(cam_gps[0])
    lon1 = radians(cam_gps[1])
    lat2 = radians(obj_gps[0])
    lon2 = radians(obj_gps[1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    return R*c


@register_method
def __store_dict(self, hash_tbl, path_to_store, with_value=True):
    """ Write a dictionary into a text/csv file """

    with open(path_to_store, 'w') as log:
        if self.log_extension == '.txt':
            writer = csv.writer(log, delimiter=' ')
        elif self.log_extension == '.csv':
            writer = csv.writer(log, delimiter=',')
        else:
            print('--Error: Unsupported file type ', self.log_extension)

        if with_value:
            for k, v in hash_tbl.items():
                writer.writerow([k, v])
        else:
            for k, v in hash_tbl.items():
                writer.writerow([k])

    print('--Message: Updated', path_to_store)


@register_method
def __load_dict(self, path_to_labels, with_value=True):
    """ Load labels from a text/csv file into a dictionary """

    # pull in each row as a key-value pair
    with open(path_to_labels, mode='r') as log:
        if self.log_extension == '.txt':
            reader = csv.reader(log, delimiter=' ')
        elif self.log_extension == '.csv':
            reader = csv.reader(log, delimiter=',')
        else:
            print('--Error: Unsupported file type ', self.log_extension)

        print('--Message: Loaded', path_to_labels)

        if with_value:
            return {l[0]: l[1] for l in reader}
        else:
            return {k[0]: i for i, k in enumerate(reader)}

    
@register_method
def __crop_thumbnail(self, dir_path, image_path, sign):
    """ Take a snapshot of a target sign from a given image and save it to desk """

    img = Image.open(image_path)
    thumbnail = img.crop(
        (sign['xmin'], sign['ymin'], sign['xmax'], sign['ymax']))

    path_to_store = os.path.join(
        self.logs_path, 'Thumbnails', sign['name']
    )

    # create a new folder to store thumbnails categorized by their class name
    if not os.path.exists(path_to_store):
        try:
            os.makedirs(path_to_store)
            print('--Message: Saved ', path_to_store)
        except OSError as exc:  # to avoid race condition
            if exc.errno != errno.EEXIST:
                raise

    # save to desk
    thumbnail.save(
        os.path.join(
            path_to_store,
            '{pt}_{dir}_{name}'.format(
                pt=int(abs(sign['xmin'] - sign['ymin'])),
                dir=dir_path.replace('/', '_'),
                name=os.path.basename(image_path)
            )
        )
    )
