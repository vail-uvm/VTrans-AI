
import sys
import os
import argparse
from _manager import DataManager
sys.path.append("../..")


def parse_args():
    parser = argparse.ArgumentParser(
        description='ARTS - Automotive Repository of Traffic Signs API',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    subparsers = parser.add_subparsers(
        help='Arguments for specific util codename.', dest='util')
    subparsers.required = True

    '''
        Subparsers
        [     
            pascal, (more dataset-parsers may be added later ie. yolo, coco, etc...)
            test, 
        ]
    '''

    # PASCAL-VOC Parser
    # --------------------------------------------------
    pascal_parser = subparsers.add_parser(
        'pascal', help='Argument for PASCAL VOC dataset utils.')

    # Required args
    pascal_parser.add_argument(
        'dataset_path', help='path to dataset directory (ie. /tmp/VOCdevkit).')

    # Optional args

    # automatic configurations
    pascal_parser.add_argument(
        '--easy',
        dest='easy',
        action='store_true',
        default=False,
        help='ARTS(EASY) configuration.',
    )
    
    pascal_parser.add_argument(
        '--challenging',
        dest='challenging',
        action='store_true',
        default=False,
        help='ARTS(CHALLENGING) configuration.',
    )

    pascal_parser.add_argument(
        '--challenging_tracker',
        dest='challenging_tracker',
        action='store_true',
        default=False,
        help='ARTS(CHALLENGING) + tracker configuration.',
    )

    pascal_parser.add_argument(
        '--video-logs',
        dest='video_logs',
        action='store_true',
        default=False,
        help='ARTS(VIDEO-LOGS) configuration.',
    )

    pascal_parser.add_argument(
        '--vision',
        dest='vision',
        action='store_true',
        default=False,
        help='ARTS(VISION) configuration.',
    )
        
    # manual configurations
    pascal_parser.add_argument(
        '--prefix',
        dest='prefix',
        type=str,
        default=None,
        help='Prefix for all files/folders with given prefix.',
    )

    pascal_parser.add_argument(
        '--arrange',
        dest='arrange',
        action='store_true',
        default=False,
        help='Move images to /JPEGImages folder and move .xml files to /Annotations folder.',
    )

    pascal_parser.add_argument(
        '--unpack',
        dest='unpack',
        action='store_true',
        default=False,
        help='Unpack all subdirectories in /JPEGImages and /Annotations recursively into one master directory.',
    )

    pascal_parser.add_argument(
        '--map-labels',
        dest='map',
        type=int,
        default=0,
        help='Create a lookup table by combining labels(signs) based on their types \
            ([0]-MUTCD encoding, [1]-superclass encoding, [2]-subclass encoding).',
    )

    pascal_parser.add_argument(
        '--mark_non_annotated',
        dest='mark_na',
        action='store_true',
        default=False,
        help='Detect directories exclusively containing images without annotations, and automatically generate special '
             'XML files which serve as markers that those images don\'t have annotations.',
    )

    pascal_parser.add_argument(
        '--split',
        dest='split',
        action='store_true',
        default=False,
        help='Create log files for training and validation subsets in /ImageSets directory.',
    )

    pascal_parser.add_argument(
        '--extract-labels',
        dest='extract',
        action='store_true',
        default=False,
        help='Scan all XML files and collect all labels(signs) found in a dict.',
    )

    pascal_parser.add_argument(
        '--no-noise',
        dest='noise',
        action='store_true',
        default=False,
        help='Remove all images that do not have signs.',
    )

    pascal_parser.add_argument(
        '--combine-unknown',
        dest='combine_unknown',
        action='store_true',
        default=False,
        help='Combine unkown sign-types by their superclass.',
    )

    pascal_parser.add_argument(
        '--drop-unknown',
        dest='drop_unknown',
        action='store_true',
        default=False,
        help='Drop out annotations with unknown sign-types.',
    )

    pascal_parser.add_argument(
        '--labels',
        dest='labels',
        metavar='relpath',
        type=str,
        default=None,
        help='Enter relative path to load as predefined labels(signs).',
    )

    pascal_parser.add_argument(
        '--lookup-dict',
        dest='lookups',
        metavar='relpath',
        type=str,
        default=None,
        help='Enter relative path to load as predefined labels(signs).',
    )

    pascal_parser.add_argument(
        '--pixels-threshold',
        dest='pixels',
        metavar='number-of-pixels',
        type=int,
        default=0,
        help='Enter threshold to filter out signs that are too blurry (too small).',
    )

    pascal_parser.add_argument(
        '--freq-threshold',
        dest='freq',
        metavar='minimum-frequency',
        type=int,
        default=1,
        help='Enter threshold to combine labels(signs) based on their frequencies.',
    )

    pascal_parser.add_argument(
        '--dist-threshold',
        dest='dist',
        metavar='minimum-distance',
        type=int,
        default=-1,
        help='Enter threshold to eliminate labels(signs) based on their distances.',
    )

    pascal_parser.add_argument(
        '--extension',
        dest='ext',
        metavar='.xxx',
        type=str,
        default='.txt',
        help='Specify the file format for log files.',
    )

    pascal_parser.add_argument(
        '--seed',
        dest='qseed',
        metavar='integer',
        type=int,
        default=2018,
        help='Specify a seed for splitting the data (enter [-1] for a random seed).',
    )

    pascal_parser.add_argument(
        '--test-ratio',
        dest='test_ratio',
        metavar='float',
        type=float,
        default=.5,
        help='Specify a ratio for splitting the data into training and testing subsets.',
    )

    pascal_parser.add_argument(
        '--val-ratio',
        dest='val_ratio',
        metavar='float',
        type=float,
        default=.5,
        help='Specify a ratio for splitting the test samples into validation and testing subsets.',
    )

    # --------------------------------------------------

    # Test Parser
    # --------------------------------------------------
    test_parser = subparsers.add_parser(
        'test',
        help='Argument for dataset unit tests.',
    )

    # Required args
    test_parser.add_argument(
        'dataset_path',
        help='Path to dataset directory (ie. /tmp/VOCdevkit).',
    )

    # Optional args
    test_parser.add_argument(
        '--clean',
        dest='clean',
        action='store_true',
        default=False,
        help='Filter out any unnecessary metadata from the xml files.',
    )

    test_parser.add_argument(
        '--stats',
        dest='stats',
        type=int,
        default=None,
        help='Collect some statistical analysis about the dataset. \
            ([1]-Raw-MUTCD, [2]-MUTCD-Pascal).',
    )

    test_parser.add_argument(
        '--duplicates',
        dest='duplicates',
        action='store_true',
        default=False,
        help='Flag any duplicated files in the dataset.',
    )

    test_parser.add_argument(
        '--tags',
        dest='tags',
        action='store_true',
        default=False,
        help='Flag any XML files that have corrupted tags.',
    )

    test_parser.add_argument(
        '--gps-seq',
        dest='gps_seq',
        action='store_true',
        default=False,
        help='Flag XML files for any mislabeled GPS information in a sequence of images.',
    )

    test_parser.add_argument(
        '--sign-seq',
        dest='sign_seq',
        action='store_true',
        default=False,
        help='Re-arrange data into a sequance of frames for every unique sign in the dataset.',
    )

    test_parser.add_argument(
        '--thumbnails',
        dest='thumbnails',
        action='store_true',
        default=False,
        help='Capture thumbnails of all labels(signs) found in the dataset.',
    )

    test_parser.add_argument(
        '--ids',
        dest='ids',
        action='store_true',
        default=False,
        help='Verify that each sign has a unique ID to track it down in a sequence of frames when needed.',
    )

    test_parser.add_argument(
        '--foi',
        dest='foi',
        action='store_true',
        default=False,
        help='Collect frames of interest of all unique signs in the dataset.',
    )

    test_parser.add_argument(
        '--soi',
        dest='soi',
        action='store_true',
        default=False,
        help='Collect sign\'s info of all unique signs in the dataset.',
    )

    # --------------------------------------------------

    # CSV Parser
    # --------------------------------------------------
    csv_parser = subparsers.add_parser(
        'csv',
        help='Exposes utilities to generate CSV annotation files.',
    )

    # Required args
    csv_parser.add_argument(
        'dataset_path',
        help='Path to dataset directory (ie. /ARTS-Dataset/rawdata/12).',
    )

    csv_parser.add_argument(
        '--output_path',
        default=None,
        help='Directory where generated outputs will be placed (ie. /ARTS-Dataset/rawdata).',
    )

    return parser.parse_args()


def main():
    data_parser = DataManager()
    main_dir = os.getcwd()

    # parse arguments
    args = parse_args()

    if args.util == 'pascal':
        data_dir = os.path.join(main_dir, args.dataset_path)

        if args.extract:
            data_parser.set_extract_flag(args.extract)

        if args.noise:
            data_parser.set_noise_flag(args.noise)

        if args.combine_unknown:
            data_parser.set_combine_unknown_flag(args.combine_unknown)

        if args.drop_unknown:
            data_parser.set_drop_unknown_flag(args.drop_unknown)

        if args.pixels > 0:
            data_parser.set_blur_threshold(args.pixels)

        if args.freq > 1:
            data_parser.set_freq_threshold(args.freq)

        if args.dist > -1:
            data_parser.set_dist_threshold(args.dist)

        if args.labels is not None:
            data_parser.set_labels_path(os.path.join(data_dir, args.labels))

        if args.lookups is not None:
            data_parser.set_lookups_path(os.path.join(data_dir, args.lookups))

        if args.ext != '.txt':
            data_parser.set_log_extension(args.ext)

        if args.test_ratio != .5:
            data_parser.set_test_ratio(args.test_ratio)

        if args.val_ratio != .5:
            data_parser.set_val_ratio(args.val_ratio)

        if args.qseed != 2018:
            data_parser.set_seed(args.qseed)

        if args.mark_na:
            data_parser.mark_non_annotated(data_dir)

        if args.prefix:
            data_parser.add_prefix(data_dir, args.prefix)

        if args.arrange:
            data_parser.arrange_dirs(data_dir)

        if args.unpack:
            data_parser.unpack_dirs(data_dir)

        if args.map:
            data_parser.map_labels(data_dir)

        if args.split:
            data_parser.split_data(data_dir)

        if args.easy:
            data_parser.set_noise_flag(True)
            data_parser.set_drop_unknown_flag(True)
            data_parser.set_freq_threshold(50)
            data_parser.set_dist_threshold(30)
            data_parser.arrange_dirs(data_dir)
            data_parser.unpack_dirs(data_dir)
            data_parser.map_labels(data_dir, encoding=2)
            data_parser.split_data(data_dir)
            data_parser.verify_unique_ids(data_dir)
            data_parser.stats(data_dir, class_encoding=2)

        if args.challenging:
            data_parser.set_noise_flag(True)
            data_parser.set_drop_unknown_flag(True)
            data_parser.set_freq_threshold(25)
            data_parser.set_dist_threshold(100)
            data_parser.arrange_dirs(data_dir)
            data_parser.unpack_dirs(data_dir)
            data_parser.map_labels(data_dir, encoding=2)
            data_parser.split_data(data_dir)
            data_parser.verify_unique_ids(data_dir)
            data_parser.stats(data_dir, class_encoding=2)

        if args.challenging_tracker:
            pascal_dir = os.path.join(data_dir, 'pascal')
            tracker_dir = os.path.join(data_dir, 'tracker')
            data_parser.set_noise_flag(False)
            data_parser.set_drop_unknown_flag(True)
            data_parser.set_freq_threshold(25)
            data_parser.set_dist_threshold(100)
            data_parser.create_tracker_files(tracker_dir, data_dir)
            data_parser.split_tracker_pascal_directories(data_dir, tracker_dir, pascal_dir)
            data_parser.arrange_dirs(pascal_dir)
            data_parser.unpack_dirs(pascal_dir)
            data_parser.map_labels(pascal_dir, encoding=2)
            data_parser.split_data(pascal_dir)
            data_parser.verify_unique_ids(pascal_dir)
            data_parser.stats(pascal_dir, class_encoding=2)

        if args.video_logs:
            data_parser.set_drop_unknown_flag(True)
            data_parser.clean_annotations(data_dir)
            data_parser.verify_unique_ids(data_dir)
            data_parser.stats(data_dir, class_encoding=2)

        if args.vision:
            data_parser.set_noise_flag(True)
            data_parser.set_drop_unknown_flag(True)
            data_parser.set_blur_threshold(25)
            data_parser.set_freq_threshold(25)
            data_parser.set_dist_threshold(25)
            data_parser.arrange_dirs(data_dir)
            data_parser.unpack_dirs(data_dir)
            data_parser.map_labels(data_dir, encoding=2)
            data_parser.split_data(data_dir)
            data_parser.verify_unique_ids(data_dir)
            data_parser.stats(data_dir, class_encoding=2)

    if args.util == 'test':
        data_dir = os.path.join(main_dir, args.dataset_path)

        if args.clean:
            data_parser.clean_annotations(data_dir)
            data_parser.verify_unique_ids(data_dir)

        if args.stats is not None:
            data_parser.stats(data_dir, class_encoding=args.stats)

        if args.duplicates:
            data_parser.check_duplicates(data_dir)

        if args.tags:
            data_parser.check_xml_tags(data_dir)

        if args.gps_seq:
            data_parser.check_gps_seq(data_dir)

        if args.thumbnails:
            data_parser.capture_thumbnails(data_dir)

        if args.ids:
            data_parser.verify_unique_ids(data_dir)

        if args.foi:
            data_parser.find_frames_of_interest(data_dir)

        if args.soi:
            data_parser.find_signs_of_interest(data_dir)

        if args.sign_seq:
            data_parser.find_sign_sequence(data_dir)

    if args.util == 'csv':
        data_parser.set_log_extension('.csv')

        if args.output_path:
            output_dir = args.output_path
        else:
            output_dir = args.dataset_path

        data_parser.__xml2csv(args.dataset_path, output_dir)


if __name__ == '__main__':
    main()
