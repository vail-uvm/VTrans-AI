import os, sys
import numpy as np
import operator
import ast
import shutil
from lxml import etree as ET
from math import sqrt
import pickle


import _register
__methods__ = []
register_method = _register.register_method(__methods__)


@register_method
def check_xml_tags(self, data_dir):
    """ Flag any XML files that have corrupted tags """

    # get all xml files
    for dirpath, folders, files in os.walk(data_dir):
        for f in files:
            path = os.path.join(dirpath, f)
            if f.endswith(self.annotation_extension):
                triggered = False
                warnings = []
                root = ET.parse(path).getroot()

                location = self.__get_element(root, 'Location')
                if self.__get_element(location, 'Latitude', cast=str) == 'None':
                    triggered = True
                    warnings.append('--Warning: Missing cam\'s Latitude')

                if self.__get_element(location, 'Longitude', cast=str) == 'None':
                    triggered = True
                    warnings.append('--Warning: Missing cam\'s Longitude')

                if self.__get_element(location, 'Altitude', cast=str) == 'None':
                    triggered = True
                    warnings.append('--Warning: Missing cam\'s Altitude')

                for i, obj in enumerate(root.iter('object')):
                    
                    if self.__get_element(obj, 'subclass', cast=str) == 'None':
                        triggered = True
                        warnings.append('--Warning: Missing subclass for sign \t #{}'.format(i+1))

                    location = self.__get_element(obj, 'location')

                    if self.__get_element(location, 'latitude', cast=str) == 'None':
                        triggered = True
                        warnings.append('--Warning: Missing latitude for sign \t #{}'.format(i+1))

                    if self.__get_element(location, 'longitude', cast=str) == 'None':
                        triggered = True
                        warnings.append('--Warning: Missing longitude for sign \t #{}'.format(i+1))

                    bndbox = self.__get_element(obj, 'bndbox')

                    if self.__get_element(bndbox, 'xmin', cast=str) == 'None':
                        triggered = True
                        warnings.append('--Warning: Missing xmin for sign \t #{}'.format(i+1))
                    
                    if self.__get_element(bndbox, 'ymin', cast=str) == 'None':
                        triggered = True
                        warnings.append('--Warning: Missing ymin for sign \t #{}'.format(i+1))

                    if self.__get_element(bndbox, 'xmax', cast=str) == 'None':
                        triggered = True
                        warnings.append('--Warning: Missing xmax for sign \t #{}'.format(i+1))

                    if self.__get_element(bndbox, 'ymax', cast=str) == 'None':
                        triggered = True
                        warnings.append('--Warning: Missing ymax for sign \t #{}'.format(i+1))

                if triggered:
                    parts = os.path.normpath(path).split(os.sep)
                    sub_path = os.path.join(*parts[-3:])
                    print('File path: {}'.format(sub_path))
                    for w in warnings:
                        print(w)
                    print("\n\n")

    print('--Message: Finished XML tags sanity-check successfully.')


@register_method
def capture_thumbnails(self, data_dir):
    """ Capture thumbnails of all labels(signs) found in the dataset """

    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    if not os.path.exists(self.logs_path):
        print(
            '--Error: Dataset needs to be re-arranged into PASCAL VOC format before running this test.')
        return

    for root, folders, files in os.walk(self.annotations_path):
        for f in files:
            fname, ext = os.path.splitext(f)
            if ext == self.annotation_extension:
                # parse the given xml file
                tree = ET.parse(os.path.join(root, f))
                xml_dict = self.__parse_xml(tree.getroot())

                # scan the corresponding xml file and get the label of any detected objects
                if xml_dict['signs'] and len(xml_dict['signs']) > 0:
                    for sign in xml_dict['signs']:
                        # get corresponding image path
                        img_path = os.path.join(
                            root.replace(self.annotations_path,
                                         self.images_path),
                            fname + self.image_extension
                        )

                        # crop thumbnail and save it to desk
                        self.__crop_thumbnail(
                            xml_dict['folder'], img_path, sign)

            else:
                print('--Warning: Unknown file type ', f)

    print('--Message: Finished classification sanity-check successfully.')


@register_method
def check_gps_seq(self, data_dir):
    """ Flag XML files for any mislabeled GPS information in a sequence of images """

    xml_files = []
    # get all xml files
    for root, folders, files in os.walk(data_dir):
        for f in sorted(files):
            if f.endswith(self.annotation_extension):
                xml_files.append(os.path.join(root, f))
            else:
                pass

    for i, f in enumerate(xml_files):
        next_file = xml_files[(i + 1) % len(xml_files)]
        second_next_file = xml_files[(i + 2) % len(xml_files)]
        #print('{}\n{}\n{}\n'.format(f, next_file, second_next_file))

        if f.endswith(self.annotation_extension):
            # parse the given xml file
            tree = ET.parse(f).getroot()

            # scan the corresponding xml file and get the label of any detected objects
            for idx, sign in enumerate(tree.iter('object')):
                mislabeled = True
                try:
                    loc = self.__get_element(sign, 'location')
                except ValueError as e:
                    #print('--Warning: Missing GPS info ', idx, f)
                    continue

                cur_sign_hash = np.array(
                    [
                        self.__get_element(loc, 'latitude', cast=str),
                        self.__get_element(loc, 'longitude', cast=str)
                    ]
                )

                # parse the given xml for the next file
                next_tree = ET.parse(next_file).getroot()

                # check if any of the signs in the next file has the same hash
                for next_sign in next_tree.iter('object'):
                    try:
                        next_loc = self.__get_element(
                            next_sign, 'location')
                    except ValueError as e:
                        #print('--Warning: Missing GPS info ', next_file)
                        continue

                    next_sign_hash = np.array(
                        [
                            self.__get_element(
                                next_loc, 'latitude', cast=str),
                            self.__get_element(
                                next_loc, 'longitude', cast=str)
                        ]
                    )

                    # if you find a match in the next file, skip and cont.
                    if np.array_equal(next_sign_hash, cur_sign_hash):
                        mislabeled = False
                        break

                if mislabeled:
                    # parse the given xml for the second next file
                    second_next_tree = ET.parse(second_next_file).getroot()

                    # check if any of the signs in the next file has the same hash
                    for second_next_sign in second_next_tree.iter('object'):
                        try:
                            second_next_loc = self.__get_element(
                                second_next_sign, 'location')
                        except ValueError as e:
                            #print('--Warning: Missing GPS info ', second_next_file)
                            continue

                        second_next_sign_hash = np.array(
                            [
                                self.__get_element(
                                    second_next_loc, 'latitude', cast=str),
                                self.__get_element(
                                    second_next_loc, 'longitude', cast=str)
                            ]
                        )

                        # if you find a match in the second next file, update mislabeled object
                        if np.array_equal(second_next_sign_hash, cur_sign_hash):
                            # flag currputed file
                            print(
                                '--Warning: Currputed GPS seqence ', next_file)
                            break

    print('--Message: Finished GPS-sequence sanity-check successfully.')


@register_method
def check_duplicates(self, data_dir):
    """ Flag any duplicated files in the directory """

    # Traverse through the parent directory
    hash_set = {}   # To track down duplicated files
    for root, folders, files in os.walk(data_dir):
        for fname in files:
            src = os.path.join(root, fname)

            if hash_set.get(fname) != None:
                print('--Warning: Duplicated file', src)
            else:
                hash_set[fname] = src

    print('--Message: Finished duplicates-check successfully.')


@register_method
def verify_unique_ids(self, data_dir):
    """ Verify that each sign has a unique ID to track down in a sequence of frames when needed """

    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    if os.path.exists(self.logs_path):
        path_to_search = self.annotations_path
    else:
        path_to_search = data_dir

    for root, folders, files in os.walk(path_to_search):
        for f in sorted(files):
            file_pth = os.path.join(root, f)
            fname, ext = os.path.splitext(f)
            if ext == self.annotation_extension:
                # parse the given xml file
                tree = ET.parse(file_pth).getroot()

                mislabeled = False
                htbl, ids = {}, {}  # a hash table to track unique IDs
                for sign in tree.iter('object'):
                    try:
                        cur_id = self.__get_element(sign, 'id')
                        box = self.__get_element(sign, 'bndbox')
                        xmin = self.__get_element(box, 'xmin', cast=int)
                        ymin = self.__get_element(box, 'ymin', cast=int)
                    except ValueError:
                        print('--Warning: Missing ID', file_pth)

                    if ids.get(cur_id.text) == None:
                        ids[cur_id.text] = [
                            ((xmin, ymin), sqrt(xmin**2 + ymin**2))]
                    else:
                        ids[cur_id.text] += [((xmin, ymin),
                                              sqrt(xmin**2 + ymin**2))]

                for i, keys in ids.items():
                    if len(keys) > 1:
                        mislabeled = True
                        sorted_keys = sorted(keys, key=lambda x: x[1])
                        for idx, corner in enumerate(sorted_keys):
                            htbl[corner[0]] = idx
                    else:
                        continue

                if mislabeled:
                    for sign in tree.iter('object'):
                        cur_id = self.__get_element(sign, 'id')
                        box = self.__get_element(sign, 'bndbox')
                        xmin = self.__get_element(box, 'xmin', cast=int)
                        ymin = self.__get_element(box, 'ymin', cast=int)

                        if htbl.get((xmin, ymin)) != None:
                            new_id = '{idx}_{old_id}'.format(
                                idx=htbl[(xmin, ymin)],
                                old_id=cur_id.text
                            )
                            cur_id.text = str(new_id)
                        else:
                            continue


                    ET.ElementTree(tree).write(file_pth, pretty_print=True)
                    print('--Warning: Sign ID was updated ', file_pth)

    print('--Message: Finished unique IDs sanity-check successfully.')


@register_method
def stats(self, data_dir, class_encoding=1):
    """ Collect some statistical analysis about the dataset """

    stats_tbl = {}
    total_imgs = 0
    total_pos = 0

    for root, folders, files in os.walk(data_dir):
        for fname in files:
            src = os.path.join(root, fname)
            if fname.lower().endswith(self.annotation_extension):
                total_pos += 1

                # parse the given xml file
                tree = ET.parse(src).getroot()
                for i, obj in enumerate(tree.iter('object')):
                    try:
                        # to apply the search on VTrans raw-data
                        if class_encoding == 1:  
                            sign = self.__get_element(
                                obj, 'subclass', cast=str)
                        # to apply the search on PASCAL-VOC format
                        elif class_encoding == 2:
                            sign = self.__get_element(
                                obj, 'name', cast=str)
                        else:
                            print('--Error: Undefined label encoding.')
                            return
                    except ValueError as e:
                        continue

                    # update stats table for the given sign
                    if stats_tbl.get(sign) == None:
                        stats_tbl[sign] = 1
                    else:
                        stats_tbl[sign] += 1

            if fname.lower().endswith(self.image_extension):
                total_imgs += 1

    # print stats sorted by value
    sorted_tbl = dict(
        sorted(stats_tbl.items(), key=operator.itemgetter(1)))

    for k, v in sorted_tbl.items():
        print('{} \t {}'.format(v, k.upper()))

    print('-'*20)
    print('{} \t ANNOTATIONs'.format(sum(stats_tbl.values())))
    print('{} \t LABELs'.format(sum(i > 0 for i in stats_tbl.values())))
    print('{} \t IMAGEs'.format(total_imgs))
    print('{} \t XMLs\n'.format(total_pos))
    output_dir = os.path.join(data_dir, "dataset_stats.dat")
    print(f"Output stats dictionary to {output_dir}")
    pickle.dump(stats_tbl, open(output_dir, "wb"))
    

@register_method
def clean_annotations(self, data_dir):
    """ Filter out any unnecessary metadata from the xml files """

    for root, folders, files in os.walk(data_dir):
        for fname in files:
            file_path = os.path.join(root, fname)
            if fname.endswith(self.annotation_extension):

                # load in the given xml file
                tree = ET.parse(file_path)
                xml_root = tree.getroot()
                fname, ext = os.path.splitext(os.path.basename(file_path))
                
                xml_map = self.__parse_xml(xml_root, class_tag='subclass')
                
                # if xml file is corrupted, replace it with a generic xml file
                if not xml_map:
                    os.remove(file_path)
                    self.__create_xml(data_dir, file_path)
                else:
                    # define a new xml tree structure
                    self.__fill_xml(data_dir, file_path, xml_map)

    print('--Message: Finished cleaning xml files.')


@register_method
def find_frames_of_interest(self, data_dir):
    """ Collect frames of interest of all unique signs in the dataset """

    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    if os.path.exists(self.logs_path):
        path_to_save = os.path.join(data_dir, 'ImageSets', 'Layout', 'fois'+self.log_extension)
        unique_signs = self.__find_unique_signs(self.annotations_path)
    else:
        path_to_save = os.path.join(data_dir, 'fois'+self.log_extension)
        unique_signs = self.__find_unique_signs(data_dir)
    
    foi_tbl = {}
    for k, v in unique_signs.items():
        # sort frames 
        v = sorted(v) if type(v)==list else sorted(ast.literal_eval(v))
        # pick the last frame in the sequance 
        foi_tbl[v[-1]] = k

    self.__store_dict(
        foi_tbl,
        path_to_save,
        with_value=False
    )

    print('--Message: Saved frames of interest to {}.'.format(path_to_save))


@register_method
def find_signs_of_interest(self, data_dir):
    '''
        Collect frames of interest of all unique signs in the dataset
    '''
    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    if os.path.exists(self.logs_path):
        path_to_save = os.path.join(data_dir, 'ImageSets', 'Layout', 'sois'+self.log_extension)
        unique_signs = self.__find_unique_signs(self.annotations_path)
    else:
        path_to_save = os.path.join(data_dir, 'sois'+self.log_extension)
        unique_signs = self.__find_unique_signs(data_dir)
    
    soi_tbl = {}
    for k, v in unique_signs.items():
        v = sorted(v) if type(v)==list else sorted(ast.literal_eval(v))

        # parse the given xml file
        if os.path.exists(self.annotations_path):
            xml_dict = self.__parse_xml(
                ET.parse(
                    os.path.join(
                        self.annotations_path, 
                        v[-1]+self.annotation_extension)
                ).getroot()
            )
        else:
            xml_dict = self.__parse_xml(
                ET.parse(
                    os.path.join(
                        data_dir, 
                        v[-1]+self.annotation_extension)
                ).getroot()
            )

        # scan the corresponding xml file and get the requested sign info
        if xml_dict['signs'] and len(xml_dict['signs']) > 0:
            for sign in xml_dict['signs']:
                if sign['id'] == k:
                    soi_tbl[k] = [sign['name'], sign['latitude'], sign['longitude']]

    self.__store_dict(
        soi_tbl,
        path_to_save
    )

    print('--Message: Saved signs of interest to {}.'.format(path_to_save))


@register_method
def find_sign_sequence(self, data_dir):
    '''
        Re-arrange data into a sequance frames for every unique sign in the dataset
    '''
    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    if not os.path.exists(self.logs_path):
        print(
            '--Error: Dataset needs to be re-arranged into PASCAL VOC format before running this test.')
        return
    
    unique_signs_tbl = os.path.join(
        self.logs_path, 'Layout',
        'unique'+self.log_extension
    )

    if not os.path.exists(unique_signs_tbl):
        self.find_frames_of_interest(data_dir)

    dest_dir = os.path.join(data_dir, 'unique')
    images_dest = os.path.join(dest_dir, 'JPEGImages')
    xml_dest = os.path.join(dest_dir, 'Annotations')

    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
        os.makedirs(images_dest)
        os.makedirs(xml_dest)

    for sign_id,frames in self.__load_dict(unique_signs_tbl).items(): 
        frames = ast.literal_eval(frames)
        for i, fname in enumerate(frames):
            
            # copy image
            shutil.copy(os.path.join(self.images_path, fname+self.image_extension), 
                os.path.join(images_dest, '{}_{}{}'.format(sign_id, i, self.image_extension))
            )

            # copy xml
            shutil.copy(os.path.join(self.annotations_path, fname+self.annotation_extension), 
                os.path.join(xml_dest, '{}_{}{}'.format(sign_id, i, self.annotation_extension))
            )
            
            # update xml
            self.__remove_unrelated_signs( 
                dest_dir, 
                os.path.join(xml_dest, '{}_{}{}'.format(sign_id, i, self.annotation_extension)), 
                sign_id
            )

    self.split_data(dest_dir)
