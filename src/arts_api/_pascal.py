import glob
import os
import numpy as np
import errno
import operator
from sklearn.model_selection import train_test_split

import _register
__methods__ = []
register_method = _register.register_method(__methods__)


@register_method
def arrange_dirs(self, data_dir):
    """ Move images to /JPEGImages folder and move .xml files to /Annotations folder """
    
    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    # Traverse through the parent directory
    # and move each file to the appropriate directory
    for root, folders, files in os.walk(data_dir):
        for f in files:
            fname, ext = os.path.splitext(f)
            src = os.path.join(root, fname)

            # Define relative path for images
            images_dst = os.path.join(
                self.images_path,
                root.replace(os.path.commonpath(
                    [root, self.images_path]), '')[1:]
            )

            # Define relative path for xml
            annotation_dst = os.path.join(
                self.annotations_path,
                root.replace(os.path.commonpath(
                    [root, self.annotations_path]), '')[1:]
            )

            # Move images
            if ext == self.image_extension or ext == self.image_extension.upper():
                if not os.path.exists(images_dst):
                    os.makedirs(images_dst)
                os.rename(
                    src+ext, 
                    os.path.join(images_dst, fname+self.image_extension)
                )

            # Move xml files
            elif ext == self.annotation_extension or ext == self.annotation_extension.upper():
                if not os.path.exists(annotation_dst):
                    os.makedirs(annotation_dst)
                os.rename(
                    src+ext, 
                    os.path.join(annotation_dst, fname+self.annotation_extension)
                )

            # Remove any other files with unknown extension
            else:
                print('--Warning: Removed', src+ext)
                os.remove(src+ext)

    # remove empty folders
    self.__clean_dirs(data_dir)

    self.__format_annotations(data_dir, class_tag='subclass')

    print('--Message: All files were moved to /JPEGImages & /Annotations directories.')


@register_method
def unpack_dirs(self, data_dir):
    """ Unpack all subdirectories in /JPEGImages & /Annotations recursively into one master directory """

    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    # Traverse through the parent directory
    # and move each file to the appropriate directory
    hash_set = {}   # To track down duplicated files
    for root, folders, files in os.walk(data_dir):
        for fname in files:
            src = os.path.join(root, fname)

            # check for duplicated files
            if hash_set.get(fname) is not None:
                print('--Warning: Removed duplicated file', src)
            else:
                hash_set[fname] = src

            # Move images
            if fname.endswith(self.image_extension):
                if not os.path.exists(self.images_path):
                    os.makedirs(self.images_path)
                os.rename(src, os.path.join(self.images_path, fname))

            # Move xml files
            elif fname.endswith(self.annotation_extension):
                if not os.path.exists(self.annotations_path):
                    os.makedirs(self.annotations_path)
                os.rename(src, os.path.join(self.annotations_path, fname))

            # Remove any other files with unknown extension
            else:
                print('--Warning: Removed', src)
                os.remove(src)

    # remove empty folders
    self.__clean_dirs(data_dir)

    print('--Message: All sub-directories were unpacked successfully.')


@register_method
def map_labels(self, data_dir, encoding=0):
    """ Create a lookup table by combining labels(signs) based on their names """

    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    if not os.path.exists(self.labels_path):
        self.__extract_labels(data_dir)
    else:
        self.set_labels(self.__load_dict(self.labels_path, with_value=False))

    # reset lookup table
    self.lookup_tbl.clear()
    self.set_lookup_tbl({k: 'UNKNOWN' for k in self.labels.keys()})

    for k, v in self.labels.items():
        if k.startswith('UNKNOWN') or k.startswith('NONE') or k.startswith('NULL'):
            self.lookup_tbl[k] = 'UNKNOWN'
        else:
            # MUTCD encoding
            if encoding == 0:
                self.lookup_tbl[k] = k

            # combine subclasses into a master label (superclass)
            elif encoding == 1:
                label = ''
                for char in k:
                    if char.isalpha():
                        label += str(char)
                    else:
                        break
                        
                self.lookup_tbl[k] = label

            # combine sub-subclasses into a master label (sub-class)
            elif encoding == 2:
                label = k.split('-')
                new = label[0]
                if len(label) > 1:
                    new += '-'
                    for char in label[1]:
                        if not char.isalpha():
                            new += str(char)

                self.lookup_tbl[k] = new

    # save lookup dict to ImageSets/Layout/lookups
    self.__store_dict(dict(sorted(self.lookup_tbl.items(), key=operator.itemgetter(0))),
        os.path.join(self.logs_path, 'Layout',
                    'lookups'+self.log_extension)
        )

    self.__collect_stats(self.__create_dataframe())

    print('--Message: Labels were mapped successfully')


@register_method
def split_data(self, data_dir):
    """ Split main dataset into training, validation, and testing subsets """

    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    # Create a new folder to store all logs /ImageSets
    if not os.path.exists(self.logs_path):
        try:
            os.makedirs(self.logs_path)
            os.makedirs(os.path.join(self.logs_path, 'Main'))
            os.makedirs(os.path.join(self.logs_path, 'Layout'))
        except OSError as exc:  # to avoid race condition
            if exc.errno != errno.EEXIST:
                raise

    if not os.path.exists(self.labels_path) or self.extract:
        self.__extract_labels(data_dir)
    else:
        self.set_labels(self.__load_dict(self.labels_path, with_value=False))

    # combine labels(signs) based upon their frequencies
    if self.freq_threshold > 1:
        self.__compress_labels(data_dir)

    df = self.__create_dataframe()

    if self.seed == -1:
        # split samples into train and test subsets
        X_test, X_train, y_test, y_train = train_test_split(
            df[:, 0], df[:, 1:], test_size=self.ratio_test)

        # split test samples into test and validation subsets
        X_test, X_val, y_test, y_val = train_test_split(
            X_test, y_test, test_size=self.ratio_val)
    else:
        X_test, X_train, y_test, y_train = train_test_split(
            df[:, 0], df[:, 1:], test_size=self.ratio_test, random_state=self.seed)

        # split test samples into test and validation subsets
        X_test, X_val, y_test, y_val = train_test_split(
            X_test, y_test, test_size=self.ratio_val, random_state=self.seed)

    train_data = np.column_stack((X_train, y_train)) 
    val_data = np.column_stack((X_val, y_val))
    test_data = np.column_stack((X_test, y_test))

    if(self.log_extension == '.txt'):
        self.__create_logs(train_data, val_data, test_data, sep=' ')
    elif(self.log_extension == '.csv'):
        self.__create_logs(train_data, val_data, test_data, sep=',')
    else:
        print('--Error: Unsupported file type ', self.log_extension)

    print('--Message: Dataset was rearranged into training, validation and testing subsets successfully.')


@register_method
def add_prefix(self, data_dir, prefix):
    """ Prefix for all files/folders with given prefix """

    for root, folders, files in os.walk(data_dir, topdown=False):
        for name in folders:
            #print(os.path.join(root, name))
            os.rename(
                os.path.join(root, name), 
                os.path.join(root, prefix+name)
            )
        for name in files:
            #print(os.path.join(root, name))
            os.rename(
                os.path.join(root, name), 
                os.path.join(root, prefix+name)
            )
  
    print('--Message: Finished renaming files/folders successfully.')


@register_method
def mark_non_annotated(self, data_dir):
    """
    Detects non-annotated data subsets (years where there are not XML files for any images),
    and marks them with a specific XML file indicating it wasn't annotated.
    """

    for year_dir in os.listdir(data_dir):
        # print(year_dir)
        annotation_files = glob.glob(os.path.join(data_dir, year_dir, '**', '*' + self.annotation_extension), recursive=True)
        # print(annotation_files)
        # input(f"xmls for: {year_dir}")

        if len(annotation_files) == 0:
            image_files = glob.glob(os.path.join(data_dir, year_dir, '**', '*' + self.image_extension), recursive=True)
            # print(image_files)
            # input("images")
            for image_path in image_files:
                annotation_path = os.path.splitext(image_path)[0] + self.annotation_extension
                # print(annotation_path)
                self.__create_xml(data_dir, annotation_path, annotated=False)
                # input("Enter")
