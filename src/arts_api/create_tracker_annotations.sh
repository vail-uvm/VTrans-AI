#!/usr/bin/env bash

# Run from VTrans-AI/src
# Assumes that the raw data is in VTrans-AI/src/data/raw
# Uses the GNU tool Parallel to speed up execution


parallel --jobs 200% command "python parser.py csv {}" ::: ../../../ARTS-Dataset/rawdata/*/
