import errno
import operator
import os
import math
from pathlib import Path

import numpy as np
import pandas as pd
from PIL import Image, ExifTags
from lxml import etree as ET

import _register

__methods__ = []
register_method = _register.register_method(__methods__)


@register_method
def __extract_cam_gps(self, img_path):
    """ Extract GPS info (latitude, longitude & altitude) from a given image """

    img = Image.open(img_path)
    metadata = {ExifTags.TAGS[k]: v for k,
                v in img._getexif().items() if k in ExifTags.TAGS}

    # calculate latitude & longitude for the given image
    if metadata.get('GPSInfo') is not None:
        gps = {ExifTags.GPSTAGS[k]: v for k, v in metadata['GPSInfo'].items(
        ) if k in ExifTags.GPSTAGS}
        if gps['GPSLatitude'] and gps['GPSLatitudeRef'] and gps['GPSLongitude'] and gps['GPSLongitudeRef']:
            lat = self.__convert_to_degrees(gps.get('GPSLatitude'))
            if gps.get('GPSLatitudeRef') != 'N':
                lat = 0 - lat

            lon = self.__convert_to_degrees(gps.get('GPSLongitude'))
            if gps.get('GPSLongitudeRef') != 'E':
                lon = 0 - lon

            alt = gps.get('GPSAltitude')[0]/gps.get('GPSAltitude')[1]
            if ord(gps.get('GPSAltitudeRef')) != 0:
                alt = 0 - alt

    return lat, lon, alt


@register_method
def __extract_cam_dom(self, image_path):
    """ Return the direction of movement for the camera in degrees """

    dom = None
    img = Image.open(image_path)
    metadata = {
        ExifTags.TAGS[k]: v
        for k, v in img._getexif().items()
        if k in ExifTags.TAGS
    }

    if metadata.get('GPSInfo') is not None:
        gps = {
            ExifTags.GPSTAGS[k]: v
            for k, v in metadata['GPSInfo'].items()
            if k in ExifTags.GPSTAGS
        }

        if 'GPSTrack' in gps:
            dom = gps['GPSTrack']
            dom = self.__convert_to_cam_dom(dom)

    return dom


@register_method
def __clean_dirs(self, data_dir):
    """ Remove all empty directories recursively """

    for root, folders, files in os.walk(data_dir):
        for fname in folders:
            path = os.path.join(root, fname)
            if not os.listdir(path):
                os.removedirs(path)


@register_method
def __filter_xml(self, root_dir, file_path, class_tag):
    """ Filter out any unnecessary metadata from a given xml file """

    # load in the given xml file
    tree = ET.parse(file_path)
    xml_root = tree.getroot()

    xml_map = self.__parse_xml(xml_root, class_tag)
    
    corresponding_img = file_path.replace(
        self.annotations_path, self.images_path)
    corresponding_img = corresponding_img.replace(
        self.annotation_extension, self.image_extension)

    # if xml file is corrupted, replace it with a generic xml file
    if not xml_map:
        os.remove(file_path)
        if self.no_noise:
            os.remove(corresponding_img)
        else:
            print('--Warning: Fixed corrupted xml file', file_path)
            self.__create_xml(root_dir, file_path)

    # remove xml file & image if it has no signs
    elif self.no_noise and len(xml_map['signs']) < 1:
        os.remove(file_path)
        os.remove(corresponding_img)

    else:
        # define a new xml tree structure
        self.__fill_xml(root_dir, file_path, xml_map)


@register_method
def __uniquify_ids(self, signs):
    """ Verify that each sign has a unique ID to track down in a sequence of frames when needed """
     
    mislabeled = False
    htbl, ids = {}, {}  # a hash table to track unique IDs
    for sign in signs:
        if ids.get(sign['id']) is None:
            ids[sign['id']] = [
                ((sign['xmin'], sign['ymin']), math.sqrt(sign['xmin']**2 + sign['ymin']**2))]
        else:
            ids[sign['id']] += [((sign['xmin'], sign['ymin']),
                                math.sqrt(sign['xmin']**2 + sign['ymin']**2))]
    
    for k, v in ids.items():
        if len(v) > 1:
            mislabeled = True
            sorted_keys = sorted(v, key=lambda x: x[1])
            for idx, corner in enumerate(sorted_keys):
                htbl[corner[0]] = idx
        else:
            continue

    if mislabeled:
        for sign in signs:
            if htbl.get((sign['xmin'], sign['ymin'])) is not None:
                sign['id'] = '{idx}_{old_id}'.format(
                    idx=htbl[(sign['xmin'], sign['ymin'])],
                    old_id=sign['id']
                )
            else:
                continue

    return signs


@register_method
def __collect_stats(self, df, train_df=None, val_df=None, test_df=None):
    """ Collect some statistical analysis about the dataset """

    if self.stats_tbl:
        self.stats_tbl.clear()

    self.set_stats_tbl({k: 0 for k in self.labels.keys()})
    for root, folders, files in os.walk(self.annotations_path):
        for fname in files:
            src = os.path.join(root, fname)
            if fname.endswith(self.annotation_extension):
                # parse the given xml file
                tree = ET.parse(src).getroot()
                for i, obj in enumerate(tree.iter('object')):
                    sign = self.__get_element(obj, 'name', cast=str)
                    # update stats table for the given sign
                    self.stats_tbl[sign] += 1

    # sort table by value
    self.stats_tbl = dict(sorted(self.stats_tbl.items(), key=operator.itemgetter(1)))

    # store some statistical data to ImageSets/Layout/stats
    self.stats_tbl['__TOTAL_ANNOTATIONs'] = sum(self.stats_tbl.values())
    self.stats_tbl['__TOTAL_IMAGEs'] = df.shape[0]
    self.stats_tbl['__TOTAL_LABELs'] = sum(i > 0 for i in self.stats_tbl.values()) - 2

    if train_df is not None:
        self.stats_tbl['__TRAIN'] = train_df.shape[0]
        tbl = {k: 0 for k in self.labels.keys()}
        for root, folders, files in os.walk(self.annotations_path):
            for fname in files:
                if (fname.endswith(self.annotation_extension) and
                        os.path.splitext(fname)[0] in train_df['id'].tolist()):
                    src = os.path.join(root, fname)
                    # parse the given xml file
                    tree = ET.parse(src).getroot()
                    for i, obj in enumerate(tree.iter('object')):
                        sign = self.__get_element(obj, 'name', cast=str)
                        # update stats table for the given sign
                        tbl[sign] += 1
        
        # sort table by value
        tbl = dict(sorted(tbl.items(), key=operator.itemgetter(1)))

        tbl['__TOTAL_ANNOTATIONs'] = sum(tbl.values())
        tbl['__TOTAL_IMAGEs'] = train_df.shape[0]
        tbl['__TOTAL_LABELs'] = sum(i > 0 for i in tbl.values()) - 2

        self.__store_dict(
            tbl,
            os.path.join(
                self.logs_path,
                'Layout',
                'stats_train' + self.log_extension,
            )
        )

    if val_df is not None:
        self.stats_tbl['__VAL'] = val_df.shape[0]
        tbl = {k: 0 for k in self.labels.keys()}

        for root, folders, files in os.walk(self.annotations_path):
            for fname in files:
                if (fname.endswith(self.annotation_extension) and
                        os.path.splitext(fname)[0] in val_df['id'].tolist()):
                    src = os.path.join(root, fname)
                    # parse the given xml file
                    tree = ET.parse(src).getroot()
                    for i, obj in enumerate(tree.iter('object')):
                        sign = self.__get_element(obj, 'name', cast=str)
                        # update stats table for the given sign
                        tbl[sign] += 1

        # sort table by value
        tbl = dict(sorted(tbl.items(), key=operator.itemgetter(1)))

        tbl['__TOTAL_ANNOTATIONs'] = sum(tbl.values())
        tbl['__TOTAL_IMAGEs'] = val_df.shape[0]
        tbl['__TOTAL_LABELs'] = sum(i > 0 for i in tbl.values()) - 2

        self.__store_dict(
            tbl,
            os.path.join(
                self.logs_path,
                'Layout',
                'stats_val' + self.log_extension,
            )
        )

    if test_df is not None:
        self.stats_tbl['__TEST'] = test_df.shape[0]
        tbl = {k: 0 for k in self.labels.keys()}

        for root, folders, files in os.walk(self.annotations_path):
            for fname in files:
                if (fname.endswith(self.annotation_extension) and
                        os.path.splitext(fname)[0] in test_df['id'].tolist()):
                    src = os.path.join(root, fname)
                    # parse the given xml file
                    tree = ET.parse(src).getroot()
                    for i, obj in enumerate(tree.iter('object')):
                        sign = self.__get_element(obj, 'name', cast=str)
                        # update stats table for the given sign
                        tbl[sign] += 1

        # sort table by value
        tbl = dict(sorted(tbl.items(), key=operator.itemgetter(1)))
        
        tbl['__TOTAL_ANNOTATIONs'] = sum(tbl.values())
        tbl['__TOTAL_IMAGEs'] = test_df.shape[0]
        tbl['__TOTAL_LABELs'] = sum(i > 0 for i in tbl.values()) - 2

        self.__store_dict(
            tbl,
            os.path.join(
                self.logs_path,
                'Layout',
                'stats_test'+self.log_extension,
            )
        )

    self.__store_dict(
        self.stats_tbl,
        os.path.join(
            self.logs_path,
            'Layout',
            'stats' + self.log_extension,
        )
    )


@register_method
def __create_logs(self, train_df, val_df, test_df, sep):
    """ Create a log file indicating the training, validation & testing data for each label """

    df = pd.DataFrame(
        np.row_stack((train_df, val_df, test_df)),
        columns=['id'] + list(self.labels.keys())
    )

    trainval_data = pd.DataFrame(
        np.row_stack((train_df, val_df)),
        columns=['id'] + list(self.labels.keys())
    )

    train_df = pd.DataFrame(
        train_df,
        columns=['id'] + list(self.labels.keys())
    )

    val_df = pd.DataFrame(
        val_df,
        columns=['id'] + list(self.labels.keys())
    )

    test_df = pd.DataFrame(
        test_df,
        columns=['id'] + list(self.labels.keys())
    )

    # write out current labels & lookups to ImageSets/Layout/labels
    self.__store_dict(dict(sorted(self.labels.items(), key=operator.itemgetter(0))),
                      os.path.join(self.logs_path, 'Layout',
                                   'labels'+self.log_extension),
                      with_value=False,
                      )

    # save most updated lookup dict to ImageSets/Layout/lookups
    self.__store_dict(dict(sorted(self.lookup_tbl.items(), key=operator.itemgetter(0))),
                      os.path.join(self.logs_path, 'Layout',
                                   'lookups'+self.log_extension)
                      )

    self.__collect_stats(df, train_df, val_df, test_df)

    for label in self.labels.keys():
        cols = ['id', label]
        trainval_data[cols].to_csv(
            os.path.join(self.logs_path, 'Main', label +
                         '_trainval'+self.log_extension),
            header=False, index=False, sep=sep)

        train_df[cols].to_csv(
            os.path.join(self.logs_path, 'Main', label +
                         '_train'+self.log_extension),
            header=False, index=False, sep=sep)

        val_df[cols].to_csv(
            os.path.join(self.logs_path, 'Main', label +
                         '_val'+self.log_extension),
            header=False, index=False, sep=sep)

        test_df[cols].to_csv(
            os.path.join(self.logs_path, 'Main', label +
                         '_test'+self.log_extension),
            header=False, index=False, sep=sep)

    # Traverse through logs to remove unused files
    for root, folders, files in os.walk(os.path.join(self.logs_path, 'Main')):
        for fname in files:
            src = os.path.join(root, fname)
            sign_type = fname.split('_')
            # remove label if it's not in-use anymore
            if self.labels.get(sign_type[0]) is None:
                os.remove(src)
            else:
                continue

    trainval_data['id'].to_csv(
        os.path.join(self.logs_path, 'Layout',
                     'trainval'+self.log_extension),
        header=False, index=False, sep=sep)
    trainval_data['id'].to_csv(
        os.path.join(self.logs_path, 'Main',
                     'trainval'+self.log_extension),
        header=False, index=False, sep=sep)

    train_df['id'].to_csv(
        os.path.join(self.logs_path, 'Layout', 'train'+self.log_extension),
        header=False, index=False, sep=sep)
    train_df['id'].to_csv(
        os.path.join(self.logs_path, 'Main', 'train'+self.log_extension),
        header=False, index=False, sep=sep)

    val_df['id'].to_csv(
        os.path.join(self.logs_path, 'Layout', 'val'+self.log_extension),
        header=False, index=False, sep=sep)
    val_df['id'].to_csv(
        os.path.join(self.logs_path, 'Main', 'val'+self.log_extension),
        header=False, index=False, sep=sep)

    test_df['id'].to_csv(
        os.path.join(self.logs_path, 'Layout', 'test'+self.log_extension),
        header=False, index=False, sep=sep)
    test_df['id'].to_csv(
        os.path.join(self.logs_path, 'Main', 'test'+self.log_extension),
        header=False, index=False, sep=sep)


@register_method
def __relabel(self, data_dir):
    """ Update xml files with given old labels with a new defined label """

    for root, folders, files in os.walk(self.annotations_path):
        for fname in files:
            src = os.path.join(root, fname)
            if fname.endswith(self.annotation_extension):
                # parse the given xml file
                tree = ET.parse(src).getroot()
                for i, obj in enumerate(tree.iter('object')):
                    sign_type = self.__get_element(obj, 'name')
                    
                    # re-assign name
                    if self.lookup_tbl.get(sign_type.text) is not None:
                        try:
                            sign_hash = self.__get_element(obj, 'id')
                        except ValueError as e:
                            print(e, src)
                            exit(1)

                        # update sign name & hash
                        sign_hash.text = sign_hash.text.replace(
                            sign_type.text,
                            str(self.lookup_tbl[sign_type.text])
                        )
                        sign_type.text = str(
                            self.lookup_tbl[sign_type.text])

                        ET.ElementTree(tree).write(
                            os.path.join(root, fname), pretty_print=True)


@register_method
def __compress_labels(self, data_dir):
    """ Combine labels(signs) based on their frequencies and update corresponding xml files with new labels """

    if os.path.exists(self.lookups_path):
        self.set_lookup_tbl(self.__load_dict(self.lookups_path))

        # update xml files with new labels
        self.__relabel(data_dir)

        # update labels dict and stats table
        self.labels.clear()
        self.set_labels(
            {k: i for i, k in enumerate(set(self.lookup_tbl.values()))})
        self.__collect_stats(self.__create_dataframe())

        # check for signs that don't meet the frequency criteria
        # and relabel them as UNKNOWN
        for old, new in self.lookup_tbl.copy().items():
            if self.stats_tbl[new] < self.freq_threshold:
                del self.lookup_tbl[old]
                
                # combine unknown sign-types by their superclass
                if self.combine_unknown:
                    superclass = ''
                    for char in old:
                        if char.isalpha():
                            superclass += str(char)
                        else:
                            break
                    self.lookup_tbl[new] = 'UNKNOWN_' + superclass
                else:
                    self.lookup_tbl[new] = 'UNKNOWN'

        # re-update xml files with new labels after adjusting the frequency
        self.__relabel(data_dir)
        self.__format_annotations(data_dir)

        # update labels dict and stats table
        self.labels.clear()

        new_labels = {}

        for i, k in enumerate(set(self.lookup_tbl.values())):
            # drop out annotations with unknown sign-types
            if self.drop_unknown and k.upper() == 'UNKNOWN':
                continue
            else:
                new_labels[k] = i

        self.set_labels(new_labels)

    else:
        print('--Warning: No such lookup table', self.lookups_path)


@register_method
def __create_dataframe(self):
    """ Scan all xml files and create a dataframe to store collected signs """

    # initialize an empty array to store our dataframe
    entries = []

    # Traverse through all xml files and collect their IDs(names)
    for root, folders, files in os.walk(self.annotations_path):
        if entries is None:
            entries = np.empty(shape=(len(files), len(self.labels.keys())+1), dtype=str)

        for i, fname in enumerate(files):
            f, ext = os.path.splitext(fname)
            if ext == self.annotation_extension:
                src = os.path.join(root, fname)
                
                # parse the given xml file
                tree = ET.parse(src)
                xml_dict = self.__parse_xml(tree.getroot())

                # create a new entry placeholder
                # and fill in all signs with -1 indicating that the object was not detected
                new_entry = {k: '-1' for k in self.labels.keys()}

                # scan the corresponding xml file and flag any detected objects with a value of 1
                if xml_dict.get('signs') is not None and len(xml_dict['signs']) > 0:
                    for sign in xml_dict['signs']:
                        key = str(sign['name']).upper()
                        if self.labels.get(key) is not None:
                            new_entry[key] = '1'

                entries.append(np.array([f] + list(new_entry.values())))  

    return np.array(entries)


@register_method
def __format_annotations(self, data_dir, class_tag='name'):
    """ Format all xml files as indicated in PASCAL VOC docs and create any missing xml files """

    for root, folders, images in os.walk(self.images_path):
        for img in images:
            fname, ext = os.path.splitext(img)
            if ext == self.image_extension:
                # locate the corresponding xml file
                corresponding_xml = os.path.join(
                    root.replace(self.images_path, self.annotations_path),
                    fname + self.annotation_extension
                )

                # if file already exists, clean it out
                if os.path.exists(corresponding_xml):
                    self.__filter_xml(data_dir, corresponding_xml, class_tag)
                else:  # otherwise, create a generic xml file
                    if self.no_noise:
                        os.remove(os.path.join(
                            root, fname+self.image_extension))
                    else:
                        self.__create_xml(data_dir, corresponding_xml)

            else:
                print('--Warning: Unknown file type ',
                      os.path.join(root, img))


@register_method
def __extract_labels(self, data_dir):
    """ Scan all XML files and collect all labels(signs) found in a dict """

    print('--Message: Scanning xml files to collect labels...')

    self.set_images_path(data_dir)
    self.set_annotations_path(data_dir)
    self.set_logs_path(data_dir)

    # Create a new folder to store all logs /ImageSets
    if not os.path.exists(self.logs_path):
        try:
            os.makedirs(self.logs_path)
            os.makedirs(os.path.join(self.logs_path, 'Main'))
            os.makedirs(os.path.join(self.logs_path, 'Layout'))
        except OSError as exc:  # to avoid race condition
            if exc.errno != errno.EEXIST:
                raise

    for root, folders, files in os.walk(self.annotations_path):
        for fname in files:
            src = os.path.join(root, fname)
            if fname.endswith(self.annotation_extension):
                # parse the given xml file
                tree = ET.parse(src)
                xml_dict = self.__parse_xml(tree.getroot())

                # scan the corresponding xml file and get the label of any detected objects
                if xml_dict.get('signs') is not None and (len(xml_dict['signs']) > 0 or not self.no_noise):
                    for sign in xml_dict['signs']:
                        key = str(sign['name']).upper()
                        if self.labels.get(key) is None:
                            self.labels[key] = len(self.labels.keys())
                else:
                    print('--Warning: Unknown file type ', src)


@register_method
def __remove_unrelated_signs(self, root_dir, xml_file, sign_id):
    """ Remove signs that don't match the given id in a given xml file """

    if xml_file.endswith(self.annotation_extension):
        # parse the given xml file
        tree = ET.parse(xml_file)
        xml_dict = self.__parse_xml(tree.getroot())

        # scan the corresponding xml file and get the label of any detected objects
        if xml_dict.get('signs') is not None and len(xml_dict['signs']) > 0:
            for sign in xml_dict['signs']:
                if sign_id == str(sign['id']):
                    # update list of signs 
                    xml_dict['signs'] = [sign]
                    self.__fill_xml(root_dir, xml_file, xml_dict)
                    break
                else:
                    continue
    else:
        print('--Warning: Unknown file type ', xml_file)


@register_method
def __find_unique_signs(self, data_dir):
    """ Identify all unique signs in the dataset along with a list of their frames """

    unique_signs = {}
    for root, folders, files in os.walk(data_dir):
        for f in sorted(files):
            fname, ext = os.path.splitext(f)
            if ext == self.annotation_extension:
                # parse the given xml file
                tree = ET.parse(os.path.join(root, f))
                xml_dict = self.__parse_xml(tree.getroot())

                # assign frame of interest (FOI) of current active signs to the last known frame
                if xml_dict.get('signs') is not None:
                    # scan signs in the current xml file
                    for sign in xml_dict['signs']:
                        if sign['id'] in unique_signs:
                            unique_signs.get(sign['id'], []).append(fname)
                        else:
                            unique_signs[sign['id']] = [fname]

    if os.path.exists(self.logs_path):
        self.__store_dict(
            unique_signs,
            os.path.join(self.logs_path, 'Layout', 'unique'+self.log_extension)
        )
    else:
        self.__store_dict(
            unique_signs,
            os.path.join(data_dir, 'unique'+self.log_extension)
        )

    return unique_signs


@register_method
def __xml2csv(self, data_dir, output_dir):
    """ Collects distributed XML annotations into consolidated CSV files. """

    csv_paths = []

    for road_segment in sorted(filter(lambda x: x.is_dir(), Path(data_dir).iterdir())):
        # annotations = {x: [] for x in ['image', 'id', 'class', 'lat', 'lon', 'xmin', 'xmax', 'ymin', 'ymax', 'score',
        #                                'assembly', 'side', 'X Offset', 'Y Offset']}
        annotations = {x: [] for x in ['image', 'id', 'class', 'lat', 'lon', 'xmin', 'xmax', 'ymin', 'ymax', 'score',
                                       'assembly', 'side']}

        for sequence in sorted(filter(lambda x: x.is_dir(), road_segment.iterdir())):
            for annotation in sequence.glob('*.xml'):
                print(f"Parsing following annotation file for : {annotation}")
                tree = ET.parse(str(annotation))
                xml_dict = self.__parse_xml(tree.getroot(), class_tag='subclass')

                # a dictionary counting how many times sign with same ID occurs
                # each ID is a string indicating sign class, longitude, and latitude
                # signs with the same class at the same location will therefore have the same ID by default,
                # so we separate them
                same_signs = {}

                for i, sign in enumerate(xml_dict.get('signs', [])):

                    # count the number of times each unique sign id appears in the image
                    # by adding this integer to the ID, we keep multiple instances of signs with the same ID as unique
                    if not sign['id'] in same_signs.keys():
                        same_signs[sign['id']] = 0
                    else:
                        same_signs[sign['id']] += 1

                    annotations['image'].append(str(annotation.with_suffix('.jpg')))
                    annotations['id'].append(sign['id'] + str(same_signs[sign['id']]))
                    annotations['class'].append(sign['name'])
                    annotations['lat'].append(sign['latitude'])
                    annotations['lon'].append(sign['longitude'])
                    annotations['xmin'].append(sign['xmin'])
                    annotations['xmax'].append(sign['xmax'])
                    annotations['ymin'].append(sign['ymin'])
                    annotations['ymax'].append(sign['ymax'])
                    annotations['assembly'].append(sign['assembly'])
                    annotations['side'].append(sign['side'])
        annotations['score'] = np.ones(len(annotations['id']))

        df = pd.DataFrame(annotations).sort_values('image')
        all_signs_path = Path(output_dir) / f'{road_segment.name}_all.csv'
        df.to_csv(all_signs_path, index=False)
        csv_paths.append(all_signs_path)

        df_unique = df.loc[:, ['lat', 'lon']].groupby(df['id']).mean()
        df_unique['class'] = df['class'].groupby(df['id']).agg(lambda x: x.value_counts().index[0])
        df_unique['image'] = df['image'].groupby(df['id']).agg(lambda x: x.value_counts().index[0])
        df_unique['assembly'] = df['assembly'].groupby(df['id']).agg(lambda x: x.value_counts().index[0])
        df_unique['side'] = df['side'].groupby(df['id']).agg(lambda x: x.value_counts().index[0])
        df_unique = df_unique.loc[:, ['class', 'lat', 'lon', 'assembly', 'side', 'image']].reset_index(drop=True)
        unique_signs_path = Path(output_dir) / f'{road_segment.name}_unique.csv'
        df_unique.to_csv(unique_signs_path, index=False)
        csv_paths.append(unique_signs_path)

    return csv_paths


@register_method
def rotate_coordinates(self, cam_dom, x, y):

    new_x = x * math.cos(cam_dom) + y * math.sin(cam_dom)
    new_y = y * math.cos(cam_dom) - x * math.sin(cam_dom)

    return new_x, new_y


@register_method
def get_x_y(self, sign_lat, sign_lon, cam_lat, cam_lon, cam_dom):
    """
    :param sign_lat: The latitude of the sign.
    :param sign_lon: The longitude of the sign.
    :param cam_lat: The latitude of the camera.
    :param cam_lon: The longitude of the camera.
    :param cam_dom: The direction (in degrees) the camera is facing.
    :return: The x, y sign offset from the image (in meters).
    """

    # convert to lat, lon offsets
    dLat = (sign_lat - cam_lat) * math.pi / 180
    dLon = (sign_lon - cam_lon) * math.pi / 180

    # convert the lat, lon offsets to meters
    R = 6378137
    y = dLat * R
    x = dLon * (R * math.cos(math.pi * cam_lat / 180))

    # rotate the meters offsets to align with the local image coordinate system
    rot = -1 * (math.pi / 180 * cam_dom)
    img_x, img_y = self.rotate_coordinates(rot, x, y)

    return img_x, img_y
