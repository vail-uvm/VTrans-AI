import os
import numpy as np
import errno
import operator
from sklearn.model_selection import train_test_split
from pathlib import Path

import _register
__methods__ = []
register_method = _register.register_method(__methods__)


@register_method
def create_tracker_files(self, tracker_dir, data_dir):
    """Creates a directory containing the files the tracker needs to run."""

    # self.set_log_extension('.csv')

    self.tracker_files = []

    for year in os.listdir(data_dir):
        year_dir = os.path.join(data_dir, year)
        self.tracker_files += self.__xml2csv(year_dir, year_dir)

    # tracker_dir = os.path.join(pascal_dir, 'tracker')
    # os.mkdir(tracker_dir)
    #
    # for file in tracker_files:
    #     os.rename(file, os.path.join(tracker_dir, os.path.basename(file)))

    # os.mkdir(tracker_dir)


@register_method
def split_tracker_pascal_directories(self, data_dir, tracker_dir, pascal_dir):
    """
    Creates separate directories for the detector and tracker files.
    Moves the files into the appropriate directories.
    """

    os.mkdir(tracker_dir)
    os.mkdir(pascal_dir)

    for file in self.tracker_files:
        os.rename(file, os.path.join(tracker_dir, os.path.basename(file)))

    for dir in os.listdir(data_dir):
        if not dir == os.path.basename(tracker_dir) and not dir == os.path.basename(pascal_dir):
            os.rename(os.path.join(data_dir, dir), os.path.join(pascal_dir, dir))
