import os
import _register
import _utils
import _xml
import _annotation
import _test
import _pascal
import _tracker


@_register.add_methods_from(_utils, _xml, _tracker, _annotation, _test, _pascal)
class DataManager:
    """
        A parser to arrange data according to PASCAL VOC format 
        and filter out their corresponding XML files
    """

    # Constructor
    def __init__(self):
        print('--Processing...')
        self.labels = {}
        self.stats_tbl = {}
        self.lookup_tbl = {}
        self.image_extension = '.jpg'
        self.annotation_extension = '.xml'
        self.log_extension = '.txt'
        self.extract = False
        self.no_noise = False
        self.combine_unknown = False
        self.drop_unknown = False
        self.blur_threshold = 0
        self.freq_threshold = 1
        self.dist_threshold = -1
        self.ratio_test = .5
        self.ratio_val = .5
        self.seed = 2018
        self.labels_path = None
        self.lookups_path = None
        self.images_path = os.path.join(os.getcwd(), 'JPEGImages')
        self.annotations_path = os.path.join(os.getcwd(), 'Annotations')
        self.logs_path = os.path.join(os.getcwd(), 'ImageSets')
        self.tracker_path = os.path.join(os.getcwd(), 'TrackerPath')

    def set_labels(self, d):
        self.labels = d

    def set_lookup_tbl(self, d):
        self.lookup_tbl = d

    def set_stats_tbl(self, d):
        self.stats_tbl = d

    def set_image_extension(self, ext):
        self.image_extension = ext

    def set_annotation_extension(self, ext):
        self.annotation_extension = ext

    def set_log_extension(self, ext):
        self.log_extension = ext

    def set_freq_threshold(self, f):
        self.freq_threshold = f

    def set_blur_threshold(self, f):
        self.blur_threshold = f

    def set_dist_threshold(self, f):
        self.dist_threshold = f

    def set_extract_flag(self, flag):
        self.extract = flag

    def set_noise_flag(self, flag):
        self.no_noise = flag

    def set_combine_unknown_flag(self, flag):
        self.combine_unknown = flag

    def set_drop_unknown_flag(self, flag):
        self.drop_unknown = flag

    def set_test_ratio(self, r):
        self.ratio_test = r

    def set_val_ratio(self, r):
        self.ratio_val = r

    def set_seed(self, s):
        self.seed = s

    def set_images_path(self, pth):
        self.images_path = os.path.join(pth, 'JPEGImages')

    def set_annotations_path(self, pth):
        self.annotations_path = os.path.join(pth, 'Annotations')

    def set_logs_path(self, pth):
        self.logs_path = os.path.join(pth, 'ImageSets')

        if self.labels_path is None:
            self.labels_path = os.path.join(
                self.logs_path, 'Layout', 'labels'+self.log_extension)

        if self.lookups_path is None:
            self.lookups_path = os.path.join(
                self.logs_path, 'Layout', 'lookups'+self.log_extension)

    def set_labels_path(self, pth):
        self.labels_path = pth

    def set_lookups_path(self, pth):
        self.lookups_path = pth

    def set_tracker_path(self, pth):
        self.tracker_path = pth
