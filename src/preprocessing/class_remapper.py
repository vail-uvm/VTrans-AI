"""
The datasets provided by VTrans tend to contain redundant, overlapping, or inconsistent class labels.
This code will map classes as defined in the dictionary.
The keys represent the class in the new dataset, and the values represent the classes from the raw dataset that will be
mapped to it.
"""

