"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This file fixes the missing camera GPS coordinates in the target dataset directory with coordinates from the source
directory.
"""

from lxml import etree as ET
import os
import glob


SOURCE_DIRECTORY = r'/home/vision/VTransVision/VTrans-AI/src/data/cleaned_annotations/12_old'
TARGET_DIRECTORY = r'/home/vision/VTransVision/VTrans-AI/src/data/cleaned_annotations/12_new'


def get_xml_source_files():
    source_files = glob.glob(os.path.join(SOURCE_DIRECTORY, "**", "*.xml"), recursive=True)
    return source_files


def get_xml_target_files(source_files):
    target_paths = []
    for file in source_files:
        parts = os.path.normpath(file).split(os.sep)
        keep_parts = parts[-4:]
        target_path = os.path.join(TARGET_DIRECTORY, *keep_parts)
        target_paths.append(target_path)

    return target_paths


def replace_cam_gps(source_files, target_files):

    for source_file, target_file in zip(source_files, target_files):

        # get the location data from the source file
        lat_lon_alt = []
        tree = ET.parse(source_file)
        root = tree.getroot()
        location = root.find("Location")
        for coordinate in location:
            lat_lon_alt.append(coordinate.text)

        if os.path.exists(target_file):
            try:
                # replace the corresponding data in the target file
                tree = ET.parse(target_file)
                root = tree.getroot()
                location = root.find("Location")
                for coordinate, val in zip(location, lat_lon_alt):
                    coordinate.text = val
                tree.write(target_file)
            except Exception as e:
                print(e)

        else:
            print(f"Warning: Target file {target_file} corresponding to source file {source_file} does not exist.")


if __name__ == "__main__":
    xml_source_files = get_xml_source_files()
    target_files = get_xml_target_files(xml_source_files)
    replace_cam_gps(xml_source_files, target_files)
