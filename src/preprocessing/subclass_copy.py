"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This file copies sign classes with matching ids to the target from the source directory.
"""

from lxml import etree as ET
import os
import glob


SOURCE_DIRECTORY = r'/home/vision/VTransVision/VTrans-AI/src/data/rawdata/14'
TARGET_DIRECTORY = r'/home/vision/Downloads/14_fix_subclass/14'


def get_xml_source_files():
    source_files = glob.glob(os.path.join(SOURCE_DIRECTORY, "**", "*.xml"), recursive=True)
    return source_files


def get_xml_target_files(source_files):
    target_paths = []
    for file in source_files:
        parts = os.path.normpath(file).split(os.sep)
        keep_parts = parts[-4:]
        target_path = os.path.join(TARGET_DIRECTORY, *keep_parts)
        target_paths.append(target_path)

    return target_paths


def replace_subclass(source_files, target_files):

    for source_file, target_file in zip(source_files, target_files):
        print(source_file)
        print(target_file)
        input("Enter")
        tree = ET.parse(source_file)
        root = tree.getroot()
        for i, sign in enumerate(root.iter('object')):
            # print(sign)
            subclass = sign.find('subclass')
            # print(subclass.text)
            location = sign.find('location')
            latitude = location.find('latitude')
            # print(latitude.text)
            longitude = location.find('longitude')
            # print(longitude.text)
            sign_dict = {'class': subclass.text, 'lat': float(latitude.text), 'lon': float(longitude.text)}
            if are_identical(annotation, sign_dict):
                # if True:
                id = sign.find('ID')
                # print(id.text)
                id.text = str(new_id)
                found_match = True

        if found_match:
            print(f"Added ID: {new_id} to {xml_path}")
        else:
            print(f"Warning: Did not find {annotation} in {xml_path}.")

        tree.write(xml_path)


if __name__ == "__main__":
    xml_source_files = get_xml_source_files()
    target_files = get_xml_target_files(xml_source_files)
    replace_subclass(xml_source_files, target_files)
