"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This file scans the detections in road segments and gives what appears to be each unique sign
(based on GPS and class labels) a unique integer id.
"""


import os
import glob
import pandas as pd
from pathlib import Path
from lxml import etree as ET
from duplicate_detector import are_identical


YEAR_DIRECTORY = r'/home/vision/VTransVision/VTrans-AI/src/data/large_data'


def add_id_to_xml(annotation, new_id):
    found_match = False
    xml_path = str(get_xml_path(annotation['image']))
    # xml_path = r'/home/vision/Downloads/id_test/12/127B0SV0000/12000000000023/12000000060052.xml'
    tree = ET.parse(xml_path)
    root = tree.getroot()
    for i, sign in enumerate(root.iter('object')):
        # print(sign)
        subclass = sign.find('subclass')
        # print(subclass.text)
        location = sign.find('location')
        latitude = location.find('latitude')
        # print(latitude.text)
        longitude = location.find('longitude')
        # print(longitude.text)
        sign_dict = {'class': subclass.text, 'lat': float(latitude.text), 'lon': float(longitude.text)}
        if are_identical(annotation, sign_dict):
        # if True:
            id = sign.find('ID')
            # print(id.text)
            id.text = str(new_id)
            found_match = True

    if found_match:
        print(f"Added ID: {new_id} to {xml_path}")
    else:
        print(f"Warning: Did not find {annotation} in {xml_path}.")

    tree.write(xml_path)


def get_id(sign, unique_signs, largest=0):
    """Returns the ID of the sign as the index of sign in the unique_signs dataframe."""

    match = []
    for _, row in unique_signs.iterrows():
        match.append(are_identical(sign, row))

    try:
        id = match.index(True)
    except:
        id = largest + 1
        print(f"Did not find match for sign: {sign}\n"
              f"Assigning unused id: {id}.")

    return id


def set_segment_ids(segment_dir):

    # load the unique signs and annotations dataframes
    annotations_csv_path = os.path.join(os.path.dirname(os.path.dirname(segment_dir)), Path(segment_dir).parts[-1] + "_all.csv")
    annotations = pd.read_csv(annotations_csv_path)
    unique_signs_csv_path = os.path.join(os.path.dirname(os.path.dirname(segment_dir)), Path(segment_dir).parts[-1] + "_unique.csv")
    unique_signs = pd.read_csv(unique_signs_csv_path)

    largest_id = 9999  # used to prevent duplicate sign labels
    for segment_subdir in glob.glob(os.path.join(segment_dir, "*/")):
        print(f"Processing subsegment: {segment_subdir}")

        for _, sign in annotations.iterrows():
            id = get_id(sign, unique_signs, largest=largest_id)
            if id > largest_id:
                largest_id = id
            add_id_to_xml(sign, id)


def fix_camera_heading(xml_path):
    print(f"Fixing camera heading for: {xml_path}")

    # find the camera heading format
    tree = ET.parse(xml_path)
    root = tree.getroot()
    for i, sign in enumerate(root.iter('object')):
        # print(sign)
        subclass = sign.find('subclass')
        # print(subclass.text)
        location = sign.find('location')
        latitude = location.find('latitude')
        # print(latitude.text)
        longitude = location.find('longitude')
        # print(longitude.text)
        sign_dict = {'class': subclass.text, 'lat': float(latitude.text), 'lon': float(longitude.text)}
        if are_identical(annotation, sign_dict):
            # if True:
            id = sign.find('ID')
            # print(id.text)
            id.text = str(new_id)
            found_match = True

    if found_match:
        print(f"Added ID: {new_id} to {xml_path}")
    else:
        print(f"Warning: Did not find {annotation} in {xml_path}.")

    tree.write(xml_path)


def fix_missing_altitude(xml_path):
    raise NotImplementedError


def fix_missing_sign_side(xml_path):
    raise NotImplementedError


def fix_missing_assembly(xml_path):
    raise NotImplementedError


def fix_missing_sign_class(xml_path):
    """Maybe change "NONE" sign class to "unkown"?"""

    raise NotImplementedError


def main():
    for segment_dir in glob.glob(os.path.join(YEAR_DIRECTORY, "*/")):
        print(f"Processing segment: {segment_dir}")
        print(os.path.join(segment_dir, "*/**.xml"))
        for xml_path in glob.glob(os.path.join(segment_dir, "*/*/**.xml")):
            # print(f"Processing xml: {xml_path}")
            fix_camera_heading(xml_path)


if __name__ == "__main__":
    main()
