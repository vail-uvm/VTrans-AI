"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This file removes any empty JPEG and XML files in the data directory.
"""


import os
import glob


DATA_DIRECTORY = r'/home/vision/Downloads/id_test_creation'


def get_xml_files():
    xml_files = glob.glob(os.path.join(DATA_DIRECTORY, "**", "*.xml"), recursive=True)
    return xml_files


def get_jpeg_files():
    jpeg_files = glob.glob(os.path.join(DATA_DIRECTORY, "**", "*.jpg"), recursive=True)
    return jpeg_files


def delete_empty_files(file_paths):
    for path in file_paths:
        if os.path.getsize(path) == 0:
            print(f"Removing: {path}")
            os.remove(path)


def main():
    xml_files = get_xml_files()
    jpeg_files = get_jpeg_files()
    delete_empty_files(xml_files)
    delete_empty_files(jpeg_files)


if __name__ == "__main__":
    main()
