"""
Datasets from VTrans have inconsistent formatting due to it being gathered by different parties, created using different
equipment, and stored on different severs with different compression methods.

This file contains code which will pre-process the XML and PNG files from the dataset to try to make their formatting
consistent with the other datasets so the can run without issue.

To preprocess a dataset, pass the path (including the year) to the dataset you want to preprocess as an argument.
Example path: /home/vision/Downloads/dir_test/19
"""


import argparse
import os
import glob
from pathlib import Path
from lxml import etree as ET


def get_arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset_path', type=str, help="The path to the top level folder in the dataset to preprocess.")

    return parser


def get_annotations_and_images(dataset_dir):
    """
    Returns a dictionary where each key is an image file path and each value is an annotation file path.
    If there is no annotation file path for that image the value is set to None.
    """

    search_path = Path(dataset_dir) / '*' / '*'
    annotation_dict = {}

    jpg_search_path = search_path / '*.jpg'
    jpg_paths = glob.glob(str(jpg_search_path))
    jpg_uppercase_search_path = search_path / '*.JPG'
    jpg_paths += glob.glob(str(jpg_uppercase_search_path))

    for jpg_path in jpg_paths:
        # xml_path = jpg_path.replace('.jpg', '.xml')
        xml_path = jpg_path[:-4] + '.xml'
        if Path(xml_path).exists():
            annotation_dict[jpg_path] = xml_path
        else:
            annotation_dict[jpg_path] = None

    return annotation_dict


def fix_directory_structure(dataset_directory):
    """
    Verifies that the structure of the dataset directory is correct.
    The correct directory structure should be year/segment/subsegments/images.jpg
    This function will attempt to merge any extra subdirectories with the subsegments folder.
    """

    # unpack the "FULLSIZE" directory
    unpack_directory(dataset_directory)

    # unpack the "Images" directory
    for dir in glob.glob(os.path.join(dataset_directory, '*')):
        unpack_directory(dir)

    # unpack the "ROW" directory
    for dir in glob.glob(os.path.join(dataset_directory, '*', '*')):
        unpack_directory(dir)


def unpack_directory(directory_path):
    """Unpacks the contents of a directory and then deletes it."""

    unpack_dir = os.path.join(directory_path, os.listdir(directory_path)[0])
    contents = glob.glob(os.path.join(unpack_dir, '*'))

    for content in contents:
        os.rename(content, os.path.join(directory_path, os.path.basename(content)))

    os.rmdir(unpack_dir)


def add_altitude(annotations):
    """Adds an altitude value of 0 if it is missing from the dataset."""

    for jpg_path, xml_path in annotations.items():
        if xml_path:
            tree = ET.parse(xml_path)
            root = tree.getroot()
            location = root.find('Location')
            altitude = location.find('Altitude')
            try:
                float(altitude.text)
            except ValueError:
                altitude.text = '0'
                tree.write(xml_path)
                print(f"Set altitude to 0 in: {xml_path}")


def fix_sign_side_tag(annotations):
    """
    Fixes the inconsistent tag names for the sign side elements in the xml files.
    """

    for jpg_path, xml_path in annotations.items():
        if xml_path:

            tree = ET.parse(xml_path)
            root = tree.getroot()

            for i, sign in enumerate(root.iter('object')):
                try:
                    sign_side = sign.find('SIGN_SIDE')
                    sign_side.tag = 'side'
                    tree.write(xml_path)
                    print(f"Fixed sign side tag in: {xml_path}")
                except:
                    pass


def fix_assembly_tag(annotations):
    """
    Fixes the inconsistent tag names for the assembly elements in the xml files.
    """

    for jpg_path, xml_path in annotations.items():
        if xml_path:

            tree = ET.parse(xml_path)
            root = tree.getroot()

            for i, sign in enumerate(root.iter('object')):
                try:
                    sign_side = sign.find('Assembly')
                    sign_side.tag = 'assembly'
                    tree.write(xml_path)
                    print(f"Fixed assembly tag in: {xml_path}")
                except:
                    pass


def fix_sign_side_label(annotations):
    """
    The labelIMG tool will set sign side label to 'None' if the user didn't change the default value.
    Since the default sign side is 'Right', this function will replace sign sides marked as 'None' with 'Right'.
    If the sign side annotation is completely missing, this will add a sign side element with the text 'Not Annotated'.
    """

    for jpg_path, xml_path in annotations.items():
        if xml_path:

            tree = ET.parse(xml_path)
            root = tree.getroot()

            for i, sign in enumerate(root.iter('object')):

                # find the sign side label if it exists
                sign_side = sign.find('side')
                if sign_side is None:
                    attributes = sign.find('attributes')
                    if attributes is not None:
                        sign_side = attributes.find('side')

                # add the sign side element if it is missing
                if sign_side is None:
                    sign_side = ET.Element('side')
                    sign_side.text = 'Not Annotated'
                    sign.insert(22, sign_side)
                    tree.write(xml_path)
                    print(f"Added missing sign side annotation in: {xml_path}")

                # fix the sign side text if it is None
                else:
                    if sign_side.text == 'None':
                        sign_side.text = 'Right'
                        tree.write(xml_path)
                        print(f"Fixed sign side label in: {xml_path}")


def fix_assembly_label(annotations):
    """
    The labelIMG tool will set the assembly label to 'None' if the user didn't change the default value.
    Since the default assembly value is False, this function will replace assembly tags marked as 'None' with 'False'.
    If the assembly annotation is completely missing, this will add an assembly element with the text 'Not Annotated'.
    """

    for jpg_path, xml_path in annotations.items():
        if xml_path:

            tree = ET.parse(xml_path)
            root = tree.getroot()

            for i, sign in enumerate(root.iter('object')):

                # find the assembly label if it exists
                assembly = sign.find('assembly')
                if assembly is None:
                    attributes = sign.find('attributes')
                    if attributes is not None:
                        assembly = attributes.find('assembly')

                # add he assembly element if it is missing
                if assembly is None:
                    assembly = ET.Element('assembly')
                    assembly.text = 'Not Annotated'
                    sign.insert(28, assembly)
                    tree.write(xml_path)
                    print(f"Added missing assembly annotation in: {xml_path}")

                # fix the assembly text if it is None
                else:
                    if assembly.text == 'None':
                        assembly.text = 'False'
                        tree.write(xml_path)
                        print(f"Fixed assembly label in: {xml_path}")


def fix_class_label(annotations):
    """
    Missing sign classes are sometimes marked as 'None' and other times marked as 'unknown'.
    This function will change all missing sign classes to be marked as 'unknown'.
    """

    for jpg_path, xml_path in annotations.items():
        if xml_path:

            tree = ET.parse(xml_path)
            root = tree.getroot()

            for i, sign in enumerate(root.iter('object')):

                sign_class = sign.find('subclass')
                if sign_class.text == 'None':
                    sign_class.text = 'Unknown'
                    tree.write(xml_path)
                    print(f"Fixed class label in: {xml_path}")


def add_attribute_element(annotations):
    """
    Adds the "attributes" tag if it is missing.
    Moves the sign side and assembly elements inside of the tag if they aren't already.
    """

    for jpg_path, xml_path in annotations.items():
        if xml_path:

            tree = ET.parse(xml_path)
            root = tree.getroot()

            for i, sign in enumerate(root.iter('object')):

                # add missing attributes element
                attributes = sign.find('attributes')
                if attributes is None:
                    attributes = ET.SubElement(sign, 'attributes')
                    print(f"Added missing attributes element in: {xml_path}")

                # move the sign side element to the new attribute
                if attributes.find('side') is None:
                    sign_side = sign.find('side')
                    sign_side_text = sign_side.text
                    sign.remove(sign_side)
                    sign_side = ET.SubElement(attributes, 'side')
                    sign_side.text = sign_side_text
                    print(f"Moved sign side sub-element to attribute element in: {xml_path}")

                # move the assembly element to the new attribute
                if attributes.find('assembly') is None:
                    assembly = sign.find('assembly')
                    assembly_text = assembly.text
                    sign.remove(assembly)
                    assembly = ET.SubElement(attributes, 'assembly')
                    assembly.text = assembly_text
                    print(f"Moved assembly sub-element to attribute element in: {xml_path}")

            tree.write(xml_path)


if __name__ == "__main__":
    parser = get_arg_parser()
    args = parser.parse_args()

    annotations = get_annotations_and_images(args.dataset_path)
    # print(annotations)
    # input("Enter")
    # fix_directory_structure(args.dataset_path)
    add_altitude(annotations)
    fix_sign_side_tag(annotations)
    fix_assembly_tag(annotations)
    fix_sign_side_label(annotations)
    fix_assembly_label(annotations)
    fix_class_label(annotations)
    add_attribute_element(annotations)
