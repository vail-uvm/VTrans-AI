"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This file removes xml files which don't have corresponding jpgs.
"""

from lxml import etree as ET
import os
import glob

DIRECTORY = r'/home/vision/VTransVision/VTrans-AI/src/data/rawdata_side_assembly'


def get_xml_source_files():
    source_files = glob.glob(os.path.join(DIRECTORY, "**", "*.xml"), recursive=True)
    return source_files


def get_jpg_target_file_paths(source_files):
    target_paths = []
    for file in source_files:
        parts = os.path.normpath(file).split(os.sep)
        keep_parts = parts[-4:]
        target_path = os.path.join(DIRECTORY, *keep_parts)
        target_path = target_path[:-4] + ".jpg"
        target_paths.append(target_path)

    return target_paths


def copy_jpg_files(source_files, target_files):

    for xml_file, jpg_file in zip(source_files, target_files):

        if not os.path.exists(jpg_file):
            print(f"Removing {xml_file} due to having no corresponding JPG file.")
            os.remove(xml_file)
        else:
            print(f"XML file {xml_file} corresponding to JPG file {jpg_file} found.")


if __name__ == "__main__":
    xml_source_files = get_xml_source_files()
    target_files = get_jpg_target_file_paths(xml_source_files)
    copy_jpg_files(xml_source_files, target_files)
