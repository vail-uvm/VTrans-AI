"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This file scans for potential duplicate detections and prints out these examples to the user.
"""

import os
import glob
import pandas as pd
from pathlib import Path
import numpy as np
from utils import load_detections

# file_paths = glob.glob('../data/rawdata/*/*_all.csv')
# print(file_paths)
# for path in file_paths:
#     detections = load_detections(path)
#     for image in detections.keys():
#         print(detections[image])


def get_shortened_path(path):
    path = Path(path)
    return os.path.join(*path.parts[3:])


def are_identical(sign_1, sign_2, tolerance=.000000001):
    """Returns a boolean indicating whether the passed signs the same latitude, longitude, and class."""

    # print(sign_1['lon'])
    # print(sign_2['lon'])
    # print(type(sign_1['lon']))
    # print(type(sign_2['lon']))

    return sign_1['class'] == sign_2['class'] \
           and abs(float(sign_1['lon']) - float(sign_2['lon'])) < tolerance \
           and abs(float(sign_1['lat']) - float(sign_2['lat'])) < tolerance


def within_distance(sign_1, sign_2, sensitivity=.0002):
    """Returns a boolean indicating if the signs are near one another."""

    return abs(sign_1['lat'] - sign_2['lat']) < sensitivity and abs(sign_1['lon'] - sign_2['lon']) < sensitivity


def image_contains_match(image, sign):
    """Returns a boolean indicating weather the image contains an identical match for the sign."""

    # print(image)
    detections_csv_path = Path(os.path.join(*Path(image).parts[:-3])) / (Path(image).parts[4] + "_all.csv")
    # print("detections_csv_path")
    # print(detections_csv_path)
    # input("Enter")
    image_detections = load_detections(detections_csv_path)[image]
    # print(type(image_detections))
    # print(image_detections)
    # print(f"detections for {image}")
    # print(image_detections)
    for detection in image_detections:
        # print(detection)
        if are_identical(sign, detection):
            return True
    return False


# def find_redundant(unique_signs):
#     indices_to_drop = []
#
#     for i in range(unique_signs.shape[0]):
#         # print("i")
#         # print(i)
#         # input("Next")
#         sign_1 = unique_signs.iloc[i]
#         # print(sign_1)
#         class_matches = unique_signs.iloc[i:unique_signs.shape[0]].loc[unique_signs['class'] == sign_1['class']]
#         print("\n\nclass match images")
#         print(class_matches['image'])
#         for j in range(class_matches.shape[0]):
#             # print("j")
#             # print(j)
#             sign_2 = class_matches.iloc[j]
#             if not are_identical(sign_1, sign_2):
#                 if within_distance(sign_1, sign_2):
#                     # print("match")
#                     if image_contains_match(sign_2['image'], sign_1):
#                         print(f"Found exact match for {sign_1} in image {sign_2['image']}")
#                     else:
#                         print(f"Sign of class {sign_1['class']} "
#                               f"moves from coordinates {(sign_1['lat'], sign_1['lon'])} "
#                               f"to {(sign_2['lat'], sign_2['lon'])} "
#                               f"from image {get_shortened_path(sign_1['image'])} "
#                               f"to {get_shortened_path(sign_2['image'])}.")
#
#             # input("Next")
#
#     unique_signs.drop(indices_to_drop, inplace=True)
#     unique_signs.reset_index(drop=True, inplace=True)
#
#     return unique_signs


def find_image(sign, detections):
    """Finds the image of the unique sign from the dataframe of detections."""

    class_matches = detections.loc[detections['class'] == sign['class']]
    for i in range(class_matches.shape[0]):
        next_sign = class_matches.iloc[i]
        if are_identical(sign, next_sign):
            return next_sign['image']

    raise FileNotFoundError(f"No image was found for sign {sign}")


def find_redundant_2(unique_signs, detections):
    """
    Iterates through each pair of unique signs to detect if any signs from the same class are suspiciously close
    together.
    If so, looks up file path from the detection and prints a message.
    """
    sign_classes = set(unique_signs['class'])

    for sign_class in sign_classes:
        class_matches = unique_signs.loc[unique_signs['class'] == sign_class]

        for i in range(class_matches.shape[0]):
            sign_1 = class_matches.iloc[i]

            for j in range(i+1, class_matches.shape[0]):
                sign_2 = unique_signs.iloc[j]

                if not are_identical(sign_1, sign_2) and within_distance(sign_1, sign_2):
                    sign_1_image = find_image(sign_1, detections)
                    sign_2_image = find_image(sign_2, detections)
                    if not sign_1_image == sign_2_image:
                        if not image_contains_match(sign_1_image, sign_2) and not image_contains_match(sign_2_image, sign_1):
                            print(f"Sign of class {sign_1['class']} "
                                  f"might move from coordinate {(sign_1['lat'], sign_1['lon'])} "
                                  f"to {(sign_2['lat'], sign_2['lon'])} "
                                  f"from image {get_shortened_path(sign_1_image)} "
                                  f"to {get_shortened_path(sign_2_image)}.\n")


def find_redundant_3(detections):
    """
    Iterates through each pair of detections to find if any signs from the same class are suspiciously close
    together.
    If so, looks up file path from the detection and prints a message.
    """
    sign_classes = set(detections['class'])
    redundant_signs = []  # keys: ["image", "class", "lat", "lon", "num"]
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    pd.set_option('display.max_colwidth', -1)

    for sign_class in sign_classes:
        class_matches = detections.loc[detections['class'] == sign_class]
        # print(sign_class)
        # print(class_matches['image'])
        # print(class_matches['lat'])
        # print(class_matches['lon'])

        for i in range(class_matches.shape[0]):
            sign_1 = class_matches.iloc[i]
            # print(i)

            for j in range(i+1, class_matches.shape[0]):
                sign_2 = class_matches.iloc[j]
                # print(j)
                # input("enter")

                if not are_identical(sign_1, sign_2) and within_distance(sign_1, sign_2):
                    sign_1_image = sign_1['image']
                    sign_2_image = sign_2['image']
                    if not sign_1_image == sign_2_image:
                        if not image_contains_match(sign_1_image, sign_2) and not image_contains_match(sign_2_image, sign_1):
                            # add_redundant_signs(redundant_signs, sign_1, sign_2)
                            add_sign_to_dict(redundant_signs, sign_1)
                            add_sign_to_dict(redundant_signs, sign_2)
                            # print(redundant_signs)
                            # input("Enter")

    for sign_dict in redundant_signs:
        output = f"\nSign of class {sign_dict['class'][0]} might move from "
        for image, lat, lon in zip(sign_dict['image'], sign_dict['lat'], sign_dict['lon']):
            output += f"{(round(lat, 11), round(lon, 11))} in {Path(image).parts[-1]} to "
        output = output[:-4] + "."
        print(output)


def merge_signs(sign_1, sign_2):
    # create list of sign images if they aren't sorted like that already
    sign_1_image = sign_1['image']
    sign_2_image = sign_2['image']
    # print("type test")
    # print(sign_1_image)
    # print(type(sign_1_image))
    # print(sign_2_image)
    # print(type(sign_2_image))
    if not isinstance(sign_1_image, list):
        sign_1_image = [sign_1_image]
    if not isinstance(sign_2_image, list):
        sign_2_image = [sign_2_image]
    # print("test")
    # print(sign_1_image)
    # print(sign_2_image)
    # print(sign_1_image + sign_2_image)
    # input("End test")
    new_images = []
    new_lats = []
    new_lons = []
    for image in sign_2_image:
        if image not in sign_1_image:
            new_images.append(image)

    new_sign = {'image': sign_1_image + sign_2_image,
                'class': sign_1['class'],
                'lat': (float(sign_1['lat']) + float(sign_2['lat'])) / 2,
                'lon': (float(sign_1['lon']) + float(sign_2['lon'])) / 2}

    return new_sign


def add_sign_to_dict(sign_dict_list, new_sign):
    found_match = False

    for sign_dict in sign_dict_list:
        sign = {'image': sign_dict['image'][0], 'class': sign_dict['class'][0], 'lat': sign_dict['lat'][0], 'lon': sign_dict['lon'][0]}
        if sign['class'] == new_sign['class'] and within_distance(sign, new_sign):
            found_match = True
            if not new_sign['image'] in sign_dict['image']:
                for st in ['image', 'class', 'lat', 'lon']:
                    sign_dict[st].append(new_sign[st])
            break

    if not found_match:
        sign_dict_list.append({'image': [new_sign['image']],
                    'class': [new_sign['class']],
                    'lat': [float(new_sign['lat'])],
                    'lon': [float(new_sign['lon'])]})

    return sign_dict_list


def add_redundant_signs(sign_list, sign_1, sign_2):
    new_sign = merge_signs(sign_1, sign_2)
    # print(sign_list)

    found_merge = False
    for i, sign in enumerate(sign_list):
        if sign['class'] == new_sign['class'] and within_distance(sign, new_sign) and not found_merge:
            merged_sign = merge_signs(sign, new_sign)
            sign_list[i] = merged_sign
            found_merge = True
            break
    if not found_merge:
        sign_list.append(new_sign)
    # print(sign_list)
    # input("Enter")


def find_duplicates():
    unique_signs = sorted(glob.glob('../data/rawdata/*/*_unique.csv'))
    # remove the paths corresponding to the full year
    for path in unique_signs:
        if len(path) < 33:
            unique_signs.remove(path)
    detections = sorted(glob.glob('../data/rawdata/*/*_all.csv'))

    for unique_sign_path, detection_path in zip(unique_signs, detections):
        segment_path = Path(unique_sign_path)
        print("\n\n\n")
        print(f"Year: {segment_path.parts[3]}")
        print(f"Road Segment Name: {segment_path.parts[4][:-11]}")

        unique_signs_df = pd.read_csv(unique_sign_path)
        detections_df = pd.read_csv(detection_path)
        # find_redundant_2(unique_signs_df, detections_df)
        find_redundant_3(detections_df)

if __name__ == "__main__":
    find_duplicates()
