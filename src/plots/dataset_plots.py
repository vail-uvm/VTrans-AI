import numpy as np
import pickle
import matplotlib.pyplot as plt


def load_retinanet_evaluation(path):
    total_instances, aps, moes = pickle.load(open(path, "rb"))
    return total_instances, aps, moes


def make_loss_function_plot():
    _, focal_loss_ap, focal_loss_moe = load_retinanet_evaluation("focal_loss.dat")
    _, focal_e_ap, focal_e_moe = load_retinanet_evaluation("focal_loss_e.dat")
    print(focal_loss_ap)
    print(focal_loss_moe)

    width = .8
    indices = np.arange(len(focal_loss_ap))
    plt.rcParams["figure.figsize"] = (15, 5)
    plt.bar(indices, sorted(focal_loss_ap), width=width,
            color='tab:orange', label='FL')
    plt.bar(indices, sorted(focal_e_ap),
            width=width, color='tab:green', alpha=0.5, label='FLe')

    plt.xlabel("Unique Classes")
    plt.ylabel("Average Precision (AP)")
    plt.legend()
    plt.grid()

    plt.show()


if __name__ == "__main__":
    make_loss_function_plot()