#!/usr/bin/env bash

# Run this from VTrans-AI/src

python keras_retinanet/bin/predict.py --convert-model --detections_only aran data/double_dataset_test/pascal/JPEGImages ../models/resnet50_arts-challenging_baseline.h5;

