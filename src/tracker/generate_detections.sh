#!/usr/bin/env bash

# Run this from VTrans-AI/src

for f in data/rawdata/*/*; do
    if [[ -d "$f" ]]
    then
        python keras_retinanet/bin/predict.py --convert-model --detections_only aran "$f" ../models/resnet50_arts-challenging_baseline.h5;
    fi
done

#for f in ../data/rawdata/12/12*; do
#    if [[ -d "$f" ]]
#    then
#        python ../keras_retinanet/bin/predict.py --convert-model --detections_only aran "$f" ../models/resnet50_arts-challenging_baseline.h5;
#    fi
#done
#
#
#for f in ../data/rawdata/13/13*; do
#    if [[ -d "$f" ]]
#    then
#        python ../keras_retinanet/bin/predict.py --convert-model --detections_only aran "$f" ../models/resnet50_arts-challenging_baseline.h5;
#    fi
#done
#
#
#for f in ../data/rawdata/14/14*; do
#    if [[ -d "$f" ]]
#    then
#        python ../keras_retinanet/bin/predict.py --convert-model --detections_only aran "$f" ../models/resnet50_arts-challenging_baseline.h5;
#    fi
#done