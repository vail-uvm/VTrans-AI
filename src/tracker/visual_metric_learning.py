"""
Trains a neural network to predict the "distance" between a pair of sign predictions,
which may then be used in the object tracking system in order to assign new predictions
to tracklets.
"""

import argparse
import random
import math
from pathlib import Path
from ast import literal_eval

import copy
import cv2
import numpy as np
import pandas as pd
import scipy.ndimage, scipy.misc
from skimage.transform import resize
from keras.callbacks import ModelCheckpoint
from keras.layers import Input, Embedding, concatenate, Flatten, Dense, BatchNormalization, Activation, Conv2D, Dropout, MaxPooling2D
from keras.models import Model, load_model, Sequential
from keras.optimizers import SGD, Adam, Adadelta
from keras.losses import categorical_crossentropy, binary_crossentropy, mean_squared_error
from keras.utils import Sequence
from vailtools.callbacks import CyclicLRScheduler


def build_parser():
    parser = argparse.ArgumentParser(
        description='Trains a small neural network to learn a matching function '
                    'between sign predictions from adjacent video frames.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '-d',
        '--data_folder',
        help='Path to the stacked images directory.',
        default='/home/vision/Documents/stacks',
    )

    parser.add_argument(
        '-e',
        '--epochs',
        help='Number of times the network observes the entire training set.',
        default=20,
        type=int,
    )

    parser.add_argument(
        '-b',
        '--batch_size',
        help='Number of samples that are shown to the network each step of training.',
        default=128,
        type=int,
    )

    parser.add_argument(
        '-v',
        '--verbose',
        help='Controls the amount of terminal output.',
        action='count',
        default=1,
    )

    return parser


def train_visual_network(
        data_folder='/home/vision/Documents/stacks',
        epochs=50,
        batch_size=128,
        verbose=1,
):

    csv_filepath = Path(data_folder) / 'labels.csv'
    train, val, test = load_data(csv_filepath, 0.1, 0.1)

    train = MetricLearningData(*train, batch_size=batch_size, noise=True)
    val = MetricLearningData(*val, batch_size=batch_size)
    test = MetricLearningData(*test, batch_size=batch_size)

    print('Training Images: ', train.x.shape[0])
    print('Validation Images: ', val.x.shape[0])
    print('Testing Images: ', test.x.shape[0])

    print(f'Training batches:      {len(train)}')
    print(f'Validation batches: {len(val)}')
    print(f'Testing batched:       {len(test)}')

    print("End of dataset information.\n\n\n")

    input_shape = train.x.shape[1:]

    model = build_visual_network(input_shape=input_shape)

    model.compile(loss=categorical_crossentropy,
                  optimizer=Adam(lr=0.001),
                  metrics=['accuracy'])

    checkpoint = ModelCheckpoint(
        'visual_metric_model.h5',
        save_best_only=True,
    )

    lr_schedule = CyclicLRScheduler(
        cycles=5,
        total_steps=epochs * len(train),
    )

    model.fit_generator(
        train,
        epochs=epochs,
        verbose=verbose,
        validation_data=val,
        callbacks=[checkpoint],
    )


    model = load_model('visual_metric_model.h5')
    test_results = model.evaluate_generator(test, verbose=verbose)
    print(f'Test loss: {test_results[0]: .4f}')
    print(f'Test accuracy: {test_results[1]: .4f}')

    # find any predictions the model performed particularly poorly at and save them to an output directory
    output_directory = Path('/home/vision/Documents/bad_prediction_examples')
    if not output_directory.exists():
        output_directory.mkdir()
    y_predicted = model.predict(test.x)
    model_preds = np.argmax(y_predicted, axis=1)
    correct_preds = np.argmax(test.y, axis=1)
    error_indicies = (abs(model_preds - correct_preds) > .5)
    print("Error Indices")
    print(correct_preds.shape)
    print(y_predicted.shape)
    print(error_indicies)
    print(error_indicies.shape)
    input("Enter")
    for i in range(len(error_indicies)):
        if error_indicies[i]:
            image_data = test.x[i] * 255
            class_1 = test.class_1s[i]
            class_2 = test.class_2s[i]
            image_name = 'image_number ' + (str(i)) + ' correct_class=' + str(test.y[i]) + 'pedicted_class=' + str(y_predicted[i]) + ' class_1=' + class_1 + ' class_2=' + class_2 + '.png'
            save_image(output_directory / image_name, image_data)

    print(pd.Series(y_predicted.flatten(), name='Test Predictions').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]))
    print(
        pd.Series(test.y.flatten(), name='Test Labels').describe(percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]))
    print(pd.Series((y_predicted.flatten() - test.y.flatten()).round(4), name='Test Errors').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]))
    print(pd.Series(np.abs((y_predicted.flatten() - test.y.flatten()).round(4)), name='Absolute Test Errors').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]))


def load_data(csv_path, validation_proportion=0.1, testing_proportion=0.25):
    # load the image data and labels from the csv files
    df = pd.read_csv(csv_path)
    print("\n\nDataset Information...")
    print("Total number of images: ", df.shape[0])
    image_datas = []
    labels = []
    class_1s = []
    class_2s = []
    for row in df.iterrows():
        data = row[1]
        filepath = data['filepath']
        label = literal_eval(data['label'])
        class_1s.append(data['class 1'])
        class_2s.append(data['class 2'])
        image_data = load_image(filepath)
        image_data = image_data / 255.
        image_datas.append(image_data)
        labels.append(label)

    print("Matching signs: ", labels.count([1, 0]))
    print("Non-Matching signs: ", labels.count([0, 1]))

    # split the data into training, validation, and testing
    validation_start = int(len(labels) * (1-(validation_proportion + testing_proportion)))
    testing_start = int(len(labels) * (1-testing_proportion))
    training_data = (np.array(image_datas[:validation_start]), np.array(labels[:validation_start]), class_1s[:validation_start], class_2s[:validation_start])
    validation_data = (np.array(image_datas[validation_start:testing_start]), np.array(labels[validation_start:testing_start]), class_1s[validation_start:testing_start], class_2s[validation_start:testing_start])
    testing_data = (np.array(image_datas[testing_start:]), np.array(labels[testing_start:]), class_1s[testing_start:], class_2s[testing_start:])

    return training_data, validation_data, testing_data


def load_image(filepath):
    return scipy.ndimage.imread(filepath)


def save_image(filepath, image_data):
    scipy.misc.imsave(filepath, image_data)


class MetricLearningData(Sequence):
    def __init__(
            self,
            x,
            y,
            class_1s,
            class_2s,
            batch_size,
            noise=False,
    ):
        self.x, self.y = x, y
        self.class_1s, self.class_2s = class_1s, class_2s
        self.batch_size = batch_size
        self.noise = noise

    def __len__(self):
        return int(np.ceil(len(self.y) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size:(idx + 1) * self.batch_size]

        if self.noise:
            batch_x = copy.copy(batch_x)  # copy it so the noise is not added to the actual image
            batch_x, batch_y = self.apply_noise(batch_x, batch_y)

        return batch_x, batch_y

    def apply_noise(self, batch_x, batch_y):
        batch_x = self.add_gaussian_noise(batch_x)
        batch_x = self.add_blockout(batch_x)
        batch_x = self.add_single_rotation(batch_x)
        batch_x = self.add_single_blur(batch_x)
        batch_x = self.add_single_cropping(batch_x)

        for i in range(batch_x.shape[0]):
            save_image('/home/vision/Documents/samples/' + str(batch_y[i]) + "_" + str(random.randint(0, 1000)) + ".png", batch_x[i])
        return batch_x, batch_y

    def add_gaussian_noise(self, data, stdev=3):
        noise = np.random.normal(0, stdev, data.shape) / 255.
        data += noise
        return data

    def add_blockout(self, batch_x, p=.5, num=2):
        for i in range(batch_x.shape[0]):
            for n in range(num):
                p_1 = np.random.rand()
                if p_1 < p:
                    x = np.random.randint(0, batch_x.shape[2])
                    y = np.random.randint(0, batch_x.shape[1])
                    width = random.randint(0, batch_x.shape[2] - x)
                    height = random.randint(0, batch_x.shape[1] - y)
                    c = np.random.uniform(0, 255, (height, width, batch_x.shape[3])) / 255.
                    batch_x[i, y:y + height, x:x + width, :] = c
        return batch_x

    def add_single_rotation(self, batch_x, p=.5, max_deg=5):

        # for each image in the batch
        for i in range(batch_x.shape[0]):

            # get the top and bottom images
            top_image = self.get_top_image(batch_x[i])
            bottom_image = self.get_bottom_image(batch_x[i])

            # determine whether to rotate each image
            p_rotate_top = np.random.rand()
            p_rotate_bottom = np.random.rand()

            # rotate each half if appropriate
            if p_rotate_top < p:
                top_image = scipy.ndimage.rotate(top_image, np.random.uniform(-max_deg, max_deg), reshape=False)
            if p_rotate_bottom < p:
                bottom_image = scipy.ndimage.rotate(bottom_image, np.random.uniform(-max_deg, max_deg), reshape=False)

            batch_x[i] = self.get_stacked_image(top_image, bottom_image)

        return batch_x

    def add_single_blur(self, batch_x, p=.5, amount=2):

        # for each image in the batch
        for i in range(batch_x.shape[0]):

            # get the top and bottom images
            top_image = self.get_top_image(batch_x[i])
            bottom_image = self.get_bottom_image(batch_x[i])

            # determine whether to rotate each image
            p_rotate_top = np.random.rand()
            p_rotate_bottom = np.random.rand()

            # rotate each half if appropriate
            if p_rotate_top < p:
                top_image = scipy.ndimage.gaussian_filter(top_image, sigma=amount)
            if p_rotate_bottom < p:
                bottom_image = scipy.ndimage.gaussian_filter(bottom_image, sigma=amount)

            batch_x[i] = self.get_stacked_image(top_image, bottom_image)

        return batch_x

    def add_single_cropping(self, batch_x, p=.5, max_amount=.4):
        # for each image in the batch
        for i in range(batch_x.shape[0]):

            # get the top and bottom images
            top_image = self.get_top_image(batch_x[i])
            bottom_image = self.get_bottom_image(batch_x[i])

            # determine whether to rotate each image
            p_rotate_top = np.random.rand()
            p_rotate_bottom = np.random.rand()

            # rotate each half if appropriate
            if p_rotate_top < p:
                original_shape = top_image.shape
                left_crop = random.randint(0, int(max_amount * top_image.shape[1]))
                right_crop = random.randint(top_image.shape[1] - int(max_amount * top_image.shape[1]), top_image.shape[1])
                top_crop = random.randint(0, int(max_amount * top_image.shape[0]))
                bottom_crop = random.randint(top_image.shape[0] - int(max_amount * top_image.shape[0]), top_image.shape[0])
                top_image = top_image[top_crop:bottom_crop, left_crop:right_crop]
                top_image = resize(top_image, (original_shape[0], original_shape[1]))
            if p_rotate_bottom < p:
                original_shape = bottom_image.shape
                left_crop = random.randint(0, int(max_amount * bottom_image.shape[1]))
                right_crop = random.randint(bottom_image.shape[1] - int(max_amount * bottom_image.shape[1]), bottom_image.shape[1])
                top_crop = random.randint(0, int(max_amount * bottom_image.shape[0]))
                bottom_crop = random.randint(bottom_image.shape[0] - int(max_amount * bottom_image.shape[0]), bottom_image.shape[0])
                bottom_image = bottom_image[top_crop:bottom_crop, left_crop:right_crop]
                bottom_image = resize(bottom_image, (original_shape[0], original_shape[1]))

            batch_x[i] = self.get_stacked_image(top_image, bottom_image)

        return batch_x

    def get_top_image(self, image_data):
        shape = image_data.shape
        top_image = image_data[0:int(shape[0]/2)]
        return top_image

    def get_bottom_image(self, image_data):
        shape = image_data.shape
        bottom_image = image_data[int(shape[0] / 2):shape[0]]
        return bottom_image

    def get_stacked_image(self, top_image, bottom_image):
        vertically_stacked_image_data = np.vstack((bottom_image, top_image))
        return vertically_stacked_image_data


def build_visual_network(input_shape):
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=input_shape))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Conv2D(32, (3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(512, activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(16, activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(2, activation='softmax'))
    return model


if __name__ == '__main__':
    args = build_parser().parse_args()
    train_visual_network(**vars(args))
