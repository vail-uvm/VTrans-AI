"""This file contains code to training and using an RNN which will learn whether to merge two tracklets."""

"""
Trains a neural network to predict the "distance" between a pair of sign predictions,
which may then be used in the object tracking system in order to assign new predictions
to tracklets.

TODO: Add correct noise to manually modified features.  Make it so detection features are modded after adding noise.
    Improve manual modification of features.  use bbox width and heights, use sign longitude and latitude difference
    Make it so I don't need to manually change input sizes in the functions that build the metric networks.
    Change the order of the features in the data generator to be consistent.
    Make it so image shape is not hardcoded into merged models.
    Add noise to image data.
    Implement -p and -m and -d argparse arguments.
    Add a get_data_generator() function which can then be used by the tracker.
"""


import argparse
from functools import partial
from multiprocessing import Pool
from pathlib import Path
from ast import literal_eval
import random

import numpy as np
import pandas as pd
import pickle
import scipy
import math
from sklearn import preprocessing
from skimage.transform import resize
import tensorflow as tf
from keras.callbacks import ModelCheckpoint
from keras.layers import Input, Embedding, concatenate, Flatten, Dense, BatchNormalization, Activation, Conv2D, \
    MaxPooling2D, Dropout, Concatenate, GlobalMaxPool2D, GlobalAvgPool2D, Subtract, Multiply, Lambda
from keras.models import Model, load_model, save_model
from keras.optimizers import SGD, Adam
from keras.utils import Sequence
from keras.losses import binary_crossentropy, mean_squared_error
from losses import binary_focal_loss
# from vailtools.callbacks import CyclicLRScheduler
from keras.applications.resnet50 import ResNet50
from keras import backend as K
from time import sleep
import matplotlib.pyplot as plt

from utils import cam_gps, cam_dom, load_image, save_image, load_detections, get_xml_path, get_sign_image_data




def build_parser():
    parser = argparse.ArgumentParser(
        description='Trains a small neural network to learn a matching function '
                    'between tracklets from different years.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '-d',
        '--data_folder',
        help='Path to the pre-loaded data directory.',
        default='../../../ARTS-Dataset/rawdata',
    )

    parser.add_argument(
        '-e',
        '--epochs',
        help='Number of times the network observes the entire training set.',
        default=20,
        type=int,
    )

    parser.add_argument(
        '-b',
        '--batch_size',
        help='Number of samples that are shown to the network each step of training.',
        default=128,
        type=int,
    )

    return parser


def train_tracklet_network():
    pass


def get_superclass(subclass):
    # return subclass.split("-")[0]
    return subclass[0]


def build_model(tracklets):
    pass


if __name__ == '__main__':
    args = build_parser().parse_args()
    train_tracklet_network(**vars(args))
