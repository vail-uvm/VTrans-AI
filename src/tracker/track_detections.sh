#!/usr/bin/env bash

# Run this from VTrans-AI/src
# Runs the object tracking algorithm on detections, providing a "real world"
# performance analysis.
# Uses the GNU tool Parallel to speed up execution


#parallel --jobs 100% command "python track.py {} --tracker_type=ARTS -c=.7" ::: ../data/rawdata/*/*_detections.csv

dataset_path=data/rawdata/*/*_detections.csv
#dataset_path=/home/vision/VTransVision/VTrans-AI/src/data/double_dataset_test/tracker/*_detections.csv

for segment in $dataset_path
do
  python tracker/track.py $segment --tracker_type=ARTS_Image -c=.9
done
