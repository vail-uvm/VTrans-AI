"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

To run the GOTURN tracker, you must download the files from here and place them in this directory:
https://www.dropbox.com/sh/77frbrkmf9ojfm6/AACgY7-wSfj-LIyYcOgUSZ0Ua?dl=0
"""


from pathlib import Path

import numpy as np
import cv2

from trackers import ARTSTracker, ARTSImageTracker, OPENCVTracker, PerfectTracker


def track(detections_path,
          tracker_type="ARTS",
          cost_threshold=0.98,
          save_tracklets=False,
          verbose=False,
          debug=False,
          debug_directory='/home/vision/Documents/debug'):
    """
    Args:
        detections_path: str, Path to a csv file containing detections.
        tracker_type: str, The type of tracker to use.
        score_threshold: float, Confidence score required for a detection to be considered relevant.
        cost_threshold: float, Bounding box IoU threshold used to determine when detections should be merged.
        verbose: Bool, Determines the level of output.

    Returns: List[Dict]
        List of identified signs and their estimated GPS coordinates.
    """

    tracklets_save_path = Path(detections_path).parent / f'{Path(detections_path.replace("detections", "all")).stem}_tracklets.txt'

    with open(tracklets_save_path, 'w'):
        pass

    openCV_func_lookup = {"BOOSTING": cv2.TrackerBoosting_create,
                          "MIL": cv2.TrackerMIL_create,
                          "KCF": cv2.TrackerKCF_create,
                          "TLD": cv2.TrackerKCF_create,
                          "MEDIANFLOW": cv2.TrackerMedianFlow_create,
                          "GOTURN": cv2.TrackerGOTURN_create,
                          "MOSSE": cv2.TrackerMOSSE_create,
                          "CSRT": cv2.TrackerCSRT_create}

    # run the appropriate tracker to generate the tracklets
    if tracker_type == "ARTS":
        tracker = ARTSTracker(detections_path, distance_threshold=cost_threshold)
        tracker.track(save_tracklets_to_csv=save_tracklets, debug=debug)
        # tracker.remove_tracklets_by_length()
    elif tracker_type == "ARTS_Image":
        tracker = ARTSImageTracker(detections_path, distance_threshold=cost_threshold)
        tracker.track(save_tracklets_to_csv=save_tracklets, debug=debug)
        # tracker.remove_tracklets_by_length()
    elif tracker_type == "PERFECT":
        tracker = PerfectTracker(detections_path)
        tracker.track(save_tracklets_to_csv=save_tracklets, debug=debug)
    elif tracker_type in openCV_func_lookup.keys():
        tracker_func = openCV_func_lookup[tracker_type]
        tracker = OPENCVTracker(detections_path, tracker_func, distance_threshold=cost_threshold)
        tracker.track(save_tracklets_to_csv=save_tracklets, debug=debug)

    signs = tracker.get_signs()
    return signs
