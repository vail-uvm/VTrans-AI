"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import csv
import os

import cv2
from PIL import Image, ExifTags
from pathlib import Path
import numpy as np
import scipy.ndimage, scipy.misc
from skimage.transform import resize
from keras.preprocessing.image import load_img, img_to_array
import imageio


def load_detections(file_path):
    """ Load detections from a csv file """

    if not os.path.exists(file_path):
        print('Error: {} - No such file or directory'.format(file_path))
    else:
        detections = {}

        with open(file_path, 'r') as log:
            reader = csv.reader(log, delimiter=',')
            header = next(reader, None)

            for d in reader:
                sign = dict(zip(header, d))
                detections[d[0]] = detections.get(d[0], []) + [sign]
        return detections


def save_detections(file_path, detections):
    """  Save detections to a csv file """

    with open(file_path, 'w') as log:
        writer = csv.writer(log, delimiter=',')
        writer.writerow(['class', 'lat', 'lon', 'score'])

        for s in detections:
            writer.writerow(
                [
                    '|'.join(s['class']) if type(s['class']) is list else s['class'],
                    s['lat'],
                    s['lon'],
                    s['score']
                ]
            )


def cam_gps(image_path):
    """ Return gps coordinates for the camera """

    def _convert_to_degress(gps):
        """ Convert the GPS coordinates stored in the EXIF to degress in float format """

        d = float(gps[0])
        m = float(gps[1])
        s = float(gps[2])

        return d + (m / 60.0) + (s / 3600.0)

    lat, lon, alt = None, None, None

    img = Image.open(image_path)
    metadata = {
        ExifTags.TAGS[k]: v
        for k, v in img._getexif().items()
        if k in ExifTags.TAGS
    }

    if metadata.get('GPSInfo') is not None:
        gps = {
            ExifTags.GPSTAGS[k]: v
            for k, v in metadata['GPSInfo'].items()
            if k in ExifTags.GPSTAGS
        }

        if (
                ('GPSLatitude' in gps) and
                ('GPSLatitudeRef' in gps) and
                ('GPSLongitude' in gps) and
                ('GPSLongitudeRef' in gps)
        ):
            lat = _convert_to_degress(gps.get('GPSLatitude'))
            if gps.get('GPSLatitudeRef') != 'N':
                lat = 0 - lat

            lon = _convert_to_degress(gps.get('GPSLongitude'))
            if gps.get('GPSLongitudeRef') != 'E':
                lon = 0 - lon

            if gps.get('GPSAltitude'):
                alt = gps.get('GPSAltitude')
                if ord(gps.get('GPSAltitudeRef')) != 0:
                    alt = 0 - alt
            else:
                alt = None

    return lat, lon, alt


def cam_dom(image_path):
    """ Return the direction of movement for the camera in degrees """
    dom = None
    img = Image.open(image_path)
    metadata = {
        ExifTags.TAGS[k]: v
        for k, v in img._getexif().items()
        if k in ExifTags.TAGS
    }

    if metadata.get('GPSInfo') is not None:
        gps = {
            ExifTags.GPSTAGS[k]: v
            for k, v in metadata['GPSInfo'].items()
            if k in ExifTags.GPSTAGS
        }

        if 'GPSTrack' in gps:
            dom = gps['GPSTrack']

    return dom


def get_xml_path(filepath):
    xml_path = (Path(filepath).parent / Path(filepath).stem).with_suffix('.xml')
    return xml_path


def get_image_annotations(image_path):
    """Returns a list of dicts containing all the annotations for the passed image path."""

    csv_path = Path(image_path).parts
    csv_path = Path(*csv_path[:-3]) / (csv_path[-3] + '_all.csv')
    all_annotations = load_detections(csv_path)
    annotations = all_annotations.get(image_path)

    return annotations


def get_vertically_stacked_sign_data_from_predictions(pred_1, pred_2, size=32, pad=0.3):
    """Returns a numpy array containing a concatenated image of the two signs in pred_1 and pred_2."""

    # get the file paths for the corresponding images
    previous_image_filepath = pred_1[0]
    current_image_filepath = pred_2[0]

    # find the locations of the signs for each image
    previous_sign_location = pred_1[5:9]
    current_sign_location = pred_2[5:9]

    # load the image data for each sign
    previous_image_data = load_image(previous_image_filepath)
    current_image_data = load_image(current_image_filepath)

    # cut the numpy array to find the appropriate signs
    pad_height = int((previous_sign_location[3] - previous_sign_location[2] + .5) * pad)
    pad_width = int((previous_sign_location[1] - previous_sign_location[0] + .5) * pad)
    small_y = previous_sign_location[2] - pad_height
    large_y = previous_sign_location[3] + pad_height
    small_x = previous_sign_location[0] - pad_width
    large_x = previous_sign_location[1] + pad_width
    if small_y < 0:
        small_y = 0
    if small_x < 0:
        small_x = 0
    if large_y > previous_image_data.shape[0]:
        large_y = previous_image_data.shape[0]
    if large_x > previous_image_data.shape[1]:
        large_x = previous_image_data.shape[1]
    previous_sign_data = previous_image_data[small_y:large_y, small_x:large_x]

    pad_height = int((current_sign_location[3] - current_sign_location[2] + .5) * pad)
    pad_width = int((current_sign_location[1] - current_sign_location[0] + .5) * pad)
    small_y = current_sign_location[2] - pad_height
    large_y = current_sign_location[3] + pad_height
    small_x = current_sign_location[0] - pad_width
    large_x = current_sign_location[1] + pad_width
    if small_y < 0:
        small_y = 0
    if small_x < 0:
        small_x = 0
    if large_y > current_image_data.shape[0]:
        large_y = current_image_data.shape[0]
    if large_x > current_image_data.shape[1]:
        large_x = current_image_data.shape[1]
    current_sign_data = current_image_data[small_y:large_y, small_x:large_x]

    # resize the images to a uniform size
    previous_sign_data = resize(previous_sign_data, (int(size / 2), size))
    current_sign_data = resize(current_sign_data, (int(size / 2), size))

    # merge the two sign images into a single image
    vertically_stacked_image_data = np.vstack((previous_sign_data, current_sign_data))
    # horizontally_stacked_image_data = np.hstack((vertically_stacked_image_data, vertically_stacked_image_data))

    return vertically_stacked_image_data


def get_sign_image_data(file_path, xmin=None, xmax=None, ymin=None, ymax=None, dimensions=(32, 32), pad=0):
    """Returns a numpy array containing the sign image data from the passed coordinates."""

    # load the image data for each sign
    image_data = load_image(file_path)

    xmin = 0 if not xmin else xmin
    xmax = image_data.shape[1] if not xmax else xmax
    ymin = 0 if not ymin else ymin
    ymax = image_data.shape[0] if not ymax else ymax

    # find the padding distance
    pad_height = int((ymax - ymin + .5) * pad)
    pad_width = int((xmax - xmin + .5) * pad)

    # find the x and y bounds
    xmin = xmin - pad_width
    ymin = ymin - pad_height
    xmax = xmax + pad_width
    ymax = ymax + pad_height

    # clip the x and y so they don't go past the edge of the image
    if xmin < 0:
        xmin = 0
    if ymin < 0:
        ymin = 0
    if ymax > image_data.shape[0]:
        ymax = image_data.shape[0]
    if xmax > image_data.shape[1]:
        xmax = image_data.shape[1]
    sign_data = image_data[ymin:ymax, xmin:xmax]

    # resize the image to a uniform size
    sign_data = resize(sign_data, dimensions)

    return sign_data


def save_image(filepath, image_data):
    # scipy.misc.imsave(filepath, image_data)
    image_data = 255 * image_data
    image_data = image_data.astype(np.uint8)
    imageio.imwrite(filepath, image_data)


def load_image(filepath):
    return img_to_array(load_img(filepath)) / 255.


def __get_element(parent, child, cast=None):
    """ Get a given tag from an XML file """

    element = parent.find(child)

    if element is None:
        raise ValueError('--Error: No such tag \'{}\''.format(child))

    if cast is not None:
        try:
            return cast(element.text)
        except ValueError as e:
            raise(ValueError(
                '--Error: illegal value for \'{}\': {}'.format(child, e)), None)

    return element


def __parse_sign(obj, class_tag='name'):
    """ Parse all metadata from a given xml object """
    sign = {}
    location = __get_element(obj, 'location')
    bndbox = __get_element(obj, 'bndbox')

    sign['name'] = __get_element(obj, class_tag, cast=str)
    sign['truncated'] = __get_element(obj, 'truncated', cast=int)
    sign['difficult'] = __get_element(obj, 'difficult', cast=int)
    sign['latitude'] = __get_element(location, 'latitude', cast=float)
    sign['longitude'] = __get_element(location, 'longitude', cast=float)
    sign['xmin'] = round(__get_element(bndbox, 'xmin', cast=int))
    sign['ymin'] = round(__get_element(bndbox, 'ymin', cast=int))
    sign['xmax'] = round(__get_element(bndbox, 'xmax', cast=int))
    sign['ymax'] = round(__get_element(bndbox, 'ymax', cast=int))

    try:
        sign['id'] = __get_element(obj, 'id', cast=str)
    except ValueError:
        sign['id'] = '{name}_{lat}_{lon}'.format(
            name=sign['name'],
            lat=sign['latitude'],
            lon=sign['longitude']
        )

    return sign


def __parse_xml(root, class_tag='name'):
    """ Parse all metadata from a given xml file """

    xml_tree = {}
    try:
        img_size = __get_element(root, 'size')
        location = __get_element(root, 'Location')
        xml_tree['folder'] = __get_element(root, 'folder', cast=str)
        xml_tree['filename'] = __get_element(root, 'filename', cast=str)
        xml_tree['width'] = __get_element(img_size, 'width', cast=int)
        xml_tree['height'] = __get_element(img_size, 'height', cast=int)
        xml_tree['depth'] = __get_element(img_size, 'depth', cast=int)
        xml_tree['Latitude'] = __get_element(location, 'Latitude', cast=float)
        xml_tree['Longitude'] = __get_element(location, 'Longitude', cast=float)
        xml_tree['Altitude'] = __get_element(location, 'Altitude', cast=float)
        xml_tree['signs'] = []
        for i, obj in enumerate(root.iter('object')):
            sign = __parse_sign(obj, class_tag)

            # check if bndbox is out of boundary
            sign['xmin'] = 0 if sign['xmin'] < 0 else sign['xmin']
            sign['ymin'] = 0 if sign['ymin'] < 0 else sign['ymin']
            sign['xmax'] = xml_tree['width'] if sign['xmax'] > xml_tree['width'] else sign['xmax']
            sign['ymax'] = xml_tree['height'] if sign['ymax'] > xml_tree['height'] else sign['ymax']

            # # skip too blury (too small) signs
            # h_sign = int(sign['ymax'] - sign['ymin'])
            # w_sign = int(sign['xmax'] - sign['xmin'])
            # if blur_threshold > 0 and (w_sign < blur_threshold or h_sign < blur_threshold):
            #     continue
            #
            # # skip sign if it's too far away
            # dist = __calc_distance(
            #     cam_gps=[xml_tree['Latitude'], xml_tree['Longitude']],
            #     obj_gps=[sign['latitude'], sign['longitude']]
            # )
            # if dist_threshold > -1 and dist > dist_threshold:
            #     continue

            # drop out annotations with unkown sign-types
            if str(sign['name']).upper() == 'UNKNOWN':
                continue

            xml_tree['signs'].append(sign)

        return xml_tree

    # flag corrupted xml file to replace it with a new one
    except ValueError:
        return {}


def get_signs_from_xml(root, class_tag='name'):
    xml_tree = {}
    signs = []
    try:
        img_size = __get_element(root, 'size')
        location = __get_element(root, 'Location')
        xml_tree['folder'] = __get_element(root, 'folder', cast=str)
        xml_tree['filename'] = __get_element(root, 'filename', cast=str)
        xml_tree['width'] = __get_element(img_size, 'width', cast=int)
        xml_tree['height'] = __get_element(img_size, 'height', cast=int)
        xml_tree['depth'] = __get_element(img_size, 'depth', cast=int)
        xml_tree['Latitude'] = __get_element(location, 'Latitude', cast=float)
        xml_tree['Longitude'] = __get_element(location, 'Longitude', cast=float)
        xml_tree['Altitude'] = __get_element(location, 'Altitude', cast=float)
        xml_tree['signs'] = []
        for i, obj in enumerate(root.iter('object')):
            sign = __parse_sign(obj, class_tag)

            # check if bndbox is out of boundary
            sign['xmin'] = 0 if sign['xmin'] < 0 else sign['xmin']
            sign['ymin'] = 0 if sign['ymin'] < 0 else sign['ymin']
            sign['xmax'] = xml_tree['width'] if sign['xmax'] > xml_tree['width'] else sign['xmax']
            sign['ymax'] = xml_tree['height'] if sign['ymax'] > xml_tree['height'] else sign['ymax']

            # # skip too blury (too small) signs
            # h_sign = int(sign['ymax'] - sign['ymin'])
            # w_sign = int(sign['xmax'] - sign['xmin'])
            # if blur_threshold > 0 and (w_sign < blur_threshold or h_sign < blur_threshold):
            #     continue
            #
            # # skip sign if it's too far away
            # dist = __calc_distance(
            #     cam_gps=[xml_tree['Latitude'], xml_tree['Longitude']],
            #     obj_gps=[sign['latitude'], sign['longitude']]
            # )
            # if dist_threshold > -1 and dist > dist_threshold:
            #     continue

            # drop out annotations with unkown sign-types
            if str(sign['name']).upper() == 'UNKNOWN':
                continue

            signs.append(sign)

    except Exception as e:
        print(e)
        return {}

    return signs


def bndbox_iou(bbs1, bbs2):
    """
    Calculate IOU for each pair of bounding boxes

    Args:
        bbs1: numpy.ndarray, (N, 4) array of bounding box coordinates.
        bbs2: numpy.ndarray, (M, 4) array of bounding box coordinates.

    Returns: numpy.ndarray, (N, M) matrix of IoU scores.
    """

    x11, y11, x12, y12 = np.split(bbs1, 4, axis=1)
    x21, y21, x22, y22 = np.split(bbs2, 4, axis=1)

    x_a = np.maximum(x11, np.transpose(x21))
    y_a = np.maximum(y11, np.transpose(y21))
    x_b = np.minimum(x12, np.transpose(x22))
    y_b = np.minimum(y12, np.transpose(y22))

    inner_area = np.maximum((x_b - x_a + 1), 0) * np.maximum((y_b - y_a + 1), 0)

    box_a_area = (x12 - x11 + 1) * (y12 - y11 + 1)
    box_b_area = (x22 - x21 + 1) * (y22 - y21 + 1)

    iou = inner_area / (box_a_area + np.transpose(box_b_area) - inner_area)

    return iou


def convert_bbox(bbox, reverse=False):
    """
    Converts bbox as (x, y, w, h) to (x1, y1, x2, y2)
    Args:
        bbox: tuple as (x, y, w, h)
        reverse: will convert the bbox in the other direction

    Returns:
        bbox: tuple as (x1, y1, x2, y2)
    """

    x, y, w, h = bbox
    x1 = x
    y1 = y

    if reverse:
        x2 = w - x
        y2 = h - y
    else:
        x2 = x + w
        y2 = y + h

    return x1, y1, x2, y2


def get_bbox(detection):
    """
    Returns the bounding box from the detection as a tuple.
    Args:
        prediction: a dictionary containing the detected object

    Returns:
        a tuple of (xmin, ymin, xmax, ymax)

    """

    xmin = int(detection['xmin'])
    ymin = int(detection['ymin'])
    xmax = int(detection['xmax'])
    ymax = int(detection['ymax'])

    return xmin, ymin, xmax, ymax


def load_and_resize_image(path, dim):
    img = cv2.imread(path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    res = cv2.resize(img, dsize=dim, interpolation=cv2.INTER_LINEAR)
    res = res / 255
    return res
