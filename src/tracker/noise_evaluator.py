"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This file quantifies the amount of noise introduced by the RetinaNet detection model.
Run this from the tracker directory...
python noise_evaluator.py
"""

import glob
from pathlib import Path
import numpy as np
import pickle
import matplotlib.pyplot as plt
from matplotlib.ticker import FormatStrFormatter
import os

from utils import load_detections, bndbox_iou, get_bbox
from trackers import Tracker
from pure_image_similarity_metric import get_superclass


DATA_DIR = r'data/rawdata'
SUPERCLASS_GRAPH_PATH = r'tracker/superclass_noise.png'
CLASS_NOISE_PATH = r'tracker/class_noise.png'
SIGN_GPS_NOISE_PATH = r'tracker/sign_gps_noise.png'
BOUNDING_BOX_NOISE = r'tracker/bounding_box_noise.png'
NOISES_FILE_PATH = r'tracker/noises.dat'


def get_most_similar_detection(annotation, detections):
    """Pass an annotation and list of detections. Returns most similar detection from list."""

    similarities = []  # compute a similarity score indicating how similar the detection is compared to the annotation
    for detection in detections:
        iou = bndbox_iou(np.array([get_bbox(annotation)]), np.array([get_bbox(detection)]))[0, 0]
        similarities.append(iou)

    most_similar_index = similarities.index(max(similarities))

    return detections[most_similar_index], similarities[most_similar_index]


def append_noises(annotation, detection, x_mins, x_maxs, y_mins, y_maxs, lons, lats, classes, superclasses):
    """Adds the "noise" introduced by each detection to the appropriate list."""

    x_mins.append(int(detection['xmin']) - int(annotation['xmin']))
    x_maxs.append(int(detection['xmax']) - int(annotation['xmax']))
    y_mins.append(int(detection['ymin']) - int(annotation['ymin']))
    y_maxs.append(int(detection['ymax']) - int(annotation['ymax']))
    lons.append(float(detection['lon']) - float(annotation['lon']))
    lats.append(float(detection['lat']) - float(annotation['lat']))
    classes.append(detection['class'] == annotation['class'])
    superclasses.append(get_superclass(detection['class']) == get_superclass(annotation['class']))

    return x_mins, x_maxs, y_mins, y_maxs, lons, lats, classes, superclasses


def save_noises(file_path, x_mins, x_maxs, y_mins, y_maxs, lons, lats, classes, superclasses):
    """Saves all of the noises to a file in a dictionary using pickle."""

    noises = {'xmin': x_mins,
              'xmax': x_maxs,
              'ymin': y_mins,
              'ymax': y_maxs,
              'lon': lons,
              'lat': lats,
              'class': classes,
              'superclass': superclasses}

    pickle.dump(noises, open(file_path, "wb"))


def sample_training_noise_distribution(num_samples=100):
    """Samples noises from the training distribution for the distance metric network."""

    x_mins = np.random.randint(-20, 20, size=(num_samples,))
    x_maxs = np.random.randint(-20, 20, size=(num_samples,))
    y_mins = np.random.randint(-20, 20, size=(num_samples,))
    y_maxs = np.random.randint(-20, 20, size=(num_samples,))
    lons = np.random.normal(loc=0, scale=0.00006 / 3, size=(num_samples,)) - np.random.normal(loc=0, scale=0.00006 / 3, size=(num_samples,))
    lats = np.random.normal(loc=0, scale=0.00006 / 3, size=(num_samples,)) - np.random.normal(loc=0, scale=0.00006 / 3, size=(num_samples,))
    classes = np.random.random(size=num_samples) < 1 - 0.001
    superclasses = np.random.random(size=num_samples) < 1 - 0.001

    return x_mins, x_maxs, y_mins, y_maxs, lons, lats, classes, superclasses


def plot_noise_distributions(observed, trained):
    """Constructs plots comparing the observed and training noise distributions."""

    alph = 0.5

    # make plot of bounding box distributions
    boxes = ["X Min", "X Max", "Y Min", "Y Max"]
    width = 20
    plt.figure(figsize=(60, 60))
    fig, axs = plt.subplots(2, 2)
    fig.subplots_adjust(hspace=0.3)
    for i, ax in enumerate(axs.flatten()):
        ax.hist(observed[i], bins=30, range=(-width, width), label="Observed", alpha=alph)
        # ax.hist(trained[i], bins=30, range=(-width, width), label="Trained", alpha=alph)
        ax.legend()
        ax.set_title(f"{boxes[i]} Noise")
    fig.suptitle("Bounding Box Noise Distributions")
    fig.savefig(BOUNDING_BOX_NOISE)

    # make a plot of latitude and longitude distributions
    coordinates = ["Longitude", "Latitude"]
    width = .0001
    plt.figure(figsize=(60, 60))
    fig, axs = plt.subplots(2)
    # fig.tight_layout()
    fig.subplots_adjust(hspace=0.3)
    for i, ax in enumerate(axs.flatten()):
        ax.hist(observed[i+4], bins=30, range=(-width, width), label="Observed", alpha=alph)
        # ax.hist(trained[i+4], bins=30, range=(-width, width), label="Trained", alpha=alph)
        ax.legend()
        ax.set_title(f"{coordinates[i]} Noise")
        ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    fig.suptitle("Sign GPS Noise Distributions")
    fig.savefig(SIGN_GPS_NOISE_PATH)

    # make a plot of the class distributions
    plt.figure(figsize=(60, 60))
    fig, ax = plt.subplots(1)
    unique, counts = np.unique(observed[-2], return_counts=True)
    ax.bar([str(u) for u in unique], counts, label="Observed", alpha=alph)
    # unique, counts = np.unique(trained[-2], return_counts=True)
    # ax.bar([str(u) for u in unique], counts, label="Trained", alpha=alph)
    ax.legend()
    ax.set_title("Class Noise")
    fig.suptitle("Class Noise Distributions")
    fig.savefig(CLASS_NOISE_PATH)

    # make a plot of the superclass distributions
    plt.figure(figsize=(60, 60))
    fig, ax = plt.subplots(1)
    unique, counts = np.unique(observed[-1], return_counts=True)
    ax.bar([str(u) for u in unique], counts, label="Observed", alpha=alph)
    # unique, counts = np.unique(trained[-1], return_counts=True)
    # ax.bar([str(u) for u in unique], counts, label="Trained", alpha=alph)
    ax.legend()
    ax.set_title("Superclass Noise")
    fig.suptitle("Superclass Noise Distributions")
    fig.savefig(SUPERCLASS_GRAPH_PATH)


def main(save_path=NOISES_FILE_PATH, similarity_requirement=0.5):
    # a list of noises to track
    x_mins = []
    x_maxs = []
    y_mins = []
    y_maxs = []
    lons = []
    lats = []
    classes = []
    superclasses = []

    annotation_directories = sorted(glob.glob(DATA_DIR + r'/*/*_all.csv'))
    detection_directories = sorted(glob.glob(DATA_DIR + r'/*/*_detections.csv'))
    sample_tracker = Tracker(annotation_directories[0])  # used for filtering redundant detections

    for i, (annotation_directory, detection_directory) in enumerate(zip(annotation_directories, detection_directories)):

        print(f"Percent complete: {i/len(annotation_directories)*100}")

        annotations = load_detections(annotation_directory)
        detections = load_detections(detection_directory)
        road_segment_path = Path(list(annotations.keys())[0]).parent.parent
        images = sorted(road_segment_path.glob('*/*.jpg'), reverse=True)

        for i, img in enumerate(images):

            next_frame = str(img)
            dets = detections.get(next_frame)
            dets = sample_tracker.filter_redundant_detections(dets)
            anns = annotations.get(next_frame)

            if anns and dets:
                for ann in anns:
                    det, sim = get_most_similar_detection(ann, dets)
                    if sim > similarity_requirement:
                        x_mins, x_maxs, y_mins, y_maxs, lons, lats, classes, superclasses = \
                            append_noises(ann, det, x_mins, x_maxs, y_mins, y_maxs, lons, lats, classes, superclasses)

    observed_noises = (x_mins, x_maxs, y_mins, y_maxs, lons, lats, classes, superclasses)
    training_noises = sample_training_noise_distribution(len(observed_noises[0]))

    save_noises(save_path, *observed_noises)

    plot_noise_distributions(observed_noises, training_noises)


if __name__ == "__main__":
    main()
