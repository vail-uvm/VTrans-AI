"""
Trains a neural network to predict the "distance" between a pair of sign predictions,
which may then be used in the object tracking system in order to assign new predictions
to tracklets.

Run this from VTransAI/src.
"""


import argparse
from functools import partial
from multiprocessing import Pool
from pathlib import Path
from ast import literal_eval
import random

import numpy as np
import pandas as pd
import pickle
import scipy
import math
from sklearn import preprocessing
from skimage.transform import resize
import tensorflow as tf
from keras.callbacks import ModelCheckpoint
from keras.layers import Input, Embedding, concatenate, Flatten, Dense, BatchNormalization, Activation, Conv2D, \
    MaxPooling2D, Dropout, Concatenate, GlobalMaxPool2D, GlobalAvgPool2D, Subtract, Multiply, Lambda
from keras.models import Model, load_model, save_model
from keras.optimizers import SGD, Adam
from keras.utils import Sequence
from keras.losses import binary_crossentropy, mean_squared_error
from losses import binary_focal_loss
from keras.applications.resnet50 import ResNet50
from keras import backend as K
from time import sleep
import matplotlib.pyplot as plt
# from vailtools.callbacks import CyclicLRScheduler

from utils import cam_gps, cam_dom, load_image, save_image, load_and_resize_image, get_image_annotations, get_xml_path, get_sign_image_data

MODEL_NAME = r'tracker/similarity_metric_model.h5'
MODEL_PARAMS_PATH = r'tracker/similarity_model_params.dat'


def build_parser():
    parser = argparse.ArgumentParser(
        description='Trains a small neural network to learn a matching function '
                    'between sign predictions from adjacent video frames.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '-d',
        '--data_folder',
        help='Path to the pre-loaded data directory.',
        default='../../../ARTS-Dataset/rawdata',
    )

    parser.add_argument(
        '-r',
        '--results_directory',
        help='Path to the directory to save performance and debug information.',
        default='/home/vision/Documents/similarity_network_results',
    )

    parser.add_argument(
        '-e',
        '--epochs',
        help='Number of times the network observes the entire training set.',
        default=20,
        type=int,
    )

    parser.add_argument(
        '-b',
        '--batch_size',
        help='Number of samples that are shown to the network each step of training.',
        default=128,
        type=int,
    )

    parser.add_argument(
        '-p',
        '--preprocess',
        help='Indicates whether to run the image preprocessing.',
        action='store_true'
    )

    parser.add_argument(
        '-f',
        '--fast',
        help='Indicates whether to make the dataset very small so the network trains fast for debugging.',
        action='store_true'
    )

    parser.add_argument(
        '-m',
        '--model_type',
        help='Which type of distance metric model is trained (options are DENSE or STACKED).',
        default='DENSE',
        type=str,
    )

    parser.add_argument(
        '-s',
        '--save_failures',
        help='Determines if examples of incorrect predictions on the test data are saved for debugging.',
        action='store_true',
    )

    parser.add_argument(
        '-t',
        '--test',
        help='Test the model without training.',
        action='store_true',
    )

    parser.add_argument(
        '-v',
        '--verbose',
        help='Controls the amount of terminal output.',
        action='count',
        default=1,
    )

    return parser


def train_metric_network(
        data_folder,
        epochs=50,
        batch_size=128,
        fast=False,
        preprocess=False,
        model_type="DENSE",
        save_failures=False,
        results_directory=None,
        test=False,
        verbose=1):

    if preprocess:
        raise NotImplementedError("The ability to preprocess in the training script is not yet implemented.")

    csv_file_path = Path(data_folder) / 'labels.csv'
    train_predictions, val_predictions, test_predictions = load_data(csv_file_path, fast=fast)

    train_gen = DataGenerator(train_predictions,
                              batch_size=batch_size,
                               randomize_order=False)

    val_gen = DataGenerator(val_predictions,
                            batch_size=batch_size,
                               randomize_order=False)

    test_gen = DataGenerator(test_predictions,
                             batch_size=batch_size,
                               randomize_order=False)

    x_sample, y_sample = train_gen.__getitem__(0)

    print(f'Train samples:      {len(train_gen)}')
    print(f'Validation samples: {len(val_gen)}')
    print(f'Test samples:       {len(test_gen)}')

    if not test:
        # create the function used to construct the model
        model_type_args = {'image_dim': x_sample[0].shape[1:]}

        model = build_and_save_model(model_type, model_type_args, 'binary_crossentropy', ['accuracy'])

        checkpoint = ModelCheckpoint(
            MODEL_NAME,
            save_best_only=True,
        )

        # lr_schedule = CyclicLRScheduler(
        #     cycles=5,
        #     total_steps=epochs * len(train_gen),
        # )

        model.fit_generator(
            train_gen,
            epochs=epochs,
            verbose=verbose,
            validation_data=val_gen,
            callbacks=[checkpoint]
        )

    # test_pairs = [t for t in train_predictions if t['image'] == 'data/rawdata/12/127B0SV0000/12000000000023/12000000810025.jpg']
    # temp_datagen = DataGenerator(test_pairs, batch_size=batch_size, visual_noise=True, other_noise=True, mod_features=True)
    # evaluate_model(save_failures, temp_datagen, results_directory, verbose)

    evaluate_model(save_failures, test_gen, results_directory, verbose)


def load_data(csv_path, validation_proportion=0.1, testing_proportion=0.1, fast=False):

    # load the image data and labels from the csv files
    df = pd.read_csv(csv_path)

    # print information about the dataset
    prediction_pairs = []
    for row in df.iterrows():
        data = row[1]
        prediction_pairs.append(data)

    # make the list shorter for testing
    if fast:
        prediction_pairs = prediction_pairs[:300]

    # split the data into training, validation, and testing
    validation_start = int(len(prediction_pairs) * (1-(validation_proportion + testing_proportion)))
    testing_start = int(len(prediction_pairs) * (1-testing_proportion))
    training_data = prediction_pairs[:validation_start]
    validation_data = prediction_pairs[validation_start:testing_start]
    testing_data = prediction_pairs[testing_start:]

    return training_data, validation_data, testing_data


def split_files(data_folder, val_frac=0.1, reverse_order=False):
    files_2012 = sorted((Path(data_folder) / '12').glob('*_all.csv'), reverse=reverse_order)
    files_2013 = sorted((Path(data_folder) / '13').glob('*_all.csv'), reverse=reverse_order)
    files_2014 = sorted((Path(data_folder) / '14').glob('*_all.csv'), reverse=reverse_order)

    val_count_12 = int(len(files_2012) * val_frac)
    val_count_13 = int(len(files_2013) * val_frac)

    train_files = files_2012[:-val_count_12] + files_2013[:-val_count_13]
    val_files = files_2012[-val_count_12:] + files_2013[-val_count_13:]
    test_files = files_2014

    return train_files, val_files, test_files


def get_prediction_pairs_from_files(files, class2ind):
    """Returns a tuple of (predictions_1_list, predictions_2_list)."""

    predictions_1 = []
    predictions_2 = []

    for file in files:
        predictions = get_prediction_pairs(file, class2ind)
        predictions_1 += predictions[0]
        predictions_2 += predictions[1]

    return predictions_1, predictions_2


def get_prediction_pairs(file, class2ind):
    predictions_1, predictions_2 = [], []
    df = pd.read_csv(str(file))
    frames = sorted(df['image'].unique())
    for prev, curr in zip(frames, frames[1:]):

        prev_preds = df[(df.image == prev) & (df['class'].isin(class2ind))]
        prev_cam_lat, prev_cam_lon, _ = cam_gps(prev)
        prev_cam_dom = cam_dom(prev)

        curr_preds = df[(df.image == curr) & (df['class'].isin(class2ind))]
        curr_cam_lat, curr_cam_lon, _ = cam_gps(curr)
        curr_cam_dom = cam_dom(curr)

        for pred_1 in prev_preds.itertuples(index=False):
            for pred_2 in curr_preds.itertuples(index=False):
                if (pred_1[2] not in class2ind) or (pred_2[2] not in class2ind):
                    continue
                predictions_1.append((*pred_1, prev_cam_lat, prev_cam_lon, prev_cam_dom))
                predictions_2.append((*pred_2, curr_cam_lat, curr_cam_lon, curr_cam_dom))

    return [predictions_1, predictions_2]


def save_bad_predictions(model, data_generator, output_directory):
    """Finds any predictions the model performed particularly poorly at and save them to an output directory."""

    output_directory = Path(output_directory)

    for b in range(len(data_generator)):
        test_x, test_y = data_generator.__getitem__(b)
        y_preds = model.predict(test_x)
        y_true = test_y
        error_indices = (abs(y_preds - y_true) > .5)
        error_indices.fill(True)

        for i in range(error_indices.shape[0]):
            if error_indices[i]:
                image_num = b*error_indices.shape[0] + i
                save_directory = output_directory / f"image_number_{image_num}_prediction={y_preds[i][0]:.2f}_correct_prediction={y_true[i][0]}"
                image_data_1 = test_x[3][i]
                image_data_2 = test_x[9][i]
                save_directory.mkdir()
                save_image(save_directory / 'image1.png', image_data_1)
                save_image(save_directory / 'image2.png', image_data_2)


def evaluate_model(save_failures, data_generator, results_directory=None, verbose=False):

    output_directory = Path(results_directory)
    if output_directory.exists():
        for file_path in output_directory.glob('*'):
            file_path.unlink()
        output_directory.rmdir()
    output_directory.mkdir()
    performance_results_save_path = output_directory / 'results.txt'
    results_file = open(performance_results_save_path, 'w')

    K.clear_session()
    model = load_and_build_image_model()

    if save_failures:
        save_bad_predictions(model, data_generator, results_directory)

    # evaluate the model's performance
    preds = []
    labels = []
    for i in range(data_generator.__len__()):
        x, l = data_generator.__getitem__(i)
        preds.append(model.predict(x))
        labels.append(l)
    labels = np.array(labels).flatten()
    preds = np.array(preds).flatten()

    # print out performance metrics
    pred_labels = np.zeros(preds.shape)
    pred_labels[preds > .5] = 1
    accuracy = np.count_nonzero(pred_labels == labels) / len(labels)
    print(f"Accuracy: {accuracy}\n\n", file=results_file)
    print(pd.Series(preds, name='Test Predictions').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]), file=results_file)
    print('\n', file=results_file)
    print(pd.Series(labels, name='Test Labels').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]), file=results_file)
    print('\n', file=results_file)
    print(pd.Series((preds - labels).round(4), name='Test Errors').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]), file=results_file)
    print('\n', file=results_file)
    print(pd.Series(np.abs((preds - labels).round(4)), name='Absolute Test Errors').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]), file=results_file)
    print('\n', file=results_file)

    # make a performance graph
    error_percentiles = pd.Series(np.abs((preds - labels).round(4)), name='Absolute Test Errors').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99])
    error_percentiles = error_percentiles.to_dict()
    plot_colors = ['tab:orange', 'tab:green', 'tab:red', 'tab:purple']
    plot_percentiles = ['50%', '90%', '95%', '99%']
    plt.hist(np.abs(preds - labels), bins=20)
    for percentile, color in zip(plot_percentiles, plot_colors):
        plt.axvline(x=error_percentiles[percentile], label=f'{percentile[:-1]}th Percentile', color=color)
    plt.xlabel("Absolute Error")
    plt.ylabel("Count")
    plt.legend()
    plt.savefig(output_directory / 'error_percentiles.png')


def get_sample_image_generator(data_folder='../../../ARTS-Dataset/rawdata'):
    csv_file_path = Path(data_folder) / 'labels.csv'
    train_predictions, val_predictions, test_predictions = load_data(csv_file_path)

    sample_gen = DataGenerator(train_predictions,
                               batch_size=16,
                               randomize_order=False)

    return sample_gen


class DataGenerator(Sequence):
    def __init__(self,
                 prediction_pairs,
                 batch_size,
                 image_size=(int(1920/2), int(1080/2)),
                 randomize_order=False):

        self.prediction_pairs = prediction_pairs
        self.batch_size = batch_size
        self.image_size = image_size
        self.randomize_order = randomize_order

    def get_all_items(self):
        features = self.get_x(self.prediction_pairs)
        # features = self.apply_noise(features)
        labels = self.get_labels(self.prediction_pairs)
        return features, labels

    def __len__(self):
        return int(len(self.prediction_pairs) / float(self.batch_size))

    def __getitem__(self, idx):
        prediction_pairs = self.prediction_pairs[idx * self.batch_size: (idx + 1) * self.batch_size]
        features = self.get_x(prediction_pairs)
        # features = self.apply_noise(features)
        # if self.mod_features:
        #     features = self.get_modded_batch(features)
        labels = self.get_labels(prediction_pairs)
        return features, labels

    def get_labels(self, prediction_pairs):
        return np.array([[pair[0]] for pair in prediction_pairs])

    def get_x(self, prediction_pairs):

        image_1 = []
        image_2 = []

        for pair in prediction_pairs:

            image_1_path = pair['image']
            image_2_path = pair['image.1']
            if self.randomize_order and random.random() < 0.5:
                image_1_path, image_2_path = image_2_path, image_1_path

            image_data_1 = load_and_resize_image(image_1_path, self.image_size)
            detections = get_image_annotations(image_1_path)
            bbox, depth, bboxes, depths = self.get_all_bboxes_and_depths(pair, detections)
            image_data_1 = self.add_detection_mask(bbox, bboxes, image_data_1)
            image_1.append(image_data_1)

            image_data_2 = load_and_resize_image(image_2_path, self.image_size)
            detections = get_image_annotations(image_2_path)
            bbox, depth, bboxes, depths = self.get_all_bboxes_and_depths(pair, detections, add_str='.1')
            image_data_2 = self.add_detection_mask(bbox, bboxes, image_data_2, seq='2')
            image_2.append(image_data_2)

        # we order image 2 before image 1 since the tracker tracks through the images backwards
        return [np.array(image_2), np.array(image_1)]

    def add_detection_mask(self, det_bbox, bboxes, image_data, seq='1'):

        # debug_data = np.copy(image_data)
        # if random.randint(1, 1) == 1:
        #     random_val = str(random.randint(0, 999999))
        #     # plt.imsave('/home/vision/Documents/similarity_network_results/' + random_val + f'_{seq}_' + '.png', debug_data)
        #     for bbox in bboxes:
        #         debug_data[int(bbox[2]):int(bbox[3]), int(bbox[0]):int(bbox[1]), :] = 0
        #     debug_data[int(det_bbox[2]):int(det_bbox[3]), int(det_bbox[0]):int(det_bbox[1]), :] = [1, 0, 0]
        #     # print(image_data.tolist())
        #     plt.imsave('/home/vision/Documents/similarity_network_results/' + random_val + f'_{seq}_' + '_mod.png', debug_data)
        #     # plt.imshow(debug_data)
        # else:
        #     debug_data[int(det_bbox[2]):int(det_bbox[3]), int(det_bbox[0]):int(det_bbox[1]), :] = 0

        # add a channel to store detection bounding box information
        image_data = np.pad(image_data, ((0, 0), (0, 0), (0, 1)), mode='constant', constant_values=0)

        # label all the other detections in the images as "1"
        for (xmin, xmax, ymin, ymax) in bboxes:
            image_data[int(ymin+.5):int(ymax+.5), int(xmin+.5):int(xmax+.5), 3] = 1

        # label the detection of interest as "-1"
        (xmin, xmax, ymin, ymax) = det_bbox
        image_data[int(ymin+.5):int(ymax+.5), int(xmin+.5):int(xmax+.5), 3] = -1

        return image_data

    def add_depth_mask(self, bboxes, depths, image_data):
        pass

    def get_all_bboxes_and_depths(self, pair, detections, add_str=''):
        """Set the add_str to '.1' to get the second detection from the passed pair."""

        horizontal_scale = self.image_size[0] / 1920
        vertical_scale = self.image_size[1] / 1080

        # get the bounding boxes and depths for all the detections
        bboxes = []
        depths = []
        for detection in detections:
            bbox = (float(detection['xmin']) * horizontal_scale,
                    float(detection['xmax']) * horizontal_scale,
                    float(detection['ymin']) * vertical_scale,
                    float(detection['ymax']) * vertical_scale)
            bboxes.append(bbox)

        # get the bounding box and depth for the detection of interest
        bbox = (float(pair['xmin'+add_str]) * horizontal_scale,
                float(pair['xmax'+add_str]) * horizontal_scale,
                float(pair['ymin'+add_str]) * vertical_scale,
                float(pair['ymax'+add_str]) * vertical_scale)
        depth = None

        return bbox, depth, bboxes, depths


"""--------------These functions save a load models.----------------------------------"""


def get_model_funcs_and_losses():
    model_funcs = {'TEST': build_test_network,
                   'IMAGE_COS': build_pure_image_cosine_network,
                   'IMAGE': build_pure_image_network}
    losses = {'binary_crossentropy': binary_crossentropy,
              'mean_squared_error': mean_squared_error,
              'binary_focal_loss': binary_focal_loss(alpha=.25, gamma=2)}

    return model_funcs, losses


def build_and_save_model(model_type, model_func_args, loss, metrics, starting_weights=None):
    model_funcs, losses = get_model_funcs_and_losses()

    # build the model
    model_func = model_funcs[model_type]
    model = model_func(**model_func_args)
    model.compile(optimizer=Adam(lr=0.00001), loss=losses[loss], metrics=metrics)
    # model.compile(optimizer=Adam(lr=.001), loss=losses[loss], metrics=metrics)
    if starting_weights:
        model.load_weights(starting_weights)

    # save the parameters used to build the model
    build_args = {'model_type': model_type,
                  'model_func_args': model_func_args,
                  'loss': loss,
                  'metrics': metrics,
                  'weights_file': MODEL_NAME}
    pickle.dump(build_args, open(MODEL_PARAMS_PATH, 'wb'))

    return model


def load_and_build_image_model():
    model_funcs, losses = get_model_funcs_and_losses()

    # load the parameters used to build the model
    build_args = pickle.load(open(MODEL_PARAMS_PATH, 'rb'))

    # build the model
    model_func = model_funcs[build_args['model_type']]
    model = model_func(**build_args['model_func_args'])
    loss = losses[build_args['loss']]
    metrics = build_args['metrics']
    model.compile(optimizer=Adam(), loss=loss, metrics=metrics)
    weights_file = build_args['weights_file']
    model.load_weights(weights_file)

    return model


"""----------------------------------------------------------------------------------"""


def build_test_network(image_dim):
    image_data_1 = Input(shape=image_dim)
    image_data_2 = Input(shape=image_dim)

    # image_data = concatenate([image_data_1, image_data_2])
    image_data = image_data_1

    conv = Conv2D(32, kernel_size=(3, 3), activation='relu')(image_data)
    conv = Conv2D(64, (3, 3), activation='relu')(conv)
    conv = MaxPooling2D(pool_size=(2, 2))(conv)
    conv = Dropout(0.25)(conv)
    conv = Conv2D(32, (3, 3), activation='relu')(conv)
    conv = MaxPooling2D(pool_size=(2, 2))(conv)
    conv = Dropout(0.25)(conv)
    conv = Flatten()(conv)
    conv = Dense(512, activation='relu')(conv)
    conv = Dropout(0.25)(conv)
    conv = Dense(64, activation='relu')(conv)

    pred = Dense(150)(conv)
    pred = Activation('relu')(pred)
    pred = Dropout(0.25)(pred)

    for i in range(1):
        pred = Dense(100)(pred)
        pred = Activation('relu')(pred)
        pred = Dropout(0.25)(pred)

    pred = Dense(10)(pred)
    pred = Activation('relu')(pred)

    pred = Dense(1)(pred)
    pred = Activation('sigmoid')(pred)

    return Model(
        inputs=[image_data_1, image_data_2],
        outputs=pred,
    )


def build_pure_image_cosine_network(image_dim):
    def cosine_distance(vests):
        x, y = vests
        x = K.l2_normalize(x, axis=-1)
        y = K.l2_normalize(y, axis=-1)
        return -K.mean(x * y, axis=-1, keepdims=True)

    def cos_dist_output_shape(shapes):
        shape1, shape2 = shapes
        return shape1[0], 1

    image_data_1 = Input(shape=image_dim)
    image_data_2 = Input(shape=image_dim)

    visual_model = ResNet50(input_shape=image_dim, weights=None, include_top=False)
    intermediate_layer = Model(inputs=visual_model.input, outputs=visual_model.get_layer('activation_48').output)

    x1 = intermediate_layer(image_data_1)
    x2 = intermediate_layer(image_data_2)

    x1 = Concatenate(axis=-1)([GlobalMaxPool2D()(x1), GlobalAvgPool2D()(x1)])
    x2 = Concatenate(axis=-1)([GlobalMaxPool2D()(x2), GlobalAvgPool2D()(x2)])

    x3 = Subtract()([x1, x2])
    x3 = Multiply()([x3, x3])

    x1_ = Multiply()([x1, x1])
    x2_ = Multiply()([x2, x2])
    x4 = Subtract()([x1_, x2_])

    x5 = Lambda(cosine_distance, output_shape=cos_dist_output_shape)([x1, x2])

    x = Concatenate(axis=-1)([x5, x4, x3])

    x = Dense(100, activation="relu")(x)
    x = Dropout(0.01)(x)
    pred = Dense(1, activation="sigmoid")(x)

    return Model(
        inputs=[image_data_1, image_data_2],
        outputs=pred,
    )


def build_pure_image_network(image_dim):
    image_data_1 = Input(shape=image_dim)
    image_data_2 = Input(shape=image_dim)

    visual_model = ResNet50(input_shape=image_dim, weights=None, include_top=False)
    intermediate_layer = Model(inputs=visual_model.input, outputs=visual_model.get_layer('activation_48').output)

    x1 = intermediate_layer(image_data_1)
    x2 = intermediate_layer(image_data_2)

    # print(x1.shape)
    # print(x2.shape)

    x = Concatenate()([GlobalMaxPool2D()(x1), GlobalMaxPool2D()(x2)])
    # print(x.shape)
    # print(GlobalMaxPool2D()(x1).shape)
    x = Dense(500, activation="relu")(x)
    x = Dropout(0.1)(x)
    x = Dense(50, activation="relu")(x)
    x = Dropout(0.1)(x)
    pred = Dense(1, activation="sigmoid")(x)

    return Model(
        inputs=[image_data_1, image_data_2],
        outputs=pred,
    )


if __name__ == '__main__':
    args = build_parser().parse_args()
    train_metric_network(**vars(args))
