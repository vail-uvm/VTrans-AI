#!/usr/bin/env python

"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
from pathlib import Path
from multiprocessing import Pool
from functools import partial

import numpy as np
import pandas as pd
from geopy.distance import distance
from scipy.optimize import linear_sum_assignment
from scipy.spatial.distance import cdist
from sklearn.cluster import dbscan

from distance_metric_model import get_superclass


def parse_args():
    parser = argparse.ArgumentParser(
        description='Evaluates predictions output by the object tracker.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        'folder',
        help='Path to a directory containing sign predictions and annotations.',
    )
    parser.add_argument(
        '--gps_threshold',
        help='Threshold on GPS error that localizes detections.',
        default=70.0,
        type=float,
    )
    parser.add_argument(
        '--class_dist',
        help='Distance penalty added if classes do not match.',
        default=10.0,
        type=float,
    )

    return parser.parse_args()


def evaluate_road_segment_2(annotation_path, prediction_path, eps, cls_dist):

    y_true = pd.read_csv(annotation_path)
    y_true.columns = [x.strip() for x in y_true.columns]
    # y_true = y_true[['class', 'lat', 'lon']]
    # y_true['class'] = y_true['class'].apply(get_superclass)  # convert dataframe to superclass

    y_pred = pd.read_csv(prediction_path)
    y_pred.columns = [x.strip() for x in y_pred.columns]
    # y_pred = y_pred[['class', 'lat', 'lon']]
    # y_pred['class'] = y_pred['class'].apply(get_superclass)  # convert dataframe to superclass

    dists = cdist(
        y_true[['lat', 'lon', 'class']],
        y_pred[['lat', 'lon', 'class']],
        lambda a, b: class_dist(a, b, cls_dist),
    )

    y_true.columns = ['true_' + x for x in y_true.columns]
    y_pred.columns = ['pred_' + x for x in y_pred.columns]

    real_signs = []
    pred_signs = []
    match_dists = []
    pairing = get_pairing(dists, eps)
    while pairing:
        real_sign = y_true.loc[pairing[0]]
        pred_sign = y_pred.loc[pairing[1]]

        real_signs.append(real_sign)
        pred_signs.append(pred_sign)
        match_dists.append(dists[pairing])

        y_true.drop(index=pairing[0], inplace=True)
        y_pred.drop(index=pairing[1], inplace=True)

        # make the selected distance large so it isn't selected again
        dists[pairing[0], :] = np.inf
        dists[:, pairing[1]] = np.inf

        pairing = get_pairing(dists, eps)

    true_matches = pd.DataFrame(real_signs)
    pred_matches = pd.DataFrame(pred_signs)
    true_matches.reset_index(drop=True, inplace=True)
    pred_matches.reset_index(drop=True, inplace=True)

    pairs = pd.concat([true_matches, pred_matches, pd.DataFrame(match_dists, columns=['distance'])], axis=1)

    save_segment_csvs(annotation_path, pairs, y_pred, y_true)

    segment_id = Path(annotation_path).stem.split('_')[0]
    segment_path = Path(*Path(annotation_path).parts[:-1])
    p = segment_path / f'{segment_id}'
    print(f"Results on {p}...")
    print(f"True Positives {len(pairs)}")
    print(f"False Positives: {len(y_pred)}.")
    print(f"False Negatives: {len(y_true)}.")

    return pairs, y_pred, y_true


def class_dist(a, b, add_dist=10):
    geo_dist = float(distance(a[:2], b[:2]).meters)
    class_dist = 0 if a[2] == b[2] else add_dist
    return geo_dist + class_dist


def get_pairing(dists, threshold):
    """Returns a tuple of the (row, col) indices of the lowest dist if one exists below the threshold."""

    min_ind = np.where(dists == np.amin(dists))
    min_ind = list(zip(min_ind[0], min_ind[1]))[0]

    if dists[min_ind] < threshold:
        return min_ind
    else:
        return None


def save_segment_csvs(annotation_path, pairs, y_pred, y_true):
    segment_id = Path(annotation_path).stem.split('_')[0]
    segment_path = Path(*Path(annotation_path).parts[:-1])

    try:
        # print("sort 1")
        pairs.sort_values('true_image').to_csv(segment_path / f'{segment_id}_true_positives.csv')
        # print("sort 2")
        y_pred.sort_values('pred_image').to_csv(segment_path / f'{segment_id}_false_positives.csv')
        # print("sort 3")
        y_true.sort_values('true_image').to_csv(segment_path / f'{segment_id}_false_negatives.csv')
    except Exception:
        p = segment_path / f'{segment_id}'
        print(f"Error on {p}.")


def main():
    args = parse_args()
    path = Path(args.folder)

    job = partial(evaluate_road_segment_2, eps=args.gps_threshold, cls_dist=args.class_dist)
    args = zip(sorted(path.glob('*_unique.csv')), sorted(path.glob('*_all_signs.csv')))
    # args = zip(['../data/rawdata/13/138A0N9CH00_unique.csv'], ['../data/rawdata/13/138A0N9CH00_all_signs.csv'])
    # args = zip(['../data/rawdata/12/127B0SV0000_unique.csv'], ['../data/rawdata/12/127B0SV0000_all_signs.csv'])
    # args = zip(['../data/rawdata/13/sample_unique.csv'], ['../data/rawdata/13/sample_all_signs.csv'])
    # args = zip(['../data/rawdata/13/138A0N9CI00_unique.csv'], ['../data/rawdata/13/138A0N9CI00_all_signs.csv'])
    # args = zip(['../data/rawdata/14/14790WXLJ00_unique.csv'], ['../data/rawdata/14/14790WXLJ00_all_signs.csv'])
    # tiny bad: 14790WXLJ00
    # args = zip(['../data/rawdata/12/127K0LV0000_unique.csv'], ['../data/rawdata/12/127K0LV0000_all_signs.csv'])

    with Pool() as pool:
        results = pool.starmap(job, args)
    # evaluate_road_segment('../data/rawdata/14/14790WXLJ00_unique.csv', '../data/rawdata/14/14790WXLJ00_all_signs.csv', eps)

    true_positives, false_positives, false_negatives = zip(*results)

    true_positives = pd.concat(true_positives, ignore_index=True).sort_values('distance')
    true_positives.to_csv(path / f'{path.stem}_tracker_evaluation.csv')
    false_positives = pd.concat(false_positives, ignore_index=True)
    false_negatives = pd.concat(false_negatives, ignore_index=True)

    tp_count = len(true_positives)
    fn_count = len(false_negatives)
    fp_count = len(false_positives)

    with open(path / f'{path.stem}_gps_evaluation.txt', 'a') as f:
        print(pd.Timestamp.now(), file=f)
        print(f'Annotation Count: {tp_count + fn_count}', file=f)
        print(f'Prediction Count: {tp_count + fp_count}', file=f)
        print(f'True Positives:   {tp_count}', file=f)
        print(f'False Negatives:  {fn_count}', file=f)
        print(f'False Positives:  {fp_count}', file=f)
        print(true_positives.distance.describe(), file=f)
        print('\n\n', file=f)


if __name__ == '__main__':
    main()
