"""
Creates a dataset for training the similarity network.

Pre-loads and saves images of cropped signs and their corresponding detection features.
This is used to to create a data set to train the merged metric network.
Creates a directory and places all of the images in it along with a .csv file listing the corresponding features and
file paths.
"""

import argparse
import pandas as pd
import scipy.ndimage, scipy.misc
import random
from functools import partial
from multiprocessing import Pool

from utils import *


def build_parser():
    parser = argparse.ArgumentParser(
        description="Builds the dataset for training the visual metric learning network.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "dataset_directory",
        help="The directory of the dataset which will be used to generate the files.",
        type=str
    )

    parser.add_argument(
        "output_directory",
        help="The directory to which the files will be outputted.",
        type=str,
    )

    parser.add_argument(
        "-i",
        "--no_images",
        help="Saves detection pair dataset without images.",
        action="store_true"
    )

    parser.add_argument(
        "-g",
        "--single_images",
        help="Saves signs as individual images instead of stacked images.",
        action="store_true"
    )

    parser.add_argument(
        "-m",
        "--match_by_class",
        help="Labels images based on whether the sign classes match.\n"
             "By default signs are labeled based on whether they are the same physical sign.",
        action="store_true"
    )

    parser.add_argument(
        "-s",
        "--size",
        help="The size of the images to produce.",
        type=int,
        default=32
    )

    parser.add_argument(
        "-n",
        "--number",
        help="The number of csv files used to generate data from.",
        type=int,
        default=9999999,
    )

    return parser


def generate_data_directory(dataset_directory, output_directory, size, match_by_class, single_images, no_images, number):

    # check validity of output directory
    output_directory = Path(output_directory)
    if output_directory.exists():
        raise FileExistsError("Directory already exists. "
                              "\nPlease specify a directory that doesn't already exist to place the new data in.")
    output_directory.mkdir()

    files = get_csv_files(dataset_directory)
    if number < len(files)-1:
        files = files[:number]

    process_metric_learning_data(files,
                                 'data/rawdata/tracker_labels.txt',
                                 size,
                                 match_by_class,
                                 single_images,
                                 no_images,
                                 output_directory)

    csv_filepaths = Path(output_directory).glob('**/*.csv')
    data = []
    for path in csv_filepaths:
        try:
            df = pd.read_csv(path)
        except Exception as e:
            print(e)
            continue
        data.append(df)
    master_dataframe = pd.concat(data)
    master_dataframe.to_csv(output_directory / 'labels.csv', index=False)


def get_csv_files(data_folder):
    files = sorted((Path(data_folder) / '12').glob('*_all.csv'))
    files += sorted((Path(data_folder) / '13').glob('*_all.csv'))
    files += sorted((Path(data_folder) / '14').glob('*_all.csv'))
    return files


def process_metric_learning_data(files, class_file, size, match_by_class, single_images, no_images, output_directory):
    with open(class_file, 'r') as f:
        classes = [x.strip() for x in f]
    class2ind = {c: i for c, i in zip(classes, range(len(classes)))}
    size = (size, size)

    # For testing
    # process_without_images(files[0], class2ind=class2ind, match_by_class=False, output_directory=output_directory)

    if no_images:
        job = partial(process_without_images,
                      class2ind=class2ind,
                      match_by_class=match_by_class,
                      output_directory=output_directory)
    elif single_images:
        job = partial(process_single_images,
                      class2ind=class2ind,
                      size=size,
                      match_by_class=match_by_class,
                      output_directory=output_directory)
    else:
        job = partial(process_stacked_images,
                      class2ind=class2ind,
                      size=size,
                      match_by_class=match_by_class,
                      output_directory=output_directory)

    with Pool() as pool:
        pool.map(job, files)

    print("Done processing jobs.")

    return


def process_without_images(file, class2ind, match_by_class, output_directory):
    print("Processing ", file)

    # make the directory
    output_directory = output_directory / Path(file).stem
    output_directory.mkdir()

    image_number = 0
    output_csv_file_path = output_directory / "labels.csv"
    df = pd.read_csv(str(file))
    frames = sorted(df['image'].unique())
    image_paths_and_labels = []

    for prev, curr in zip(frames, frames[1:]):
        prev_preds = df[(df.image == prev) & (df['class'].isin(class2ind))]
        prev_cam_lat, prev_cam_lon, _ = cam_gps(prev)
        prev_cam_dom = cam_dom(prev)

        curr_preds = df[(df.image == curr) & (df['class'].isin(class2ind))]
        curr_cam_lat, curr_cam_lon, _ = cam_gps(curr)
        curr_cam_dom = cam_dom(curr)

        for pred_1 in prev_preds.itertuples(index=False):
            for pred_2 in curr_preds.itertuples(index=False):
                if (pred_1[2] not in class2ind) or (pred_2[2] not in class2ind):
                    continue
                # throw out signs with unknown class
                if pred_1[2] == "unknown" or pred_2[2] == "unknown":
                    continue

                # generate a label which matches based on either class of they physical sign
                if match_by_class:
                    label = 0 if pred_1[2] == pred_2[2] else 1
                else:
                    label = 0 if same_annotation(pred_1, pred_2) else 1

                # append to the list of images file paths, labels, and classes
                image_paths_and_labels.append([label,
                                               "No image", *pred_1, prev_cam_lat, prev_cam_lon, prev_cam_dom,
                                               "No image", *pred_2, curr_cam_lat, curr_cam_lon, curr_cam_dom])

                image_number += 1

    # save the data generated
    pairs = pd.DataFrame(image_paths_and_labels)
    pairs.columns = ["Label", "Prev Sign Image Path"] + list(df.columns) + ["Prev Cam Lat", "Prev Cam Lon", "Prev Cam DOM"] + \
                    ["Curr Sign Image Path"] + list(df.columns) + ["Curr Cam Lat", "Curr Cam Lon", "Curr Cam DOM"]
    remove_labels = ['assembly', 'side']  # temporarily remove these since not all annotations have them
    for label in remove_labels:
        if label in pairs.columns:
            pairs = pairs.drop(columns=[label])
    pairs.to_csv(output_csv_file_path, index=False)

    print("Done Processing ", file)


def process_single_images(file, class2ind, size, match_by_class, output_directory, pad=0.3):
    print("Processing ", file)

    # make the directory
    output_directory = output_directory / Path(file).stem
    output_directory.mkdir()

    image_number = 0
    output_csv_file_path = output_directory / "labels.csv"
    df = pd.read_csv(str(file))
    frames = sorted(df['image'].unique())
    image_paths_and_labels = []

    for prev, curr in zip(frames, frames[1:]):
        prev_preds = df[(df.image == prev) & (df['class'].isin(class2ind))]
        prev_cam_lat, prev_cam_lon, _ = cam_gps(prev)
        prev_cam_dom = cam_dom(prev)

        curr_preds = df[(df.image == curr) & (df['class'].isin(class2ind))]
        curr_cam_lat, curr_cam_lon, _ = cam_gps(curr)
        curr_cam_dom = cam_dom(curr)

        for pred_1 in prev_preds.itertuples(index=False):
            for pred_2 in curr_preds.itertuples(index=False):
                if (pred_1[2] not in class2ind) or (pred_2[2] not in class2ind):
                    continue
                # throw out signs with unknown class
                if pred_1[2] == "unknown" or pred_2[2] == "unknown":
                    continue

                image_data_1 = get_sign_image_data(pred_1[0], *pred_1[5:9], size, pad)
                image_data_2 = get_sign_image_data(pred_2[0], *pred_2[5:9], size, pad)

                # generate a label which matches based on either class of they physical sign
                if match_by_class:
                    label = 0 if pred_1[2] == pred_2[2] else 1
                else:
                    label = 0 if same_annotation(pred_1, pred_2) else 1

                # determine the file paths at which to save the signs
                image_1_file_path = Path(output_directory) / Path(f"single_{image_number}_a.png")
                image_2_file_path = Path(output_directory) / Path(f"single_{image_number}_b.png")

                # temporary code to make sure there aren't any bugs saving the same image twice
                if image_1_file_path.exists():
                    raise FileExistsError(f"Path {image_1_file_path} already exists.")
                if image_2_file_path.exists():
                    raise FileExistsError(f"Path {image_2_file_path} already exists.")

                # save the sign images
                scipy.misc.imsave(image_1_file_path, image_data_1)
                scipy.misc.imsave(image_2_file_path, image_data_2)

                # append to the list of images file paths, labels, and classes
                image_paths_and_labels.append([label,
                                               image_1_file_path, *pred_1, prev_cam_lat, prev_cam_lon, prev_cam_dom,
                                               image_2_file_path, *pred_2, curr_cam_lat, curr_cam_lon, curr_cam_dom])

                image_number += 1

    # save the data generated
    pairs = pd.DataFrame(image_paths_and_labels)
    pairs.columns = ["Label", "Prev Sign Image Path"] + list(df.columns) + ["Prev Cam Lat", "Prev Cam Lon", "Prev Cam DOM"] + \
                    ["Curr Sign Image Path"] + list(df.columns) + ["Curr Cam Lat", "Curr Cam Lon", "Curr Cam DOM"]
    remove_labels = ['assembly', 'side']  # temporarily remove these since not all annotations have them
    for label in remove_labels:
        if label in pairs.columns:
            pairs = pairs.drop(columns=[label])
    pairs.to_csv(output_csv_file_path, index=False)

    print("Done Processing ", file)


def process_stacked_images(file, class2ind, size, match_by_class, output_directory, pad=0.45):
    print("Processing ", file)
    output_directory = output_directory / Path(file).stem
    output_directory.mkdir()
    output_csv_file_path = output_directory / "labels.csv"
    df = pd.read_csv(str(file))
    frames = sorted(df['image'].unique())
    image_paths_and_labels = []

    for prev, curr in zip(frames, frames[1:]):
        prev_preds = df[(df.image == prev) & (df['class'].isin(class2ind))]
        prev_cam_lat, prev_cam_lon, _ = cam_gps(prev)
        prev_cam_dom = cam_dom(prev)

        curr_preds = df[(df.image == curr) & (df['class'].isin(class2ind))]
        curr_cam_lat, curr_cam_lon, _ = cam_gps(curr)
        curr_cam_dom = cam_dom(curr)

        for pred_1 in prev_preds.itertuples(index=False):
            for pred_2 in curr_preds.itertuples(index=False):
                if (pred_1[2] not in class2ind) or (pred_2[2] not in class2ind):
                    continue

                vertically_stacked_image_data = get_vertically_stacked_sign_data_from_predictions(pred_1, pred_2, size,
                                                                                                  pad)

                # generate a label which matches based on either class or the physical sign
                if match_by_class:
                    label = 0 if pred_1[2] == pred_2[2] else 1
                else:
                    label = 0 if same_annotation(pred_1, pred_2) else 1

                # select and unused file path with which to create the image
                image_file_path = Path(output_directory) / Path("stack" + str(random.randint(1, 100000)) + ".png")
                while image_file_path.exists():
                    image_file_path = Path(
                        Path(output_directory) / Path("stack" + str(random.randint(1, 100000)) + ".png"))
                scipy.misc.imsave(image_file_path, vertically_stacked_image_data)

                # append to the list of images file paths, labels, and classes
                image_paths_and_labels.append([label, image_file_path,
                                               *pred_1, prev_cam_lat, prev_cam_lon, prev_cam_dom,
                                               *pred_2, curr_cam_lat, curr_cam_lon, curr_cam_dom])

    # save the data generated
    df = pd.DataFrame(image_paths_and_labels)
    df.to_csv(output_csv_file_path, index=False)

    print("Done Processing ", file)


def same_annotation(sign_1, sign_2, tolerance=.00005):  # .000000001
    """Returns a boolean indicating whether the passed signs the same latitude, longitude, and class."""

    class_ind = 2
    lat_ind = 3
    lon_ind = 4

    return sign_1[class_ind] == sign_2[class_ind] \
           and abs(float(sign_1[lon_ind]) - float(sign_2[lon_ind])) < tolerance \
           and abs(float(sign_1[lat_ind]) - float(sign_2[lat_ind])) < tolerance


def flatten_nested_list(x):
    return [b for a in x for b in a]


if __name__ == '__main__':
    args = build_parser().parse_args()
    generate_data_directory(**vars(args))
