#!/usr/bin/env python

"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import argparse
from pathlib import Path
from multiprocessing import Pool
from functools import partial

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats

from distance_metric_model import get_superclass
from evaluate_tracker import evaluate_road_segment, class_dist


def parse_args():
    parser = argparse.ArgumentParser(
        description='Evaluates predictions output by the object tracker.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        'folder',
        help='Path to a directory containing the predictions for the different GPS methods.',
    )
    parser.add_argument(
        '--gps_threshold',
        help='Threshold on GPS error that localizes detections.',
        default=15.0,
        type=float,
    )
    parser.add_argument(
        '-c',
        '--collapse',
        action='store_true',
        help='Collapses redundant entries in csv files using the "Sign ID" column if it exists.'
             '\nOverwrites the existing file.',
    )
    parser.add_argument(
        '-m',
        '--manual',
        action='store_true',
        help='Runs manual function instead of normal evaluation.'
             '\nUseful when developing or experimenting with evaluation methods.',
    )

    return parser.parse_args()


def collapse(file_path):
    data = pd.read_csv(file_path)
    data = data.drop_duplicates('Sign ID')
    data.to_csv(file_path, index=False)


def manual(directory, gps_threshold):
    real = directory / 'real_signs'
    real = real / '138A0N9CH00_unique.csv'
    mrf_dir = directory / 'mrf'
    mrf1 = mrf_dir / '138A0N9CH00_mrfOutput_01.csv'
    mrf2 = mrf_dir / '138A0N9CH00_mrfOutput_001.csv'

    a_count, p_count, chunks = evaluate_road_segment(mrf1, real, gps_threshold)
    results = chunks.sort_values('distance')

    print(pd.Timestamp.now())
    print(f'Annotation Count: {a_count}')
    print(f'Prediction Count: {p_count}')
    print(f'True Positives:   {len(results)}')
    print(f'False Negatives:  {a_count - len(results)}')
    print(f'False Positives:  {p_count - len(results)}')
    print(results.distance.describe())
    print('\n\n')

    a_count, p_count, chunks = evaluate_road_segment(mrf2, real, gps_threshold)
    results = chunks.sort_values('distance')

    print(pd.Timestamp.now())
    print(f'Annotation Count: {a_count}')
    print(f'Prediction Count: {p_count}')
    print(f'True Positives:   {len(results)}')
    print(f'False Negatives:  {a_count - len(results)}')
    print(f'False Positives:  {p_count - len(results)}')
    print(results.distance.describe())
    print('\n\n')


def main():
    args = parse_args()
    path = Path(args.folder)

    if args.collapse:
        triangulated_preds = sorted(path.glob('triangulated/*'))
        for path in triangulated_preds:
            collapse(path)

    elif args.manual:
        manual(path, args.gps_threshold)

    else:
        foi_preds = sorted(path.glob('foi/*'))
        weighted_avg_preds = sorted(path.glob('weighted_avg/*'))
        triangulated_preds = sorted(path.glob('triangulated/*'))
        mrf_preds = sorted(path.glob('mrf/*'))
        real_signs = sorted(path.glob('real_signs/*'))

        job = partial(evaluate_road_segment, eps=args.gps_threshold)
        arg_list = [zip(foi_preds, real_signs),
                    zip(weighted_avg_preds, real_signs),
                    zip(triangulated_preds, real_signs),
                    zip(mrf_preds, real_signs)]
        names = ["Frame of Interest", "Weighted Average", "Triangulation", "MRF"]

        # arg_list = [zip(weighted_avg_preds, real_signs)]
        # names = ["Weighted Average"]

        for args, name in zip(arg_list, names):
            with Pool() as pool:
                results = pool.starmap(job, args)

            annotation_counts, prediction_counts, chunks = zip(*results)

            a_count = np.sum(annotation_counts)
            p_count = np.sum(prediction_counts)
            results = pd.concat(chunks, ignore_index=True).sort_values('distance')

            with open(path / f'{path.stem}_gps_evaluation.txt', 'a') as f:
                print(name, file=f)
                print(pd.Timestamp.now(), file=f)
                print(f'Annotation Count: {a_count}', file=f)
                print(f'Prediction Count: {p_count}', file=f)
                print(f'True Positives:   {len(results)}', file=f)
                print(f'False Negatives:  {a_count - len(results)}', file=f)
                print(f'False Positives:  {p_count - len(results)}', file=f)
                print(results.distance.describe(), file=f)
                print('\n\n', file=f)

            # print(results.distance)
            # print(type(results.distance))
            # print(list(results.distance))

            # create a histogram of GPS errors
            bins = 20
            plt.xlim(0, 35)
            plt.xlabel("Error (meters)")
            plt.ylabel("Probability")
            plt.title("Distribution of Distances Between Predicted and Annotated Sign Locations")
            plt.hist(results.distance, density=True, bins=bins)

            # create a KDE
            xx = np.linspace(0, bins-1, 1000)
            kde = stats.gaussian_kde(sorted(list(results.distance)))
            plt.plot(xx, kde(xx))
            plt.show()


if __name__ == '__main__':
    main()
