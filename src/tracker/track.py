#!/usr/bin/env python

"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
from pathlib import Path

import pandas as pd

from tracklets import track


def parse_args():
    parser = argparse.ArgumentParser(description='Tracking script.')

    parser.add_argument(
        'detections_path',
        help='Path to a CSV file (detections from the network).',
    )

    parser.add_argument(
        '-t',
        '--tracker_type',
        help='The type of tracker to use. Options are ARTS, OPENCV (defaults to ARTS).',
        default='ARTS',
        type=str,
    )

    parser.add_argument(
        '-c',
        '--cost-threshold',
        help='Threshold on cost assignment to forcibly split tracklets (defaults to 0.98).',
        default=0.98,
        type=float,
    )

    parser.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        help='Determines the level of terminal output.',
    )

    parser.add_argument(
        '-s',
        '--save_tracklets',
        action='store_true',
        help='Determines if the tracklets are save to a csv file.',
    )

    parser.add_argument(
        '-d',
        '--debug',
        action='store_true',
        help='Determines whether debug images will be generated.',
    )

    parser.add_argument(
        '--debug_directory',
        help='The directory which the debug images are output to (default is /home/vision/Documents/debug).',
        default='/home/vision/Documents/debug',
        type=str,
    )

    return parser.parse_args()


def main():
    args = parse_args()

    signs = track(**vars(args))
    signs = pd.DataFrame(signs).sort_values(['lat', 'lon'])

    path = Path(args.detections_path.replace('detections', 'all'))
    signs.to_csv(path.parent / f'{path.stem}_signs.csv', index=False)


if __name__ == '__main__':
    main()
