"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This code feeds a csv file as input to the distance metric model and adds the predicted distance values to that file.
"""

import numpy as np
import pandas as pd
from keras.models import load_model

from pure_image_similarity_metric import MODEL_NAME, DataGenerator
from utils import get_sign_image_data, load_image

CSV_FILE = "/home/vision/Documents/debug/csv_input.csv"
PREPROCESSED_FILE = "/home/vision/Documents/debug/preprocessed.csv"
# class_file='/home/vision/VTransVision/VTrans-AI/src/data/rawdata/tracker_labels.txt'


def fill_in_distances():
    data = pd.read_csv(CSV_FILE)
    distances, preprocessed_features = compute_distances(data)
    data['Distance Metric'] = distances
    data.to_csv(CSV_FILE, index=False)

    preprocessed_features = np.hstack((preprocessed_features[0],
                                       preprocessed_features[1],
                                       preprocessed_features[3],
                                       preprocessed_features[4]))
    preprocessed_features = pd.DataFrame(preprocessed_features)
    # preprocessed_features.columns = ["Long Shift",
    #                                  "Lat Shift",
    #                                  "Cam DOM Shift",
    #                                  "Left Shift",
    #                                  "Right Shift",
    #                                  "Bottom Shift",
    #                                  "Top Shift",
    #                                  "T Class",
    #                                  "P Class"]
    preprocessed_features.to_csv(PREPROCESSED_FILE, index=False)


def compute_distances(input_data, image_dimensions=(32, 32),):

    # build the arrays in which to place the data
    num_inputs = input_data.shape[0]
    features_1 = np.zeros((num_inputs, 10))
    classes_1 = np.zeros((num_inputs, 1))
    image_datas_1 = np.zeros((num_inputs, *image_dimensions, 3))
    features_2 = np.zeros((num_inputs, 10))
    classes_2 = np.zeros((num_inputs, 1))
    image_datas_2 = np.zeros((num_inputs, *image_dimensions, 3))

    # fill the data arrays with the data from the dataframe
    for i, row in input_data.iterrows():

        features_1[i, :] = row["T Sign Longitude":"T Camera DOM"].values
        classes_1[i] = row["T Class"]
        if row["T Sign Image"] == "NONE":
            image_data = get_sign_image_data(row['Tracklet Image'],
                                             int(row['T BBOX Left']),
                                             int(row['T BBOX Right']),
                                             int(row['T BBOX Bottom']),
                                             int(row['T BBOX Top']))
        else:
            image_data = load_image(row["T Sign Image"])
        image_datas_1[i, :, :, :] = image_data

        features_2[i, :] = row["P Sign Longitude":"P Camera DOM"].values
        classes_2[i] = row["P Class"]
        if row["P Sign Image"] == "NONE":
            image_data = get_sign_image_data(row['Prediction Image'],
                                             int(row['P BBOX Left']),
                                             int(row['P BBOX Right']),
                                             int(row['P BBOX Bottom']),
                                             int(row['P BBOX Top']))
        else:
            image_data = load_image(row["P Sign Image"])
        image_datas_2[i, :, :, :] = image_data

    # compute the distance values
    distance_metric_model = load_model(MODEL_NAME)
    modded_features = DataGenerator.get_modded_batch(None, [features_1,
                                                            classes_1,
                                                            image_datas_1,
                                                            features_2,
                                                            classes_2,
                                                            image_datas_2])
    distances = distance_metric_model.predict(modded_features)

    return distances, modded_features


if __name__ == "__main__":
    fill_in_distances()
