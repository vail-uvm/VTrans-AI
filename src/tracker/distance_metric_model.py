"""
Trains a neural network to predict the "distance" between a pair of sign predictions,
which may then be used in the object tracking system in order to assign new predictions
to tracklets.

TODO: Add correct noise to manually modified features.  Make it so detection features are modded after adding noise.
    Improve manual modification of features.  use bbox width and heights, use sign longitude and latitude difference
    Make it so I don't need to manually change input sizes in the functions that build the metric networks.
    Change the order of the features in the data generator to be consistent.
    Make it so image shape is not hardcoded into merged models.
    Add noise to image data.
    Implement -p and -m and -d argparse arguments.
    Add a get_data_generator() function which can then be used by the tracker.
"""


import argparse
from functools import partial
from multiprocessing import Pool
from pathlib import Path
from ast import literal_eval
import random

import numpy as np
import pandas as pd
import pickle
import scipy
import math
from sklearn import preprocessing
from skimage.transform import resize
import tensorflow as tf
from keras.callbacks import ModelCheckpoint
from keras.layers import Input, Embedding, concatenate, Flatten, Dense, BatchNormalization, Activation, Conv2D, \
    MaxPooling2D, Dropout, Concatenate, GlobalMaxPool2D, GlobalAvgPool2D, Subtract, Multiply, Lambda
from keras.models import Model, load_model, save_model
from keras.optimizers import SGD, Adam
from keras.utils import Sequence
from keras.losses import binary_crossentropy, mean_squared_error
from losses import binary_focal_loss
# from vailtools.callbacks import CyclicLRScheduler
from keras.applications.resnet50 import ResNet50
from keras import backend as K
from time import sleep
import matplotlib.pyplot as plt

from utils import cam_gps, cam_dom, load_image, save_image, load_detections, get_xml_path, get_sign_image_data

MODEL_NAME = r'tracker/distance_metric_model.h5'
FROZEN_MODEL_NAME = r'tracker/siamese_frozen_model.h5'
MODEL_PARAMS_PATH = r'tracker/model_params.dat'
NOISES_FILE_PATH = r'tracker/noises.dat'
MATRIX_DIM = (10, 10)
MATRIX_THIRD_DIM = 21


def build_parser():
    parser = argparse.ArgumentParser(
        description='Trains a small neural network to learn a matching function '
                    'between sign predictions from adjacent video frames.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        '-d',
        '--data_folder',
        help='Path to the pre-loaded data directory.',
        default='../../../ARTS-Dataset/rawdata',
    )

    parser.add_argument(
        '-e',
        '--epochs',
        help='Number of times the network observes the entire training set.',
        default=20,
        type=int,
    )

    parser.add_argument(
        '-b',
        '--batch_size',
        help='Number of samples that are shown to the network each step of training.',
        default=128,
        type=int,
    )

    parser.add_argument(
        '-p',
        '--preprocess',
        help='Indicates whether to run the image preprocessing.',
        action='store_true'
    )

    parser.add_argument(
        '-m',
        '--model_type',
        help='Which type of distance metric model is trained (options are DENSE or STACKED).',
        default='DENSE',
        type=str,
    )

    parser.add_argument(
        '-s',
        '--save_failures',
        help='Determines if examples of incorrect predictions on the test data are saved for debugging.',
        action='store_true',
    )

    parser.add_argument(
        '-v',
        '--verbose',
        help='Controls the amount of terminal output.',
        action='count',
        default=1,
    )

    return parser


def train_metric_network(
        data_folder,
        class_file='data/rawdata/tracker_labels.txt',
        epochs=50,
        batch_size=128,
        preprocess=False,
        model_type="DENSE",
        save_failures=False,
        verbose=1):

    if preprocess:
        raise NotImplementedError("The ability to preprocess in the training script is not yet implemented.")

    csv_file_path = Path(data_folder) / 'labels.csv'
    train_predictions, val_predictions, test_predictions = load_data(csv_file_path)

    class2ind, superclass2ind = get_class_superclass_ind(class_file)

    train = MergedDataGenerator(train_predictions,
                                class2ind,
                                superclass2ind,
                                batch_size=batch_size,
                                visual_noise=True,
                                other_noise=True,
                                mod_features=True)

    val = MergedDataGenerator(val_predictions,
                              class2ind,
                              superclass2ind,
                              batch_size=batch_size,
                              visual_noise=True,
                              other_noise=True,
                              mod_features=True)

    test = MergedDataGenerator(test_predictions,
                               class2ind,
                               superclass2ind,
                               batch_size=batch_size,
                               visual_noise=True,
                               other_noise=True,
                               mod_features=True)

    x_sample, y_sample = train.__getitem__(0)

    print(f'Train samples:      {len(train)}')
    print(f'Validation samples: {len(val)}')
    print(f'Test samples:       {len(test)}')

    # create the function used to construct the model
    model_type_args = {'num_classes': len(class2ind),
                       'num_superclasses': len(superclass2ind),
                       'features1_len': x_sample[0].shape[-1],
                       'features2_len': x_sample[6].shape[-1],
                       'image_dim': x_sample[3].shape[1:],
                       'matrix_dim': x_sample[4].shape[1:],
                       'full_matrix_dim': x_sample[5].shape[1:]}

    model = build_and_save_model(model_type, model_type_args, 'mean_squared_error', ['accuracy'])

    checkpoint = ModelCheckpoint(
        MODEL_NAME,
        save_best_only=True,
    )

    # lr_schedule = CyclicLRScheduler(
    #     cycles=5,
    #     total_steps=epochs * len(train),
    # )

    model.fit_generator(
        train,
        epochs=epochs,
        verbose=verbose,
        validation_data=val,
        callbacks=[checkpoint]
    )

    # # # temp for debugging
    # save_model(model, 'test.h5')
    # model = load_model('test.h5')

    evaluate_model(model, save_failures, test, verbose)


def get_class_superclass_ind(class_file='data/rawdata/tracker_labels.txt'):
    with open(class_file, 'r') as f:
        classes = [x.strip() for x in f]
    superclasses = set([get_superclass(cls) for cls in classes])
    class2ind = {c: i for c, i in zip(classes, range(len(classes)))}
    superclass2ind = {c: i for c, i in zip(superclasses, range(len(superclasses)))}

    return class2ind, superclass2ind


def load_data(csv_path, validation_proportion=0.1, testing_proportion=0.1):  # test proportion = 0.25

    # load the image data and labels from the csv files
    df = pd.read_csv(csv_path)

    # print information about the dataset
    prediction_pairs = []
    for row in df.iterrows():
        data = row[1]
        prediction_pairs.append(data)

    # split the data into training, validation, and testing
    validation_start = int(len(prediction_pairs) * (1-(validation_proportion + testing_proportion)))
    testing_start = int(len(prediction_pairs) * (1-testing_proportion))
    training_data = prediction_pairs[:validation_start]
    validation_data = prediction_pairs[validation_start:testing_start]
    testing_data = prediction_pairs[testing_start:]

    # training_data = prediction_pairs[:128]
    # validation_data = prediction_pairs[128:256]
    # testing_data = prediction_pairs[256:512]

    return training_data, validation_data, testing_data


def split_files(data_folder, val_frac=0.1, reverse_order=False):
    files_2012 = sorted((Path(data_folder) / '12').glob('*_all.csv'), reverse=reverse_order)
    files_2013 = sorted((Path(data_folder) / '13').glob('*_all.csv'), reverse=reverse_order)
    files_2014 = sorted((Path(data_folder) / '14').glob('*_all.csv'), reverse=reverse_order)

    val_count_12 = int(len(files_2012) * val_frac)
    val_count_13 = int(len(files_2013) * val_frac)

    train_files = files_2012[:-val_count_12] + files_2013[:-val_count_13]
    val_files = files_2012[-val_count_12:] + files_2013[-val_count_13:]
    test_files = files_2014

    return train_files, val_files, test_files


def get_prediction_pairs_from_files(files, class2ind):
    """Returns a tuple of (predictions_1_list, predictions_2_list)."""

    predictions_1 = []
    predictions_2 = []

    for file in files:
        predictions = get_prediction_pairs(file, class2ind)
        predictions_1 += predictions[0]
        predictions_2 += predictions[1]

    return predictions_1, predictions_2


def get_prediction_pairs(file, class2ind):
    predictions_1, predictions_2 = [], []
    df = pd.read_csv(str(file))
    frames = sorted(df['image'].unique())
    for prev, curr in zip(frames, frames[1:]):

        prev_preds = df[(df.image == prev) & (df['class'].isin(class2ind))]
        prev_cam_lat, prev_cam_lon, _ = cam_gps(prev)
        prev_cam_dom = cam_dom(prev)

        curr_preds = df[(df.image == curr) & (df['class'].isin(class2ind))]
        curr_cam_lat, curr_cam_lon, _ = cam_gps(curr)
        curr_cam_dom = cam_dom(curr)

        for pred_1 in prev_preds.itertuples(index=False):
            for pred_2 in curr_preds.itertuples(index=False):
                if (pred_1[2] not in class2ind) or (pred_2[2] not in class2ind):
                    continue
                predictions_1.append((*pred_1, prev_cam_lat, prev_cam_lon, prev_cam_dom))
                predictions_2.append((*pred_2, curr_cam_lat, curr_cam_lon, curr_cam_dom))

    return [predictions_1, predictions_2]


def save_full_matrix_image(matrix, save_path):
    """Saves the matrix being input to the distance network as an image."""
    #
    # [float(detection['lon']),
    #  float(detection['lat']),
    #  float(detection['xmin']),
    #  float(detection['xmax']),
    #  float(detection['ymin']),
    #  float(detection['ymax']),
    #  1,
    #  cam_longitude,
    #  cam_latitude,
    #  cam_dir_movement])

    # delete_indices = [6] + [i for i in range(10, MATRIX_THIRD_DIM)]
    # make a matrix of positional information
    pos_matrix = np.delete(matrix, [2, 3, 4, 5, 6, 7, 8, 9] + [i for i in range(10, MATRIX_THIRD_DIM)], 2)
    pos_matrix = np.mean(pos_matrix, axis=2)
    pos_matrix = np.pad(pos_matrix, 1, constant_values=1)
    pos_matrix = np.expand_dims(pos_matrix, axis=2)
    # print(pos_matrix)
    # print(pos_matrix.shape)
    # input("pos matrix")

    # make a matrix of bounding box information
    bbox_matrix = np.delete(matrix, [0, 1, 6, 7, 8] + [i for i in range(10, MATRIX_THIRD_DIM)], 2)
    bbox_matrix = np.mean(bbox_matrix, axis=2)
    bbox_matrix = np.pad(bbox_matrix, 1, constant_values=1)
    bbox_matrix = np.expand_dims(bbox_matrix, axis=2)
    # print(bbox_matrix)
    # print(bbox_matrix.shape)
    # input("bbox matrix")

    # make a matrix of the class information
    num_classes = MATRIX_THIRD_DIM - 9
    cls_indices = np.delete(matrix, [i for i in range(9)], 2)
    cls_indices = np.argwhere(cls_indices == 1)
    cls_indices = list(cls_indices)  # the indices of the class labels
    cls_matrix = np.zeros(MATRIX_DIM)
    cls_matrix = np.pad(cls_matrix, 1, constant_values=1)
    for indices in cls_indices:
        cls_matrix[indices[0], indices[1]] = indices[2] / num_classes
    cls_matrix = np.expand_dims(cls_matrix, axis=2)
    # print(cls_matrix)
    # print(cls_matrix.shape)
    # input("Class matrix")

    # concatenate to form the full matrix image
    full_matrix_image = np.concatenate((pos_matrix, bbox_matrix, cls_matrix), axis=2)
    # print(full_matrix_image)
    # input("full matrix image")

    # save the full matrix image to a file
    # image_data = np.kron(full_matrix_image, np.ones((5, 5)))
    # print(full_matrix_image.shape)
    # print(full_matrix_image.dtype)
    image_data = full_matrix_image.repeat(50, axis=0).repeat(50, axis=1)
    # print(image_data.shape)
    # print(image_data.dtype)
    # save_image('/home/vision/Documents/debug/test1.png', full_matrix_image)
    save_image(save_path, image_data)
    # input("saved")


def save_bad_predictions(model, data_generator, output_directory):

    # find any predictions the model performed particularly poorly at and save them to an output directory
    output_directory = Path(output_directory)
    if output_directory.exists():
        for file_path in output_directory.glob("*"):
            file_path.unlink()
        output_directory.rmdir()
    output_directory.mkdir()

    for b in range(len(data_generator)):
        test_x, test_y = data_generator.__getitem__(b)
        y_preds = model.predict(test_x)
        y_true = test_y
        error_indices = (abs(y_preds - y_true) > .5)
        error_indices.fill(True)

        for i in range(error_indices.shape[0]):
            if error_indices[i]:
                image_num = b*error_indices.shape[0] + i
                save_directory = output_directory / f"image_number_{image_num}_prediction={y_preds[i][0]:.2f}_correct_prediction={y_true[i][0]}"
                image_data_1 = test_x[3][i]
                image_data_2 = test_x[9][i]
                save_directory.mkdir()
                save_image(save_directory / 'image1.png', image_data_1)
                save_image(save_directory / 'image2.png', image_data_2)
                save_full_matrix_image(test_x[5][i], save_directory / 'matrix1.png')
                save_full_matrix_image(test_x[11][i], save_directory / 'matrix2.png')


def evaluate_model(model, save_failures, data_generator, verbose=False):
    K.clear_session()
    model = load_and_build_model()

    if save_failures:
        save_bad_predictions(model, data_generator, r'/home/vision/Documents/bad_prediction_examples')

    # evaluate the model's performance
    test_results = model.evaluate_generator(data_generator, verbose=verbose)
    print(f'Test loss: {test_results[0]: .4f}')
    print(f'Test accuracy: {test_results[1]: .4f}')

    # get an array of corresponding predictions and labels
    test_X, labels = data_generator.get_all_items()
    preds = model.predict(test_X)
    labels = labels.flatten()
    preds = preds.flatten()

    # print out performance metrics
    print(pd.Series(preds, name='Test Predictions').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]))
    print(pd.Series(labels, name='Test Labels').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]))
    print(pd.Series((preds - labels).round(4), name='Test Errors').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]))
    print(pd.Series(np.abs((preds - labels).round(4)), name='Absolute Test Errors').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99]))

    # make a performance graph
    error_percentiles = pd.Series(np.abs((preds - labels).round(4)), name='Absolute Test Errors').describe(
        percentiles=[.25, .50, .75, .90, .95, .97, .98, .99])
    error_percentiles = error_percentiles.to_dict()

    plot_colors = ['tab:orange', 'tab:green', 'tab:red', 'tab:purple']
    plot_percentiles = ['50%', '90%', '95%', '99%']
    print(error_percentiles['99%'])
    plt.hist(np.abs(preds - labels), bins=20)
    for percentile, color in zip(plot_percentiles, plot_colors):
        plt.axvline(x=error_percentiles[percentile], label=f'{percentile[:-1]}th Percentile', color=color)
    plt.xlabel("Absolute Error")
    plt.ylabel("Count")
    plt.legend()
    plt.show()


def get_sample_generator(data_folder='../../../ARTS-Dataset/rawdata'):
    csv_file_path = Path(data_folder) / 'labels.csv'
    train_predictions, val_predictions, test_predictions = load_data(csv_file_path)

    class2ind, superclass2ind = get_class_superclass_ind()

    sample_gen = MergedDataGenerator(train_predictions,
                                     class2ind,
                                     superclass2ind,
                                     batch_size=16,
                                     visual_noise=True,
                                     other_noise=True,
                                     mod_features=True)

    return sample_gen


class MergedDataGenerator(Sequence):
    def __init__(self,
                 prediction_pairs,
                 class_2_ind,
                 superclass_2_ind,
                 batch_size,
                 visual_noise=False,
                 other_noise=False,
                 mod_features=False,
                 gps_scale=0.00006,
                 bb_scale=10,
                 class_scale=0.001,
                 superclass_scale=0.001):

        self.prediction_pairs = prediction_pairs
        self.class_2_ind = class_2_ind
        self.superclass_2_ind = superclass_2_ind
        self.batch_size = batch_size
        self.num_classes = len(class_2_ind)
        self.num_super_classes = len(superclass_2_ind)
        self.visual_noise = visual_noise
        self.other_noise = other_noise
        self.mod_features = mod_features
        self.gps_scale = gps_scale
        self.bb_scale = bb_scale
        self.class_scale = class_scale
        self.superclass_scale = superclass_scale

    def get_all_items(self):
        features = self.get_x(self.prediction_pairs)
        features = self.apply_noise(features)
        if self.mod_features:
            features = self.get_modded_batch(features)
        labels = self.get_labels(self.prediction_pairs)
        return features, labels

    def __len__(self):
        return int(len(self.prediction_pairs) / float(self.batch_size))

    def __getitem__(self, idx):
        prediction_pairs = self.prediction_pairs[idx * self.batch_size: (idx + 1) * self.batch_size]
        features = self.get_x(prediction_pairs)
        # print("\n\n\nnew")
        # print(features[0])
        features = self.apply_noise(features)
        if self.mod_features:
            features = self.get_modded_batch(features)
        # print(features[0])
        labels = self.get_labels(prediction_pairs)
        return features, labels

    def get_labels(self, prediction_pairs):
        return np.array([[pair[0]] for pair in prediction_pairs])

    def get_x(self, prediction_pairs):

        features_1, classes_1, superclasses_1, image_datas_1, matrices_1, full_matrices_1, \
        features_2, classes_2, superclasses_2, image_datas_2, matrices_2, full_matrices_2 = \
            [], [], [], [], [], [], [], [], [], [], [], []

        for pair in prediction_pairs:

            # get the detection features
            detection_1_features = np.array(pair[5:15])
            detection_2_features = np.array(pair[19:29])
            # detection_1_features = np.zeros((10,))
            # detection_2_features = np.zeros((10,))

            # get the class from the detections
            detection_1_class = np.array(self.class_2_ind[pair[4]])
            detection_2_class = np.array(self.class_2_ind[pair[18]])
            # detection_1_class = np.array(0)
            # detection_2_class = np.array(0)

            # get the superclass
            detection_1_superclass = np.array(self.superclass_2_ind[get_superclass(pair[4])])
            detection_2_superclass = np.array(self.superclass_2_ind[get_superclass(pair[18])])
            # detection_1_superclass = np.array(0)
            # detection_2_superclass = np.array(0)

            # extract the features from the image data
            image_data_1 = load_image(pair[1])
            image_data_2 = load_image(pair[15])
            # image_data_1 = np.zeros((224, 224, 3))
            # image_data_2 = np.zeros((224, 224, 3))

            # add all the features for the detection to the list
            features_1.append(detection_1_features)
            classes_1.append(detection_1_class)
            superclasses_1.append(detection_1_superclass)
            image_datas_1.append(image_data_1)
            features_2.append(detection_2_features)
            classes_2.append(detection_2_class)
            superclasses_2.append(detection_2_superclass)
            image_datas_2.append(image_data_2)

            # get the detection matrix
            image_1_path = pair['image'][3:]
            image_2_path = pair['image.1'][3:]
            matrices_1.append(self.get_detection_matrix(image_1_path, pair[7:11]))
            matrices_2.append(self.get_detection_matrix(image_2_path, pair[21:25]))
            # matrices_1.append(np.zeros(MATRIX_DIM))
            # matrices_2.append(np.zeros(MATRIX_DIM))

            # get the full detection matrix
            full_matrices_1.append(self.get_full_detection_matrix(image_1_path))
            full_matrices_2.append(self.get_full_detection_matrix(image_2_path))
            # full_matrices_1.append(np.zeros((*MATRIX_DIM, len(detection_1_features)-1)))
            # full_matrices_2.append(np.zeros((*MATRIX_DIM, len(detection_2_features)-1)))

        return [np.array(features_1),
                np.array(classes_1),
                np.array(superclasses_1),
                np.array(image_datas_1),
                np.array(matrices_1),
                np.array(full_matrices_1),
                np.array(features_2),
                np.array(classes_2),
                np.array(superclasses_2),
                np.array(image_datas_2),
                np.array(matrices_2),
                np.array(full_matrices_2)]

    def get_detection_matrix(self, image_path, detection_bbox, csv_path=None):

        # return np.zeros(MATRIX_DIM)

        if not csv_path:
            csv_path = Path(image_path).parts
            csv_path = Path(*csv_path[:-3]) / (csv_path[-3] + '_all.csv')

        all_detections = load_detections(csv_path)
        detections = all_detections.get(image_path)
        detection_matrix = np.zeros(MATRIX_DIM)

        for detection in detections:
            x_pos = int(((int(detection['xmin']) + int(detection['xmin'])) / 2) / 1920 * MATRIX_DIM[0])
            y_pos = int(((int(detection['ymin']) + int(detection['ymin'])) / 2) / 1080 * MATRIX_DIM[0])
            detection_matrix[x_pos, y_pos] = -1

        x_pos = int(((int(detection_bbox[0]) + int(detection_bbox[1])) / 2) / 1920 * MATRIX_DIM[0])
        y_pos = int(((int(detection_bbox[2]) + int(detection_bbox[3])) / 2) / 1080 * MATRIX_DIM[0])
        detection_matrix[x_pos, y_pos] = 1
        detection_matrix = np.expand_dims(detection_matrix, axis=-1)

        return detection_matrix

    def get_full_detection_matrix(self, image_path, csv_path=None, pos_shift=2, remove_prob=.1, add_prob=.01):
        """
        Returns a matrix where detections are located in their approximate position and features are across the third
        axis.
        """

        # return np.zeros((*MATRIX_DIM, MATRIX_THIRD_DIM))

        if not csv_path:
            csv_path = Path(image_path).parts
            csv_path = Path(*csv_path[:-3]) / (csv_path[-3] + '_all.csv')
        all_detections = load_detections(csv_path)
        detections = all_detections.get(image_path)
        detection_matrix = np.zeros((*MATRIX_DIM, MATRIX_THIRD_DIM))
        cam_latitude, cam_longitude, _ = cam_gps(image_path)
        cam_dir_movement = cam_dom(image_path)

        # make a list of all miscellaneous features (for normalization)
        all_features = []
        classes = []
        for detection in detections:
            # print(detection)
            # input("detection")
            all_features.append(np.array([float(detection['lon']),
                                 float(detection['lat']),
                                 float(detection['xmin']),
                                 float(detection['xmax']),
                                 float(detection['ymin']),
                                 float(detection['ymax']),
                                 1,
                                 cam_longitude,
                                 cam_latitude,
                                 cam_dir_movement]))
            # all_features.append(np.array([0,
            #                               0,
            #                               float(detection['xmin']),
            #                               float(detection['xmax']),
            #                               float(detection['ymin']),
            #                               float(detection['ymax']),
            #                               0,
            #                               0,
            #                               0,
            #                               0]))
            classes.append(detection['class'])

        # add all features
        for detection in detections:

            # add detection except with certain probability to simulate noise
            if random.random() > remove_prob:
                # compute the position in the matrix
                x_pos = int(((int(detection['xmin']) + int(detection['xmin'])) / 2) / 1920 * MATRIX_DIM[0])
                y_pos = int(((int(detection['ymin']) + int(detection['ymin'])) / 2) / 1080 * MATRIX_DIM[0])
                x_pos += random.randint(-1 * pos_shift, pos_shift)
                y_pos += random.randint(-1 * pos_shift, pos_shift)
                def clamp(n, minn, maxn):
                    return max(min(maxn, n), minn)
                x_pos = clamp(x_pos, 0, 9)
                y_pos = clamp(y_pos, 0, 9)

                # get the image data
                # image_data = get_sign_image_data(image_path,
                #                                  int(detection['xmin']),
                #                                  int(detection['xmax']),
                #                                  int(detection['ymin']),
                #                                  int(detection['ymax']))
                # image_data = image_data.flatten()

                # get the superclass one hot vector
                superclass = get_superclass(detection['class'])
                index = self.superclass_2_ind[superclass]
                superclass_one_hot = np.zeros((len(self.superclass_2_ind),))
                superclass_one_hot[index] = 1

                # get the miscellaneous features
                features = np.array([float(detection['lon']),
                                     float(detection['lat']),
                                     float(detection['xmin']),
                                     float(detection['xmax']),
                                     float(detection['ymin']),
                                     float(detection['ymax']),
                                     1,
                                     cam_longitude,
                                     cam_latitude,
                                     cam_dir_movement])

                # normalize the miscellaneous features and then concatenate all the features
                features = self.get_modded_features(features, *all_features)[0, :]
                # features = np.concatenate((features, image_data))
                features = np.concatenate((features, superclass_one_hot))
                # print("modded features")
                # print(features)
                # print(type(features))
                # # print(type(image_data))
                # print(features.shape)
                # # print(image_data.shape)
                # input("Enter")
                detection_matrix[x_pos, y_pos, :] = features[:]

        # add noise features to each tile with certain probability
        for x_pos in range(0, MATRIX_DIM[0]):
            for y_pos in range(0, MATRIX_DIM[1]):
                if random.random() < add_prob:
                    detection = random.choice(all_detections[random.choice(list(all_detections.keys()))])

                    x_pos = int(((int(detection['xmin']) + int(detection['xmin'])) / 2) / 1920 * MATRIX_DIM[0])
                    y_pos = int(((int(detection['ymin']) + int(detection['ymin'])) / 2) / 1080 * MATRIX_DIM[0])

                    # get the superclass one hot vector
                    superclass = get_superclass(detection['class'])
                    index = self.superclass_2_ind[superclass]
                    superclass_one_hot = np.zeros((len(self.superclass_2_ind),))
                    superclass_one_hot[index] = 1

                    # get the miscellaneous features
                    features = np.array([float(detection['lon']),
                                         float(detection['lat']),
                                         float(detection['xmin']),
                                         float(detection['xmax']),
                                         float(detection['ymin']),
                                         float(detection['ymax']),
                                         1,
                                         cam_longitude,
                                         cam_latitude,
                                         cam_dir_movement])

                    # normalize the miscellaneous features and then concatenate all the features
                    features = self.get_modded_features(features, *all_features)[0, :]
                    features = np.concatenate((features, superclass_one_hot))
                    detection_matrix[x_pos, y_pos, :] = features[:]

        # detection_matrix = np.expand_dims(detection_matrix, axis=-1)

        # save_full_matrix_image(detection_matrix, '/home/vision/Documents/debug/test.png')

        return detection_matrix

    def get_sample_data(self, sample_batch_size):
        """
        Returns a small data sample with the specified batch size.
        Useful for preprocessing.
        """

        current_batch_size = self.batch_size
        self.batch_size = sample_batch_size
        sample_data = self.__getitem__(0)
        self.batch_size = current_batch_size

        return sample_data

    def get_modded_batch(self, all_features):
        features_1, classes_1, superclasses_1, image_datas_1, matrices_1, full_matrices_1, \
        features_2, classes_2, superclasses_2, image_datas_2, matrices_2, full_matrices_2 \
            = all_features

        mod_features_1 = []
        mod_features_2 = []

        for i in range(features_1.shape[0]):
            modded_features = self.get_modded_features(features_1[i], features_2[i])
            mod_features_1.append(modded_features[0])
            mod_features_2.append(modded_features[1])

        mod_features_1 = np.array(mod_features_1)
        mod_features_2 = np.array(mod_features_2)

        return [mod_features_1, classes_1, superclasses_1, image_datas_1, matrices_1, full_matrices_1,
                mod_features_2, classes_2, superclasses_2, image_datas_2, matrices_2, full_matrices_2]

    def get_modded_features(self, *all_features):
        """Performs the normalizations on the passed features."""

        # find the min and max longitude and latitude for normalization
        longitudes = []
        latitudes = []
        for features in all_features:
            sign_longitude, sign_latitude = features[:2]
            cam_longitude, cam_latitude = features[7:9]
            longitudes += [sign_longitude, cam_longitude]
            latitudes += [sign_latitude, cam_latitude]
        min_longitude = min(longitudes)
        max_longitude = max(longitudes)
        min_latitude = min(latitudes)
        max_latitude = max(latitudes)

        # normalize the features
        modded_features = []
        for features in all_features:
            sign_longitude, sign_latitude = features[:2]
            left, right = features[2:4] / 1920
            bottom, top = features[4:6] / 1080
            cam_longitude, cam_latitude, cam_dom = features[7:]
            cam_dom /= 360
            sign_longitude = 0 if max_longitude == min_longitude else (max_longitude-sign_longitude)/(max_longitude-min_longitude)
            sign_latitude = 0 if max_latitude == min_latitude else (max_latitude-sign_latitude)/(max_latitude - min_latitude)
            cam_longitude = 0 if max_longitude == min_longitude else (max_longitude-cam_longitude)/(max_longitude-min_longitude)
            cam_latitude = 0 if max_latitude == min_latitude else (max_latitude-cam_latitude)/(max_latitude - min_latitude)
            modded_features.append([sign_longitude, cam_longitude, sign_latitude, cam_latitude, cam_dom, left, right, bottom, top])

        return np.array(modded_features)

    def apply_noise(self, features):
        features_1, classes_1, superclasses_1, image_features_1, matrices_1, full_matrices_1, \
        features_2, classes_2, superclasses_2, image_features_2, matrices_2, full_matrices_2 \
            = features

        if self.visual_noise:
            # apply noise to image_features_1
            image_features_1 = self.apply_visual_noise(image_features_1)

            # apply noise to image_features_2
            image_features_2 = self.apply_visual_noise(image_features_2)

        if self.other_noise:

            noises = pickle.load(open(NOISES_FILE_PATH, "rb"))

            # apply noise to features_1
            sign_gps_noise = np.zeros((len(features_1), 2))
            sign_gps_noise[:, 0:1] = np.random.choice(noises['lon'], (len(features_1), 1))
            sign_gps_noise[:, 1:2] = np.random.choice(noises['lat'], (len(features_1), 1))
            # sign_gps_noise[:, 0:1] = np.zeros((len(features_1), 1))
            # sign_gps_noise[:, 1:2] = np.zeros((len(features_1), 1))
            bb_coord_noise = np.zeros((len(features_1), 4))
            bb_coord_noise[:, 0:1] += np.random.choice(noises['xmin'], (len(features_1), 1))
            bb_coord_noise[:, 1:2] += np.random.choice(noises['xmax'], (len(features_1), 1))
            bb_coord_noise[:, 2:3] += np.random.choice(noises['ymin'], (len(features_1), 1))
            bb_coord_noise[:, 3:4] += np.random.choice(noises['ymax'], (len(features_1), 1))
            unknown_noise = np.zeros((len(features_1), 1))
            cam_gps_noise = np.zeros((len(features_1), 2))
            cam_dom_noise = np.zeros((len(features_1), 1))

            features_1 += np.hstack((sign_gps_noise, bb_coord_noise, unknown_noise, cam_gps_noise, cam_dom_noise))

            # apply noise to classes_1
            classes_1 = np.where(
                np.random.choice(noises['class'], (len(features_1))),
                classes_1,
                np.random.randint(low=self.num_classes, size=classes_1.shape),
            )

            # apply noise to superclasses_1
            superclasses_1 = np.where(
                np.random.choice(noises['superclass'], (len(features_1))),
                superclasses_1,
                np.random.randint(low=self.num_super_classes, size=superclasses_1.shape),
            )

            # Apply noise to features_2
            sign_gps_noise = np.zeros((len(features_1), 2))
            sign_gps_noise[:, 0:1] = np.random.choice(noises['lon'], (len(features_1), 1))
            sign_gps_noise[:, 1:2] = np.random.choice(noises['lat'], (len(features_1), 1))
            # sign_gps_noise[:, 0:1] = np.zeros((len(features_1), 1))
            # sign_gps_noise[:, 1:2] = np.zeros((len(features_1), 1))
            bb_coord_noise = np.zeros((len(features_1), 4))
            bb_coord_noise[:, 0:1] += np.random.choice(noises['xmin'], (len(features_1), 1))
            bb_coord_noise[:, 1:2] += np.random.choice(noises['xmax'], (len(features_1), 1))
            bb_coord_noise[:, 2:3] += np.random.choice(noises['ymin'], (len(features_1), 1))
            bb_coord_noise[:, 3:4] += np.random.choice(noises['ymax'], (len(features_1), 1))
            unknown_noise = np.zeros((len(features_1), 1))
            cam_gps_noise = np.zeros((len(features_1), 2))
            cam_dom_noise = np.zeros((len(features_1), 1))

            features_2 += np.hstack((sign_gps_noise, bb_coord_noise, unknown_noise, cam_gps_noise, cam_dom_noise))

            # apply noise to classes_2
            classes_2 = np.where(
                np.random.choice(noises['class'], (len(features_1))),
                classes_1,
                np.random.randint(low=self.num_classes, size=classes_1.shape),
            )

            # apply noise to superclasses_2
            superclasses_2 = np.where(
                np.random.choice(noises['superclass'], (len(features_1))),
                superclasses_2,
                np.random.randint(low=self.num_super_classes, size=superclasses_1.shape),
            )

        return [features_1, classes_1, superclasses_1, image_features_1, matrices_1, full_matrices_1,
                features_2, classes_2, superclasses_2, image_features_2, matrices_2, full_matrices_2]

    def apply_visual_noise(self, batch_x):

        # apply augmentation to visual information
        # batch_x = self.add_gaussian_noise(batch_x)
        # batch_x = self.add_blockout(batch_x)
        # batch_x = self.add_rotation(batch_x)
        # batch_x = self.add_blur(batch_x)
        batch_x = self.add_cropping(batch_x)

        # save samples of augmented images for visualization
        # augmented_save_path = Path('augmented_samples')
        # if not augmented_save_path.exists():
        #     augmented_save_path.mkdir()
        # for i in range(batch_x.shape[0]):
        #     save_image(f"augmented_samples/{random.randint(0, 1000)}.png", batch_x[i])

        return batch_x

    def add_gaussian_noise(self, data, stdev=3):

        noise = np.random.normal(0, stdev, data.shape) / 255.
        data += noise

        return data

    def add_blockout(self, batch_x, p=.5, num=2):

        for i in range(batch_x.shape[0]):
            for n in range(num):
                p_1 = np.random.rand()
                if p_1 < p:
                    x = np.random.randint(0, batch_x.shape[2])
                    y = np.random.randint(0, batch_x.shape[1])
                    width = random.randint(0, batch_x.shape[2] - x)
                    height = random.randint(0, batch_x.shape[1] - y)
                    c = np.random.uniform(0, 255, (height, width, batch_x.shape[3])) / 255.
                    batch_x[i, y:y + height, x:x + width, :] = c

        return batch_x

    def add_rotation(self, batch_x, p=.5, max_deg=5):

        # for each image in the batch
        for i in range(batch_x.shape[0]):

            # get the image
            image = batch_x[i]

            # determine whether to rotate the image
            p_rotate = np.random.rand()

            # rotate if appropriate
            if p_rotate < p:
                image = scipy.ndimage.rotate(image, np.random.uniform(-max_deg, max_deg), reshape=False)

            batch_x[i] = image

        return batch_x

    def add_blur(self, batch_x, p=.25, amount=0.5):

        # for each image in the batch
        for i in range(batch_x.shape[0]):

            # get the image
            image = batch_x[i]

            # determine whether to rotate each image
            p_rotate = np.random.rand()

            # rotate each half if appropriate
            if p_rotate < p:
                image = scipy.ndimage.gaussian_filter(image, sigma=amount)

            batch_x[i] = image

        return batch_x

    def add_cropping(self, batch_x, p=.5, max_amount=.2): #.4
        # for each image in the batch
        for i in range(batch_x.shape[0]):

            # get the image
            image = batch_x[i]

            # determine whether to rotate the image
            p_crop = np.random.rand()

            # rotate if appropriate
            if p_crop < p:
                original_shape = image.shape
                left_crop = random.randint(0, int(max_amount * image.shape[1]))
                right_crop = random.randint(image.shape[1] - int(max_amount * image.shape[1]), image.shape[1])
                top_crop = random.randint(0, int(max_amount * image.shape[0]))
                bottom_crop = random.randint(image.shape[0] - int(max_amount * image.shape[0]), image.shape[0])
                image = image[top_crop:bottom_crop, left_crop:right_crop]
                image = resize(image, (original_shape[0], original_shape[1]))

            batch_x[i] = image

        return batch_x


def get_superclass(subclass):
    # return subclass.split("-")[0]
    return subclass[0]


"""--------------These functions save a load models.----------------------------------"""


def get_model_funcs_and_losses():
    model_funcs = {'DENSE': build_dense_concatenated_metric_network,
                   'SEP': build_separate_concatenated_metric_network,
                   'STACKED': build_color_concatenated_metric_network,
                   'CONV': build_fully_convolutional_network,
                   'SIAM': build_siamese_network,
                   'FROZEN': build_siamese_frozen_model,
                   'MATRIX': build_matrix_model,
                   'FULLMAT': build_full_matrix_model}
    losses = {'binary_crossentropy': binary_crossentropy,
              'mean_squared_error': mean_squared_error,
              'binary_focal_loss': binary_focal_loss(alpha=.25, gamma=2)}

    return model_funcs, losses


def build_and_save_model(model_type, model_func_args, loss, metrics, starting_weights=None):
    model_funcs, losses = get_model_funcs_and_losses()

    # build the model
    model_func = model_funcs[model_type]
    model = model_func(**model_func_args)
    # model.compile(optimizer=Adam(lr=0.00005), loss=losses[loss], metrics=metrics)
    model.compile(optimizer=Adam(lr=0.00001), loss=losses[loss], metrics=metrics)
    if starting_weights:
        model.load_weights(starting_weights)

    # save the parameters used to build the model
    build_args = {'model_type': model_type,
                  'model_func_args': model_func_args,
                  'loss': loss,
                  'metrics': metrics,
                  'weights_file': MODEL_NAME}
    pickle.dump(build_args, open(MODEL_PARAMS_PATH, 'wb'))

    return model


def load_and_build_model():
    model_funcs, losses = get_model_funcs_and_losses()

    # load the parameters used to build the model
    build_args = pickle.load(open(MODEL_PARAMS_PATH, 'rb'))

    # build the model
    model_func = model_funcs[build_args['model_type']]
    model = model_func(**build_args['model_func_args'])
    loss = losses[build_args['loss']]
    metrics = build_args['metrics']
    model.compile(optimizer=Adam(), loss=loss, metrics=metrics)
    weights_file = build_args['weights_file']
    model.load_weights(weights_file)

    return model


"""----------------------------------------------------------------------------------"""


def build_color_concatenated_metric_network(num_classes, num_superclasses, features1_len, features2_len, image_dim, embedding_dim=50):
    features_1 = Input(shape=(features1_len,))
    class_1 = Input(shape=(1,))
    superclass_1 = Input(shape=(1,))
    image_data_1 = Input(shape=image_dim)
    matrix_1 = Input(shape=(*MATRIX_DIM, 1))
    features_2 = Input(shape=(features2_len,))
    class_2 = Input(shape=(1,))
    superclass_2 = Input(shape=(1,))
    image_data_2 = Input(shape=image_dim)
    matrix_2 = Input(shape=(*MATRIX_DIM, 1))

    image_data = concatenate([image_data_1, image_data_2])

    conv = Conv2D(32, kernel_size=(3, 3), activation='relu')(image_data)
    conv = Conv2D(64, (3, 3), activation='relu')(conv)
    conv = MaxPooling2D(pool_size=(2, 2))(conv)
    conv = Dropout(0.25)(conv)
    conv = Conv2D(32, (3, 3), activation='relu')(conv)
    conv = MaxPooling2D(pool_size=(2, 2))(conv)
    conv = Dropout(0.25)(conv)
    conv = Flatten()(conv)
    conv = Dense(512, activation='relu')(conv)
    conv = Dropout(0.25)(conv)
    conv = Dense(64, activation='relu')(conv)

    class_embedder = Embedding(num_classes, embedding_dim, input_length=1)

    full_input = concatenate([
        features_1,
        Flatten()(class_embedder(class_1)),
        features_2,
        Flatten()(class_embedder(class_2)),
        conv,
    ])

    pred = Dense(150)(full_input)
    pred = Activation('relu')(pred)
    pred = Dropout(0.25)(pred)

    for i in range(1):
        pred = Dense(100)(pred)
        pred = Activation('relu')(pred)
        pred = Dropout(0.25)(pred)

    pred = Dense(10)(pred)
    pred = Activation('relu')(pred)

    pred = Dense(1)(pred)
    pred = Activation('sigmoid')(pred)

    return Model(
        inputs=[features_1, class_1, superclass_1, image_data_1, matrix_1, features_2, class_2, superclass_2, image_data_2, matrix_2],
        outputs=pred,
    )


def build_separate_concatenated_metric_network(num_classes, num_superclasses, features1_len, features2_len, image_dim, embedding_dim=20):
    features_1 = Input(shape=(features1_len,))
    class_1 = Input(shape=(1,))
    superclass_1 = Input(shape=(1,))
    image_data_1 = Input(shape=image_dim)
    matrix_1 = Input(shape=(*MATRIX_DIM, 1))
    features_2 = Input(shape=(features2_len,))
    class_2 = Input(shape=(1,))
    superclass_2 = Input(shape=(1,))
    image_data_2 = Input(shape=image_dim)
    matrix_2 = Input(shape=(*MATRIX_DIM, 1))

    image_input = Input(shape=(32, 32, 3))
    conv = Conv2D(32, kernel_size=(3, 3), activation='relu')(image_input)
    conv = BatchNormalization()(conv)
    conv = Conv2D(64, (3, 3), activation='relu')(conv)
    conv = BatchNormalization()(conv)
    conv = MaxPooling2D(pool_size=(2, 2))(conv)
    conv = Dropout(0.25)(conv)
    for i in range(1):
        conv = Conv2D(32, (3, 3), activation='relu')(conv)
        conv = BatchNormalization()(conv)
        conv = MaxPooling2D(pool_size=(2, 2))(conv)
        conv = Dropout(0.25)(conv)
    conv = Flatten()(conv)
    conv = Dense(512, activation='relu')(conv)
    conv = BatchNormalization()(conv)
    conv = Dropout(0.25)(conv)
    conv = Dense(32, activation='relu')(conv)
    conv = BatchNormalization()(conv)
    conv = Dense(2, activation='relu')(conv)  # remove
    visual_model = Model(inputs=image_input, outputs=conv)

    class_input = Input(shape=(1,))
    class_embedder = Embedding(num_classes, embedding_dim, input_length=1)(class_input)
    cls = Flatten()(class_embedder)
    cls = Dense(2, activation='relu')(cls)
    class_model = Model(inputs=class_input, outputs=cls)

    superclass_input = Input(shape=(1,))
    superclass_embedder = Embedding(num_superclasses, embedding_dim, input_length=1)(superclass_input)
    superclass = Flatten()(superclass_embedder)
    superclass = Dense(2, activation='relu')(superclass)
    superclass_model = Model(inputs=superclass_input, outputs=superclass)

    full_input = concatenate([
        features_1,
        class_model(class_1),
        superclass_model(superclass_1),
        visual_model(image_data_1),
        features_2,
        class_model(class_2),
        superclass_model(superclass_2),
        visual_model(image_data_2),
    ])

    pred = Dense(20)(full_input)  # 150
    pred = Activation('relu')(pred)
    pred = BatchNormalization()(pred)
    pred = Dropout(0.25)(pred)

    for i in range(1):  # 10
        pred = Dense(10)(pred)
        pred = Activation('relu')(pred)
        # pred = BatchNormalization()(pred)
        # pred = Dropout(0.25)(pred)

    pred = Dense(1)(pred)
    pred = Activation('sigmoid')(pred)

    return Model(
        inputs=[features_1, class_1, superclass_1, image_data_1, matrix_1, features_2, class_2, superclass_2, image_data_2, matrix_2],
        outputs=pred,
    )


def build_dense_concatenated_metric_network(num_classes, num_superclasses, features1_len, features2_len, image_dim, matrix_dim, full_matrix_dim, embedding_dim=50):
    features_1 = Input(shape=(features1_len,))
    class_1 = Input(shape=(1,))
    superclass_1 = Input(shape=(1,))
    image_data_1 = Input(shape=image_dim)
    matrix_1 = Input(shape=matrix_dim)
    full_matrix_1 = Input(shape=full_matrix_dim)
    features_2 = Input(shape=(features2_len,))
    class_2 = Input(shape=(1,))
    superclass_2 = Input(shape=(1,))
    image_data_2 = Input(shape=image_dim)
    matrix_2 = Input(shape=matrix_dim)
    full_matrix_2 = Input(shape=full_matrix_dim)

    image_input = Input(shape=(32, 32, 3))
    conv = Conv2D(32, kernel_size=(3, 3), activation='relu')(image_input)
    # conv = BatchNormalization()(conv)
    conv = Conv2D(64, (3, 3), activation='relu')(conv)
    # conv = BatchNormalization()(conv)
    conv = MaxPooling2D(pool_size=(2, 2))(conv)
    conv = Dropout(0.25)(conv)
    for i in range(1):
        conv = Conv2D(32, (3, 3), activation='relu')(conv)
        # conv = BatchNormalization()(conv)
        conv = MaxPooling2D(pool_size=(2, 2))(conv)
        conv = Dropout(0.25)(conv)
    conv = Flatten()(conv)
    conv = Dense(512, activation='relu')(conv)
    # conv = BatchNormalization()(conv)
    conv = Dropout(0.25)(conv)
    conv = Dense(32, activation='relu')(conv)
    # conv = BatchNormalization()(conv)
    visual_model = Model(inputs=image_input, outputs=conv)

    class_embedder = Embedding(num_classes, embedding_dim, input_length=1)
    superclass_embedder = Embedding(num_superclasses, embedding_dim, input_length=1)

    full_input = concatenate([
        features_1,
        Flatten()(class_embedder(class_1)),
        Flatten()(superclass_embedder(superclass_1)),
        visual_model(image_data_1),
        features_2,
        Flatten()(class_embedder(class_2)),
        Flatten()(superclass_embedder(superclass_2)),
        visual_model(image_data_2),
    ])

    pred = Dense(150)(full_input) # 150
    pred = Activation('relu')(pred)
    # pred = BatchNormalization()(pred)
    pred = Dropout(0.25)(pred)

    for i in range(1): # 10
        pred = Dense(10)(pred)
        pred = Activation('relu')(pred)
        # pred = BatchNormalization()(pred)
        # pred = Dropout(0.25)(pred)

    pred = Dense(1)(pred)
    pred = Activation('sigmoid')(pred)

    return Model(
        inputs=[features_1, class_1, superclass_1, image_data_1, matrix_1, full_matrix_1,
                features_2, class_2, superclass_2, image_data_2, matrix_2, full_matrix_2],
        outputs=pred,
    )


def build_fully_convolutional_network(num_classes, num_superclasses, features1_len, features2_len, image_dim, embedding_dim=50):
    features_1 = Input(shape=(features1_len,))
    class_1 = Input(shape=(1,))
    superclass_1 = Input(shape=(1,))
    image_data_1 = Input(shape=image_dim)
    matrix_1 = Input(shape=(*MATRIX_DIM, 1))
    features_2 = Input(shape=(features2_len,))
    class_2 = Input(shape=(1,))
    superclass_2 = Input(shape=(1,))
    image_data_2 = Input(shape=image_dim)
    matrix_2 = Input(shape=(*MATRIX_DIM, 1))

    image_input = Input(shape=(128, 128, 3))
    conv = Conv2D(32, kernel_size=(3, 3), activation='relu')(image_input)
    conv = BatchNormalization()(conv)
    for i in range(2):
        conv = Conv2D(64, (3, 3), activation='relu')(conv)
        conv = BatchNormalization()(conv)
        conv = MaxPooling2D(pool_size=(2, 2))(conv)
        conv = Dropout(0.25)(conv)
    conv = Conv2D(32, (3, 3), activation='relu')(conv)
    conv = BatchNormalization()(conv)
    conv = MaxPooling2D(pool_size=(2, 2))(conv)
    conv = Dropout(0.25)(conv)
    conv = Flatten()(conv)
    conv = Dense(512, activation='relu')(conv)
    conv = BatchNormalization()(conv)
    conv = Dropout(0.25)(conv)
    conv = Dense(128, activation='relu')(conv)
    conv = Dropout(0.25)(conv)
    conv = Dense(32, activation='relu')(conv)
    visual_model = Model(inputs=image_input, outputs=conv)

    full_input = concatenate([visual_model(image_data_1), visual_model(image_data_2)])

    for i in range(2):
        pred = Dense(50)(full_input)
        pred = Activation('relu')(pred)
        pred = BatchNormalization()(pred)
        pred = Dropout(0.25)(pred)

    pred = Dense(10)(pred)
    pred = Activation('relu')(pred)
    pred = BatchNormalization()(pred)

    pred = Dense(1)(pred)
    pred = Activation('sigmoid')(pred)

    return Model(
        inputs=[features_1, class_1, superclass_1, image_data_1, matrix_1, features_2, class_2, superclass_2, image_data_2, matrix_2],
        outputs=pred,
    )


def build_siamese_network(num_classes, num_superclasses, features1_len, features2_len, image_dim, matrix_dim, full_matrix_dim, embedding_dim=50):
    def cosine_distance(vests):
        x, y = vests
        x = K.l2_normalize(x, axis=-1)
        y = K.l2_normalize(y, axis=-1)
        return -K.mean(x * y, axis=-1, keepdims=True)

    def cos_dist_output_shape(shapes):
        shape1, shape2 = shapes
        return shape1[0], 1

    features_1 = Input(shape=(features1_len,))
    class_1 = Input(shape=(1,))
    superclass_1 = Input(shape=(1,))
    image_data_1 = Input(shape=image_dim)
    matrix_1 = Input(shape=matrix_dim)
    full_matrix_1 = Input(shape=full_matrix_dim)
    features_2 = Input(shape=(features2_len,))
    class_2 = Input(shape=(1,))
    superclass_2 = Input(shape=(1,))
    image_data_2 = Input(shape=image_dim)
    matrix_2 = Input(shape=matrix_dim)
    full_matrix_2 = Input(shape=full_matrix_dim)

    visual_model = ResNet50(input_shape=(224, 244, 3), weights='imagenet', include_top=False)
    intermediate_layer = Model(inputs=visual_model.input, outputs=visual_model.get_layer('activation_48').output)

    x1 = intermediate_layer(image_data_1)
    x2 = intermediate_layer(image_data_2)

    x1 = Concatenate(axis=-1)([GlobalMaxPool2D()(x1), GlobalAvgPool2D()(x1)])
    x2 = Concatenate(axis=-1)([GlobalMaxPool2D()(x2), GlobalAvgPool2D()(x2)])

    x3 = Subtract()([x1, x2])
    x3 = Multiply()([x3, x3])

    x1_ = Multiply()([x1, x1])
    x2_ = Multiply()([x2, x2])
    x4 = Subtract()([x1_, x2_])

    x5 = Lambda(cosine_distance, output_shape=cos_dist_output_shape)([x1, x2])

    x = Concatenate(axis=-1)([x5, x4, x3])

    x = Dense(100, activation="relu")(x)
    x = Dropout(0.01)(x)
    pred = Dense(1, activation="sigmoid")(x)

    return Model(
        inputs=[features_1, class_1, superclass_1, image_data_1, matrix_1, full_matrix_1,
                features_2, class_2, superclass_2, image_data_2, matrix_2, full_matrix_2],
        outputs=pred,
    )


def build_siamese_frozen_model(num_classes, num_superclasses, features1_len, features2_len, image_dim, matrix_dim, full_matrix_dim, embedding_dim=50):
    features_1 = Input(shape=(features1_len,))
    class_1 = Input(shape=(1,))
    superclass_1 = Input(shape=(1,))
    image_data_1 = Input(shape=image_dim)
    matrix_1 = Input(shape=matrix_dim)
    full_matrix_1 = Input(shape=full_matrix_dim)
    features_2 = Input(shape=(features2_len,))
    class_2 = Input(shape=(1,))
    superclass_2 = Input(shape=(1,))
    image_data_2 = Input(shape=image_dim)
    matrix_2 = Input(shape=matrix_dim)
    full_matrix_2 = Input(shape=full_matrix_dim)

    siamese_model = build_siamese_network(num_classes, num_superclasses, features1_len, features2_len, image_dim, matrix_dim, full_matrix_dim, embedding_dim)
    # siamese_model.load_weights(FROZEN_MODEL_NAME)
    # for layer in siamese_model.layers:
    #     layer.trainable = False
    intermediate_layer = Model(inputs=siamese_model.input, outputs=siamese_model.get_layer('concatenate_3').output)

    class_embedder = Embedding(num_classes, embedding_dim, input_length=1)
    superclass_embedder = Embedding(num_superclasses, embedding_dim, input_length=1)

    full_input = concatenate([
        features_1,
        Flatten()(class_embedder(class_1)),
        Flatten()(superclass_embedder(superclass_1)),
        features_2,
        Flatten()(class_embedder(class_2)),
        Flatten()(superclass_embedder(superclass_2)),
        intermediate_layer([features_1, class_1, superclass_1, image_data_1, matrix_1, full_matrix_1,
                           features_2, class_2, superclass_2, image_data_2, matrix_2, full_matrix_2])
    ])

    pred = Dense(150)(full_input)  # 150
    pred = Activation('relu')(pred)
    # pred = BatchNormalization()(pred)
    pred = Dropout(0.25)(pred)

    for i in range(1):  # 10
        pred = Dense(10)(pred)
        pred = Activation('relu')(pred)
        # pred = BatchNormalization()(pred)
        # pred = Dropout(0.25)(pred)

    pred = Dense(1)(pred)
    pred = Activation('sigmoid')(pred)

    return Model(
        inputs=[features_1, class_1, superclass_1, image_data_1, matrix_1, full_matrix_1,
                features_2, class_2, superclass_2, image_data_2, matrix_2, full_matrix_2],
        outputs=pred,
    )


def build_matrix_model(num_classes, num_superclasses, features1_len, features2_len, image_dim, matrix_dim, full_matrix_dim, embedding_dim=50):
    features_1 = Input(shape=(features1_len,))
    class_1 = Input(shape=(1,))
    superclass_1 = Input(shape=(1,))
    image_data_1 = Input(shape=image_dim)
    matrix_1 = Input(shape=matrix_dim)
    full_matrix_1 = Input(shape=full_matrix_dim)
    features_2 = Input(shape=(features2_len,))
    class_2 = Input(shape=(1,))
    superclass_2 = Input(shape=(1,))
    image_data_2 = Input(shape=image_dim)
    matrix_2 = Input(shape=matrix_dim)
    full_matrix_2 = Input(shape=full_matrix_dim)

    siamese_model = build_siamese_network(num_classes, num_superclasses, features1_len, features2_len, image_dim, embedding_dim)
    siamese_model.load_weights(FROZEN_MODEL_NAME)
    for layer in siamese_model.layers:
        layer.trainable = False
    intermediate_layer = Model(inputs=siamese_model.input, outputs=siamese_model.get_layer('concatenate_3').output)

    # build the matrix sub-network
    matrix_input = Input(shape=(*MATRIX_DIM, 1))
    conv = Conv2D(32, kernel_size=(3, 3), input_shape=(*MATRIX_DIM, 1), activation='relu')(matrix_input)
    conv = BatchNormalization()(conv)
    for i in range(2):
        conv = Conv2D(32, (3, 3), activation='relu')(conv)
        conv = BatchNormalization()(conv)
        conv = MaxPooling2D(pool_size=(2, 2))(conv)
        conv = Dropout(0.25)(conv)
    conv = Conv2D(32, (3, 3), activation='relu')(conv)
    conv = BatchNormalization()(conv)
    conv = MaxPooling2D(pool_size=(2, 2))(conv)
    conv = Dropout(0.25)(conv)
    conv = Flatten()(conv)
    conv = Dense(512, activation='relu')(conv)
    conv = BatchNormalization()(conv)
    conv = Dropout(0.25)(conv)
    conv = Dense(128, activation='relu')(conv)
    conv = Dropout(0.25)(conv)
    conv = Dense(32, activation='relu')(conv)
    matrix_model = Model(inputs=matrix_input, outputs=conv)

    class_embedder = Embedding(num_classes, embedding_dim, input_length=1)
    superclass_embedder = Embedding(num_superclasses, embedding_dim, input_length=1)

    full_input = concatenate([
        features_1,
        Flatten()(class_embedder(class_1)),
        Flatten()(superclass_embedder(superclass_1)),
        matrix_model(matrix_1),
        features_2,
        Flatten()(class_embedder(class_2)),
        Flatten()(superclass_embedder(superclass_2)),
        matrix_model(matrix_2),
        intermediate_layer([features_1, class_1, superclass_1, image_data_1,
                           features_2, class_2, superclass_2, image_data_2]),
    ])

    pred = Dense(150)(full_input)  # 150
    pred = Activation('relu')(pred)
    # pred = BatchNormalization()(pred)
    pred = Dropout(0.25)(pred)

    for i in range(1):  # 10
        pred = Dense(10)(pred)
        pred = Activation('relu')(pred)
        # pred = BatchNormalization()(pred)
        # pred = Dropout(0.25)(pred)

    pred = Dense(1)(pred)
    pred = Activation('sigmoid')(pred)

    return Model(
        inputs=[features_1, class_1, superclass_1, image_data_1, matrix_1, full_matrix_1,
                features_2, class_2, superclass_2, image_data_2, matrix_2, full_matrix_2],
        outputs=pred,
    )


def build_full_matrix_model(num_classes, num_superclasses, features1_len, features2_len, image_dim, matrix_dim, full_matrix_dim, embedding_dim=50):
    features_1 = Input(shape=(features1_len,))
    class_1 = Input(shape=(1,))
    superclass_1 = Input(shape=(1,))
    image_data_1 = Input(shape=image_dim)
    matrix_1 = Input(shape=matrix_dim)
    full_matrix_1 = Input(shape=full_matrix_dim)
    features_2 = Input(shape=(features2_len,))
    class_2 = Input(shape=(1,))
    superclass_2 = Input(shape=(1,))
    image_data_2 = Input(shape=image_dim)
    matrix_2 = Input(shape=matrix_dim)
    full_matrix_2 = Input(shape=full_matrix_dim)

    siamese_model = build_siamese_network(num_classes, num_superclasses, features1_len, features2_len, image_dim, matrix_dim, full_matrix_dim, embedding_dim)
    siamese_model.load_weights(FROZEN_MODEL_NAME)
    for layer in siamese_model.layers:
        layer.trainable = False
    intermediate_layer = Model(inputs=siamese_model.input, outputs=siamese_model.get_layer('concatenate_3').output)

    # build the matrix sub-network
    full_matrix_input = Input(shape=full_matrix_dim)
    conv = Conv2D(8, kernel_size=(3, 3), input_shape=full_matrix_dim, activation='relu')(full_matrix_input)
    conv = BatchNormalization()(conv)
    for i in range(1):
        conv = Conv2D(8, (3, 3), activation='relu')(conv)
        conv = BatchNormalization()(conv)
        conv = MaxPooling2D(pool_size=(2, 2))(conv)
        conv = Dropout(0.25)(conv)
    conv = Conv2D(8, (3, 3), activation='relu')(conv)
    conv = BatchNormalization()(conv)
    # conv = MaxPooling2D(pool_size=(2, 2))(conv)
    conv = Dropout(0.25)(conv)
    conv = Flatten()(conv)
    conv = Dense(8, activation='relu')(conv)
    conv = Dropout(0.25)(conv)
    full_matrix_model = Model(inputs=full_matrix_input, outputs=conv)

    class_embedder = Embedding(num_classes, embedding_dim, input_length=1)
    superclass_embedder = Embedding(num_superclasses, embedding_dim, input_length=1)

    full_input = concatenate([
        features_1,
        Flatten()(class_embedder(class_1)),
        Flatten()(superclass_embedder(superclass_1)),
        full_matrix_model(full_matrix_1),
        features_2,
        Flatten()(class_embedder(class_2)),
        Flatten()(superclass_embedder(superclass_2)),
        full_matrix_model(full_matrix_2),
        intermediate_layer([features_1, class_1, superclass_1, image_data_1, matrix_1, full_matrix_1,
                            features_2, class_2, superclass_2, image_data_2, matrix_2, full_matrix_2]),
    ])

    pred = Dense(150)(full_input)  # 150
    pred = Activation('relu')(pred)
    # pred = BatchNormalization()(pred)
    pred = Dropout(0.25)(pred)

    for i in range(1):  # 10
        pred = Dense(10)(pred)
        pred = Activation('relu')(pred)
        # pred = BatchNormalization()(pred)
        # pred = Dropout(0.25)(pred)

    pred = Dense(1)(pred)
    pred = Activation('sigmoid')(pred)

    return Model(
        inputs=[features_1, class_1, superclass_1, image_data_1, matrix_1, full_matrix_1,
                features_2, class_2, superclass_2, image_data_2, matrix_2, full_matrix_2],
        outputs=pred,
    )


if __name__ == '__main__':
    args = build_parser().parse_args()
    train_metric_network(**vars(args))
