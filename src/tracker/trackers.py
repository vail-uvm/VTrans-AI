"""
Copyright 2018-2020 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

This file contains the tracker object, which can be used to perform tracking on either the annotations or detections.
"""

import sys, os
os.environ["CUDA_VISIBLE_DEVICES"]="0"
os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = 'true'

from pathlib import Path

import numpy as np
import pandas as pd
import cv2
import csv
from scipy.optimize import linear_sum_assignment
import tensorflow as tf
import xml.etree.ElementTree as ET
import shutil
from PIL import ImageDraw, Image, ImageFont, ImageFile
from evaluate_tracker import evaluate_road_segment
from evaluate_tracker_2 import evaluate_road_segment_2

from distance_metric_model import MATRIX_DIM, MATRIX_THIRD_DIM, get_class_superclass_ind, get_superclass, load_and_build_model, get_sample_generator
from pure_image_similarity_metric import load_and_build_image_model, get_sample_image_generator
from utils import load_detections, cam_gps, bndbox_iou, convert_bbox, get_bbox, save_image, cam_dom, get_xml_path, get_sign_image_data, load_and_resize_image


class Tracker:

    def __init__(self,
                 detections_csv_path,
                 distance_threshold=0.98,
                 age_cutoff=2,
                 detection_overlap_filter_threshold=0.7,
                 distance_filter_threshold=30,
                 debug_directory='/home/vision/Documents/debug'):

        self.detections_csv_path = detections_csv_path
        self.distance_threshold = distance_threshold
        self.age_cutoff = age_cutoff
        self.detection_overlap_filter_threshold = detection_overlap_filter_threshold
        self.distance_filter_threshold = distance_filter_threshold
        self.debug_directory = debug_directory
        self.distance_matrices = {}
        self.signs = []
        self.classes = []
        self.scores = []
        self.inputs_outputs = []

        self.dead_tracklets, self.live_tracklets = [], []
        self.detections = load_detections(self.detections_csv_path)
        road_segment_path = Path(list(self.detections.keys())[0]).parent.parent
        self.images_to_track = sorted(road_segment_path.glob('*/*.jpg'), reverse=True)

    def track(self, save_tracklets_to_csv=False, debug=False):
        self.generate_tracklets()
        self.generate_signs()
        if save_tracklets_to_csv:
            self.save_tracklets_to_csv()
        if debug:
            self.generate_debug_images()

    def generate_tracklets(self):
        raise NotImplementedError

    def get_tracklets(self):
        return self.dead_tracklets

    def get_distance_matrices(self):
        return self.distance_matrices

    def generate_signs(self):
        for tracklet in self.dead_tracklets:
            sign, cls, score = self.tracklet2sign(tracklet)
            self.signs.append(sign)
            self.classes.append(cls)
            self.scores.append(score)

    def get_signs(self):
        return self.signs

    def get_classes(self):
        return self.classes

    def get_scores(self):
        return self.scores

    def save_tracklets_to_csv(self):
        output_directory = Path(self.debug_directory)
        if not output_directory.exists():
            Path(output_directory).mkdir()

        csv_path = str(output_directory / Path(self.dead_tracklets[0][0]['image']).parents[1].stem) + '.csv'
        print(f"Saving tracklets to: {csv_path}")

        with open(csv_path, 'w') as log:
            writer = csv.writer(log, delimiter=',')
            writer.writerow(["Sign ID"] + list(self.dead_tracklets[0][0].keys()))

            for i, tracklet in enumerate(self.dead_tracklets):
                for detection in tracklet:
                    writer.writerow([i] + list(detection.values()))

    def get_debug_image_path(self, original_image_path):
        save_image_path = Path(self.debug_directory) / f"{Path(original_image_path).stem}.png"
        return save_image_path

    def generate_debug_images(self):
        """Generates images of the tracker along with various other images used for debugging."""

        output_directory = Path(self.debug_directory)
        if not output_directory.exists():
            Path(output_directory).mkdir()
        self.debug_directory = output_directory / Path(self.dead_tracklets[0][0]['image']).parents[1].stem
        if not self.debug_directory.exists():
            self.debug_directory.mkdir()
        self.debug_directory = self.debug_directory / Path(self.dead_tracklets[0][0]['image']).parents[0].stem
        if not self.debug_directory.exists():
            self.debug_directory.mkdir()

        print(f"Saving debug images to: {self.debug_directory}")

        self.save_tracklet_images()
        # self.label_missing_signs()
        self.label_fp_fn()
        # self.superimpose_distance_matrices()
        # self.save_inputs_outputs()

    def save_tracklet_images(self):
        """Saves images with tracklets outlined for the road segment."""

        ImageFile.LOAD_TRUNCATED_IMAGES = True
        tracklet_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255),
                           (255, 255, 255), (255, 153, 51), (203, 102, 0), (192, 194, 42), (30, 161, 115),
                           (12, 93, 128),
                           (132, 105, 167), (61, 23, 111), (102, 11, 91), (118, 15, 31)]
        image_path_to_num_tracklets = {}

        # copy the images from the data directory to the save directory
        color_index = 0
        for i, tracklet in enumerate(self.dead_tracklets):
            color = tracklet_colors[color_index]
            for frame in tracklet:

                # load the image and its signs
                save_image_path = self.get_debug_image_path(frame['image'])

                # if there isn't already a debug image, create one
                if not save_image_path.exists():
                    shutil.copy(frame['image'], save_image_path)

                # color the image with the tracklet
                image = Image.open(save_image_path)
                draw = ImageDraw.Draw(image)

                try:
                    # draw the lines around the sign
                    line_thickness = 5
                    xmin = int(frame['xmin']) - line_thickness / 2
                    xmax = int(frame['xmax']) + line_thickness / 2
                    ymin = int(frame['ymin']) - line_thickness / 2
                    ymax = int(frame['ymax']) + line_thickness / 2
                    draw.line([(xmin, ymin), (xmax, ymin)], fill=color, width=line_thickness)
                    draw.line([(xmax, ymin), (xmax, ymax)], fill=color, width=line_thickness)
                    draw.line([(xmax, ymax), (xmin, ymax)], fill=color, width=line_thickness)
                    draw.line([(xmin, ymax), (xmin, ymin)], fill=color, width=line_thickness)

                    # add text indicating the sign color
                    if frame['image'] not in image_path_to_num_tracklets.keys():
                        image_path_to_num_tracklets[frame['image']] = 1
                    else:
                        image_path_to_num_tracklets[frame['image']] += 1
                    num_image_tracklets = image_path_to_num_tracklets[frame['image']]
                    text = f"Tracklet: {i}, Class: {frame['class']}, Lat: {frame['lat']}, Lon: {frame['lon']}"
                    font_size = 30
                    font = ImageFont.truetype('Pillow/Tests/fonts/FreeMonoBold.ttf', font_size)
                    rect_shape = [(0, font_size * num_image_tracklets), (1400, font_size * (num_image_tracklets + 1))]
                    draw.rectangle(rect_shape, fill="black")
                    draw.text((10, font_size * num_image_tracklets), text, fill=color, font=font, align="left")

                    image.save(save_image_path)

                except Exception as e:
                    # catch the exception where the bounding box is not valid
                    print(f"Failed to draw bounding box for coordinates {frame['xmin']}, {frame['xmax']}, "
                          f"{frame['ymin']}, {frame['ymax']}.")
                    print(e)

            color_index += 1
            if color_index >= len(tracklet_colors):
                color_index = 0

    def label_fp_fn(self):
        """Labels the false negatives and false positives on the debug images."""

        ImageFile.LOAD_TRUNCATED_IMAGES = True
        colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255),
                  (255, 255, 255), (255, 153, 51), (203, 102, 0), (192, 194, 42), (30, 161, 115), (12, 93, 128),
                  (132, 105, 167), (61, 23, 111), (102, 11, 91), (118, 15, 31)]
        colors = [(255, 255, 255), (255, 255, 255), (255, 255, 255), (255, 255, 255)]
        color_index = 0

        # find the file path to the annotations
        ground_truth_file_path = Path(
            self.detections_csv_path).parent / f'{Path(self.detections_csv_path.replace("detections", "all")).stem[:-3]}unique.csv'

        # save the detected signs for evaluation
        detected_signs = pd.DataFrame(self.signs).sort_values(['lat', 'lon'])
        detected_signs = detected_signs.sort_values('class')
        temp_detected_signs_path = Path(
            self.detections_csv_path).parent / f'{Path(self.detections_csv_path).stem[:-3]}temp.csv'
        detected_signs.to_csv(temp_detected_signs_path, index=False)

        # create a list of real images
        real_images = set()
        for tracklet in self.dead_tracklets:
            for frame in tracklet:
                if frame['image'] not in real_images:
                    real_images.add(frame['image'])

        # find the false positives and true negatives
        tp, fp, fn = evaluate_road_segment_2(ground_truth_file_path, temp_detected_signs_path, 70)

        print("True Positives")
        print(tp.shape)
        print(tp)
        print("False Positives")
        print(fp.shape)
        print(fp)
        print("False Negatives")
        print(fn.shape)
        print(fn)

        for _, sign in fp.iterrows():
            # print(sign)
            self.mark_sign(sign, index_preface="pred_", label="False Positive")

        for _, sign in fn.iterrows():
            # print(sign)
            self.mark_sign(sign, index_preface="true_", label="False Negative")

    def mark_sign(self, sign, index_preface="", color=(255, 255, 255), label="Sign", sign_num=0):

        # load the image and its signs
        save_image_path = self.get_debug_image_path(sign[index_preface + 'image'])

        # if there isn't already a debug image, create one
        if not save_image_path.exists():
            shutil.copy(sign[index_preface + 'image'], save_image_path)

        # color the image with the tracklet
        image = Image.open(save_image_path)
        draw = ImageDraw.Draw(image)

        # draw the lines around the sign
        # if index_preface + 'xmin' in sign.keys():
        #     line_thickness = 5
        #     xmin = int(sign[index_preface + 'xmin']) - line_thickness / 2
        #     xmax = int(sign[index_preface + 'xmax']) + line_thickness / 2
        #     ymin = int(sign[index_preface + 'ymin']) - line_thickness / 2
        #     ymax = int(sign[index_preface + 'ymax']) + line_thickness / 2
        #     draw.line([(xmin, ymin), (xmax, ymin)], fill=color, width=line_thickness)
        #     draw.line([(xmax, ymin), (xmax, ymax)], fill=color, width=line_thickness)
        #     draw.line([(xmax, ymax), (xmin, ymax)], fill=color, width=line_thickness)
        #     draw.line([(xmin, ymax), (xmin, ymin)], fill=color, width=line_thickness)

        # add text indicating the sign color
        text = f"{label}, Class: {sign[index_preface + 'class']}, Lat: {sign[index_preface + 'lat']}, Lon: {sign[index_preface + 'lon']}"
        font_size = 30
        font = ImageFont.truetype('Pillow/Tests/fonts/FreeMonoBold.ttf', font_size)
        rect_shape = [(0, 1080 - (sign_num + 1) * font_size),
                      (1400, 1080 - sign_num * font_size)]
        draw.rectangle(rect_shape, fill="black")
        draw.text((10, 1080 - (sign_num + 1) * font_size), text, fill=color,
                  font=font, align="left")

        image.save(save_image_path)

    def label_missing_signs(self):
        """Labels the tracklet images with any signs in the dataset that are missing from the results of the tracker."""

        ImageFile.LOAD_TRUNCATED_IMAGES = True
        colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255),
                  (255, 255, 255), (255, 153, 51), (203, 102, 0), (192, 194, 42), (30, 161, 115), (12, 93, 128),
                  (132, 105, 167), (61, 23, 111), (102, 11, 91), (118, 15, 31)]
        colors = [(255, 255, 255), (255, 255, 255), (255, 255, 255), (255, 255, 255)]
        color_index = 0

        # create a dataframe of ground truth signs
        ground_truth_file_path = Path(self.detections_csv_path).parent / f'{Path(self.detections_csv_path.replace("detections", "all")).stem[:-3]}unique.csv'
        ground_truth_signs = pd.read_csv(ground_truth_file_path)

        # save the detected signs for evaluation
        detected_signs = pd.DataFrame(self.signs).sort_values(['lat', 'lon'])
        detected_signs = detected_signs.sort_values('class')
        temp_detected_signs_path = Path(self.detections_csv_path).parent / f'{Path(self.detections_csv_path).stem[:-3]}temp.csv'
        detected_signs.to_csv(temp_detected_signs_path, index=False)

        # create a list of real images
        real_images = set()
        for tracklet in self.dead_tracklets:
            for frame in tracklet:
                if frame['image'] not in real_images:
                    real_images.add(frame['image'])

        # find the unmatched signs
        matches = evaluate_road_segment(ground_truth_file_path, temp_detected_signs_path, 70)[2]
        matches = matches.sort_values('true_class')
        ground_truth_signs['matched'] = np.zeros(ground_truth_signs.shape[0])
        for index, sign in ground_truth_signs.iterrows():
            if matches.loc[(matches['true_class'] == sign['class']) &
                           (round(matches['true_lat'], 3) == round(sign['lat'], 3)) &
                           (round(matches['true_lon'], 3) == round(sign['lon'], 3))].any().any():
                ground_truth_signs.loc[index, "matched"] += 1

        # label signs missing in each image
        for image_file_path in real_images:
            num_signs_written = 0
            signs_in_image = self.get_annotated_signs(get_xml_path(image_file_path))
            save_image_path = self.get_debug_image_path(image_file_path)
            for sign in signs_in_image:
                for _, gt_sign in ground_truth_signs.iterrows():
                    if gt_sign['matched'] == 0:
                        if sign['class'] == gt_sign['class'] and round(sign['lat'], 10) == round(gt_sign['lat'],
                                                                                                 10) and round(
                                sign['lon'], 10) == round(gt_sign['lon'], 10):
                            color_index += 1
                            image = Image.open(save_image_path)
                            draw = ImageDraw.Draw(image)
                            text = f"Missing Sign: {sign['class']}, Lat: {sign['lat']}, Lon: {sign['lon']}"
                            font_size = 30
                            font = ImageFont.truetype('Pillow/Tests/fonts/FreeMonoBold.ttf', font_size)
                            rect_shape = [(0, 1080 - (num_signs_written + 1) * font_size),
                                          (1400, 1080 - num_signs_written * font_size)]
                            draw.rectangle(rect_shape, fill="black")
                            draw.text((10, 1080 - (num_signs_written + 1) * font_size), text, fill=colors[color_index],
                                      font=font, align="left")
                            num_signs_written += 1
                            image.save(save_image_path)

                            color_index += 1
                            if color_index >= len(colors):
                                color_index = 0

    def get_annotated_signs(self, file):
        signs = []
        file_path = Path(str(file))

        if file_path.exists():
            tree = ET.parse(str(file))
            root = tree.getroot()
            sign_elements = root.findall('object')
            for sign in sign_elements:
                sign_class = sign.find('subclass').text
                bounding_box = sign.find('bndbox')
                xmin = int(bounding_box.find('xmin').text)
                xmax = int(bounding_box.find('xmax').text)
                ymin = int(bounding_box.find('ymin').text)
                ymax = int(bounding_box.find('ymax').text)
                location = sign.find('location')
                latitude = float(location.find('latitude').text)
                longitude = float(location.find('longitude').text)
                signs.append({'class': sign_class,
                              'xmin': xmin,
                              'xmax': xmax,
                              'ymin': ymin,
                              'ymax': ymax,
                              'lat': latitude,
                              'lon': longitude})

        return signs

    def superimpose_distance_matrices(self):
        """
        Writes a matrix showing the distance metric values in a matrix on the debug images.
        Args:
            detection_directory: the directory of the detections the tracker is run on
            save_directory: the directory the debug images are saved to

        Returns:

        """
        background_color = [(0, 0, 0)]
        text_color = (255, 255, 255)

        for original_image_path in self.distance_matrices.keys():
            save_image_path = self.get_debug_image_path(original_image_path)
            matrix = self.distance_matrices[original_image_path]

            image = Image.open(save_image_path)
            draw = ImageDraw.Draw(image)
            text = f"{matrix}"
            font_size = 30
            font = ImageFont.truetype('Pillow/Tests/fonts/FreeMonoBold.ttf', font_size)
            rect_shape = [(800, 1080 - (matrix.shape[0] + 1) * 3 * font_size),
                          (1400, 1080 - matrix.shape[1] * 3 * font_size)]
            draw.rectangle(rect_shape, fill="black")
            draw.text((800, 1080 - (matrix.shape[1] + 1) * 3 * font_size), text, fill=text_color,
                      font=font, align="left")
            image.save(save_image_path)

    def get_assignment_cost_matrix(self, tracker_bboxes, detector_bboxes):
        """
        Args:
            tracker_bboxes: a list of bounding boxes as [(xmin, ymin, xmax, ymax), ...]
            detector_bboxes: [(xmin, ymin, xmax, ymax), ...]

        Returns: a numpy array of dimensions (len(tracker_bboxes), len(detector_bboxes)) where each entry shows the cost of
        assigning the bounding boxes at the corresponding list indexes using iou
        """

        costs = []
        for tracker_bbox in tracker_bboxes:
            tracker_bbox = np.array([tracker_bbox])
            for detector_bbox in detector_bboxes:
                detector_bbox = np.array([detector_bbox])
                iou = bndbox_iou(tracker_bbox, detector_bbox)
                cost = 1 - iou
                costs.append(cost)

        costs = np.array(costs)
        costs = costs.reshape(len(tracker_bboxes), len(detector_bboxes))

        return costs

    def save_inputs_outputs(self, regenerate_sign_images=True):

        # construct the dataframe
        data = pd.DataFrame(self.inputs_outputs)
        headings = ["Tracklet Image",
                    "T Sign Longitude",
                    "T Sign Latitude",
                    "T BBOX Left",
                    "T BBOX Bottom",
                    "T BBOX Right",
                    "T BBOX Top",
                    "T NA",
                    "T Camera Longitude",
                    "T Camera Latitude",
                    "T Camera DOM",
                    "T Class",
                    "T Superclass",
                    "T Sign Image",
                    "Prediction Image",
                    "P Sign Longitude",
                    "P Sign Latitude",
                    "P BBOX Left",
                    "P BBOX Bottom",
                    "P BBOX Right",
                    "P BBOX Top",
                    "P NA",
                    "P Camera Longitude",
                    "P Camera Latitude",
                    "P Camera DOM",
                    "P Class",
                    "P Superclass",
                    "P Sign Image",
                    "Distance Metric"
                    ]
        data.columns = headings

        # add links to the sign pixel images
        sign_image_save_directory = Path(self.debug_directory) / "sign_images"
        if not sign_image_save_directory.exists():
            sign_image_save_directory.mkdir()
        if regenerate_sign_images:
            for i, row in data.iterrows():
                t_image_data = get_sign_image_data(row["Tracklet Image"],
                                                   int(row["T BBOX Left"]),
                                                   int(row["T BBOX Right"]),
                                                   int(row["T BBOX Bottom"]),
                                                   int(row["T BBOX Top"]))
                save_path = sign_image_save_directory / f"{i}t.png"
                save_image(save_path, t_image_data)
                data.at[i, 'T Sign Image'] = save_path
                p_image_data = get_sign_image_data(row["Prediction Image"],
                                                   int(row["P BBOX Left"]),
                                                   int(row["P BBOX Right"]),
                                                   int(row["P BBOX Bottom"]),
                                                   int(row["P BBOX Top"]))
                save_path = sign_image_save_directory / f"{i}p.png"
                save_image(save_path, p_image_data)
                data.at[i, 'P Sign Image'] = save_path


        # save the file
        save_path = Path(self.debug_directory) / 'inputs_outputs.csv'
        data.to_csv(save_path, index=False)


    def add_to_class_file(self, sign_class, class_file):
        """Adds the passed sign class to the class file."""

        with open(class_file, 'r') as f:
            classes = [x.strip() for x in f]

        if sign_class not in classes:
            classes.append(sign_class)
            classes = sorted(classes)

            with open(class_file, 'w') as f:
                f.writelines("\n".join(classes) + "\n")

    def cast_to_nparrays(self, list_of_dicts):
        """
        Cast predictions to numpy arrays

        Args:
            list_of_dicts:

        Returns:
        """
        count = len(list_of_dicts)
        images = np.empty((count, 1), dtype=object)
        classes = np.empty((count, 1), dtype=object)
        gps = np.empty((count, 2))
        bboxes = np.empty((count, 4))
        scores = np.empty((count, 1))

        for i, p in enumerate(list_of_dicts):
            images[i] = p['image']
            classes[i] = p['class']
            scores[i] = float(p['score'])
            gps[i, :] = np.array([float(p['lat']), float(p['lon'])])
            bboxes[i, :] = np.array(
                [int(p['xmin']), int(p['ymin']), int(p['xmax']), int(p['ymax'])]
            )

        return images, classes, gps, bboxes, scores

    def tracklet2sign(self, tracklet):
        """
        Reduces a tracklet containing several sign detections into a single sign prediction.

        Args:
            tracklet: List[Dict], A sequence of sign detections.

        Returns: Dict
            A single predicted sign.
        """

        # images, classes, locs, bboxes, scores, sides, pred_sides = cast_to_nparrays(tracklet)
        images, classes, locs, bboxes, scores = self.cast_to_nparrays(tracklet)

        # get most frequent and most recent class in the tracklet
        cls_counts = np.array([(classes[:, 0] == cc).sum() for cc in classes[:, 0]])
        cc = classes[::-1, 0]
        cls = cc[np.argmax(cls_counts[::-1])]

        # Calculate the weights for aggregating values over a tracklet
        # Weights are biased towards more recent predictions and more prevalent classes
        time_wgts = np.exp(np.arange(-len(scores[:, 0]) + 1, 1))
        time_wgts /= time_wgts.sum()

        cls_wgts = cls_counts / cls_counts.sum()

        wgts = time_wgts + cls_wgts
        wgts /= wgts.sum()

        # Calculate the confidence score
        # Score is reduced by confusion, e.g. when multiple classes are present in the tracklet
        conf = np.average(scores[:, 0], weights=wgts) * cls_counts.max() / len(classes)

        # Calculate the GPS coordinates
        gps = np.average(locs, weights=wgts, axis=0)
        # gps = locs[-1]

        bbox = np.average(bboxes, weights=wgts, axis=0)

        # find predicted and actual sign side from FOI
        sign = {
            'image': tracklet[-1]['image'],
            'class': cls,
            'lat': gps[0],
            'lon': gps[1],
            'score': conf,
            'xmin': bbox[0],
            'ymin': bbox[1],
            'xmax': bbox[2],
            'ymax': bbox[3],
        }

        return sign, classes, scores

    #####POST PROCESSING################################################################################################
    def remove_tracklets_by_length(self, length=1):
        """
        Removes all tracklets with sign count less than or equal to the specified length.
        This function is outdated you should REMOVE THIS EVENTUALLY.
        """

        new_tracklets = []
        for tracklet in self.dead_tracklets:
            if len(tracklet) > length:
                new_tracklets.append(tracklet)
        self.dead_tracklets = new_tracklets

    def filter_redundant_detections(self, detections):
        """Filters out redundant detections provided RetinaNet."""

        def merge_low_cost_detections(detection_list, filter_cost):

            # if fewer than 2 detections there is nothing to merge
            if len(detection_list) < 2:
                return detection_list

            bboxes = [get_bbox(d) for d in detection_list]
            costs = self.get_assignment_cost_matrix(bboxes, bboxes)
            np.fill_diagonal(costs, 1)

            # find an index at which the minimum cost occurs
            indices = np.where(costs == np.min(costs))[0]
            min_index = (indices[0], indices[int(len(indices) / 2)])

            if costs[tuple(min_index)] <= filter_cost:
                conf_0 = detections[min_index[0]]["score"]
                conf_1 = detections[min_index[1]]["score"]
                if conf_0 >= conf_1:
                    del detections[min_index[1]]
                else:
                    del detections[min_index[0]]

            return detections

        # loop until finished removing detections
        num_detections = len(detections) + 1
        while num_detections > len(detections):
            num_detections = len(detections)
            detections = merge_low_cost_detections(detections, self.detection_overlap_filter_threshold)

        return detections

    def filter_far_detections(self, detections):
        """Filters out far away detections whose predicted Y offset exceeds a certain threshold."""

        try:
            if len(detections) == 0:
                return detections

            filtered_detections = []
            for detection in detections:
                if float(detection['Y Offset']) < self.distance_filter_threshold:
                    filtered_detections.append(detection)
        except Exception as e:
            print(e)
            print(detections)
            input("enter")

        return filtered_detections

    def average_assembly_gps_preds(self, tracklets):
        """
        Reduces GPS error by identifying tracklets from signs that are a part of the same assemblies and averaging their
        predicted GPS coordinates.
        """

        raise NotImplementedError

    def remove_low_conf_tracklets(self, tracklets):
        """Removes tracklets containing few and low confidence predictions since these are likely false positives."""

        raise NotImplementedError

    def merge_similar_tracklets(self, tracklets):
        """
        Merges tracklets with similar predicted sign and class.
        Note that implementing this properly requires accounting for different signs of the same class being at similar
        GPS locations (such as assemblies).
        """

        raise NotImplementedError

    #####END POST PROCESSING############################################################################################


class OPENCVTracker(Tracker):

    def __init__(self, detections_directory, tracker_creation_func, distance_threshold=0.999, age_cutoff=2):
        super().__init__(detections_directory, distance_threshold=distance_threshold, age_cutoff=age_cutoff)
        self.tracker_creation_func = tracker_creation_func

    def generate_tracklets(self):
        for i, img in enumerate(self.images_to_track):

            lat, lon, _ = cam_gps(img)

            next_frame = str(img)
            frame_data = cv2.imread(next_frame)
            predictions = self.detections.get(next_frame)

            if predictions is not None:
                predictions = self.filter_redundant_detections(predictions)
                # Initialize tracklets
                if len(self.live_tracklets) == 0:
                    tracker = self.tracker_creation_func()
                    trackers = cv2.MultiTracker_create()
                    for p in predictions:
                        self.live_tracklets.append(([p], i))
                        trackers.add(tracker, frame_data, convert_bbox(get_bbox(p), reverse=True))
                else:
                    # Compute cost matrix
                    successes, tracker_bboxes = trackers.update(frame_data)
                    costs = self.get_assignment_cost_matrix([convert_bbox(b) for b in tracker_bboxes],
                                                       [get_bbox(p) for p in predictions])

                    # Use Hungarian algorithm to perform assignment
                    row_inds, col_inds = linear_sum_assignment(costs)

                    # Update tracklets based on the assignment
                    tracklet_indices_to_delete = []
                    for t_ind, p_ind in zip(row_inds, col_inds):

                        # if the pairing does not meet the distance metric threshold split it
                        if costs[t_ind, p_ind] < self.distance_threshold:
                            t, age = self.live_tracklets[t_ind]
                            t.append(predictions[p_ind])
                            self.live_tracklets[t_ind] = (t, i)

                        else:
                            tracklet_indices_to_delete.append(t_ind)
                            self.live_tracklets.append(([predictions[p_ind]], i))

                    # delete tracklets split due to not meeting distance threshold
                    for t_ind in sorted(tracklet_indices_to_delete, reverse=True):
                        t, age = self.live_tracklets[t_ind]
                        self.dead_tracklets.append(t)
                        del self.live_tracklets[t_ind]

                    # Unassigned predictions form new tracklets
                    for k in np.setdiff1d(range(len(predictions)), col_inds):
                        self.live_tracklets.append(([predictions[k]], i))

                    # remove tracklets that will be expired next iteration due to age
                    for j, (tracklet, age) in enumerate(self.live_tracklets):
                        if age <= i - self.age_cutoff:
                            self.dead_tracklets.append(tracklet)
                            del self.live_tracklets[j]

                    # create a new opencv tracker from the new tracklets
                    tracker = self.tracker_creation_func()
                    trackers = cv2.MultiTracker_create()

                    for tracklet in self.live_tracklets:
                        trackers.add(tracker, frame_data, convert_bbox(get_bbox(tracklet[0][-1]), reverse=True))

        # Terminate remaining tracklets
        for t, _ in self.live_tracklets:
            self.dead_tracklets.append(t)


class ARTSImageTracker(Tracker):
    """This ARTS tracker uses the neural network designed to use the 5D images as input."""

    def __init__(self, detections_directory, distance_threshold=0.7, age_cutoff=2):

        super().__init__(detections_directory, distance_threshold=distance_threshold, age_cutoff=age_cutoff)
        gpus = tf.config.experimental.list_physical_devices('GPU')
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)

        self.pure_image_similarity_model = load_and_build_image_model()
        self.sample_generator = get_sample_image_generator('/home/vision/Documents/no_sign_images')

    def generate_tracklets(self):
        for i, img in enumerate(self.images_to_track):

            lat, lon, _ = cam_gps(img)

            # Remove expired tracklets
            for j, (tracklet, age) in enumerate(self.live_tracklets):
                if age < i - self.age_cutoff:
                    self.dead_tracklets.append(tracklet)
                    del self.live_tracklets[j]

            # get the detections
            next_frame = str(img)
            predictions = self.detections.get(next_frame)

            if predictions is not None:

                # post-process the detections
                predictions = self.filter_redundant_detections(predictions)
                # predictions = self.filter_far_detections(predictions)

                # Initialize tracklets
                if len(self.live_tracklets) == 0:
                    for p in predictions:
                        self.live_tracklets.append(([p], i))
                else:
                    # Compute a distance matrix using a learned heuristic
                    dists = self.compute_distance_matrix(self.live_tracklets, predictions)
                    self.distance_matrices[img] = dists

                    # Use Hungarian algorithm to perform assignment
                    row_inds, col_inds = linear_sum_assignment(dists)

                    # Update tracklets based on the assignment
                    tracklet_indices_to_delete = []
                    for t_ind, p_ind in zip(row_inds, col_inds):

                        # if the pairing does not meet the distance metric threshold split it
                        if dists[t_ind, p_ind] < self.distance_threshold:
                            t, age = self.live_tracklets[t_ind]
                            t.append(predictions[p_ind])
                            self.live_tracklets[t_ind] = (t, i)

                        else:
                            tracklet_indices_to_delete.append(t_ind)
                            self.live_tracklets.append(([predictions[p_ind]], i))

                    # delete tracklets split due to not meeting distance threshold
                    for t_ind in sorted(tracklet_indices_to_delete, reverse=True):
                        t, age = self.live_tracklets[t_ind]
                        self.dead_tracklets.append(t)
                        del self.live_tracklets[t_ind]

                    # Unassigned predictions form new tracklets
                    for k in np.setdiff1d(range(len(predictions)), col_inds):
                        self.live_tracklets.append(([predictions[k]], i))

        # Terminate remaining tracklets
        for t, _ in self.live_tracklets:
            self.dead_tracklets.append(t)

    def compute_distance_matrix(
            self,
            tracklets,
            predictions,
            image_dimensions=(int(1920/2), int(1080/2), 4),
    ):

        tracklet_image_list, prediction_image_list = [], []

        n_tracklets = len(tracklets)
        # last_detections = [tracklet[-1] for tracklet, _ in tracklets]
        images_1 = np.zeros((n_tracklets, image_dimensions[1], image_dimensions[0], image_dimensions[2]))

        for i, (tracklet, _) in enumerate(tracklets):
            t, _, _ = self.tracklet2sign(tracklet)
            last_detection = tracklet[-1]

            # get image data
            tracklet_image_list.append(last_detection['image'])
            image_data = load_and_resize_image(last_detection['image'], image_dimensions[:2])
            bbox, depth, bboxes, depths = self.sample_generator.get_all_bboxes_and_depths(last_detection, self.detections.get(last_detection['image']))
            image_data = self.sample_generator.add_detection_mask(bbox, bboxes, image_data)
            images_1[i, :, :, :] = image_data

        n_predictions = len(predictions)
        images_2 = np.zeros((n_predictions, image_dimensions[1], image_dimensions[0], image_dimensions[2]))

        for i, pred in enumerate(predictions):
            prediction_image_list.append(pred['image'])

            # get image data
            image_data = load_and_resize_image(pred['image'], image_dimensions[:2])
            bbox, depth, bboxes, depths = self.sample_generator.get_all_bboxes_and_depths(pred, predictions)
            image_data = self.sample_generator.add_detection_mask(bbox, bboxes, image_data, seq='pred')
            images_2[i, :, :, :] = image_data

        # Collect the Cartesian product of tracklet/prediction pairings
        images_1 = np.repeat(images_1, n_predictions, axis=0)
        tracklet_image_list = np.repeat(tracklet_image_list, n_predictions, axis=0)

        images_2 = np.tile(images_2, (n_tracklets, 1, 1, 1))
        prediction_image_list = np.tile(prediction_image_list, (n_tracklets,))

        dists = self.pure_image_similarity_model.predict([images_1, images_2])

        # print(tracklet_image_list)
        # print(prediction_image_list)
        # print(dists)
        # input("Enter")

        return dists.reshape((n_tracklets, n_predictions))

    def get_detections(self, image_path):

        csv_path = Path(image_path).parts
        csv_path = Path(*csv_path[:-3]) / (csv_path[-3] + '_all.csv')
        all_detections = load_detections(csv_path)
        detections = all_detections.get(image_path)

        return detections


class ARTSTracker(Tracker):

    def __init__(self, detections_directory, distance_threshold=0.7, age_cutoff=2):

        super().__init__(detections_directory, distance_threshold=distance_threshold, age_cutoff=age_cutoff)

        # Configures tensorflow so that it does not allocate all GPU memory to a single process
        # This allows the tracker to be parallelized while also utilizing GPU acceleration
        # tf.disable_v2_behavior()
        # config = tf.ConfigProto()
        # config.gpu_options.allow_growth = True
        # sess = tf.Session(config=config)
        # keras.backend.set_session(sess)

        # tf.config.gpu.set_per_process_memory_growth(True)

        gpus = tf.config.experimental.list_physical_devices('GPU')
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)

        # TODO: add code for training merged network
        # if not Path('distance_metric_model.h5'):
        #     pass
        # self.distance_metric_model = load_model('good_dense_concatenated.h5')
        # self.distance_metric_model = load_model(MODEL_NAME)
        # self.distance_metric_model = load_model('distance_metric_model_976_918.h5')
        self.distance_metric_model = load_and_build_model()
        self.sample_generator = get_sample_generator('/home/vision/Documents/singles_224')

    def generate_tracklets(self):
        for i, img in enumerate(self.images_to_track):

            lat, lon, _ = cam_gps(img)

            # Remove expired tracklets
            for j, (tracklet, age) in enumerate(self.live_tracklets):
                if age < i - self.age_cutoff:
                    self.dead_tracklets.append(tracklet)
                    del self.live_tracklets[j]

            # get the detections
            next_frame = str(img)
            predictions = self.detections.get(next_frame)

            if predictions is not None:

                # post-process the detections
                predictions = self.filter_redundant_detections(predictions)
                # predictions = self.filter_far_detections(predictions)

                # Initialize tracklets
                if len(self.live_tracklets) == 0:
                    for p in predictions:
                        self.live_tracklets.append(([p], i))
                else:
                    # Compute a distance matrix using a learned heuristic
                    dists = self.compute_distance_matrix(self.live_tracklets, predictions)
                    self.distance_matrices[img] = dists

                    # Use Hungarian algorithm to perform assignment
                    row_inds, col_inds = linear_sum_assignment(dists)

                    # Update tracklets based on the assignment
                    tracklet_indices_to_delete = []
                    for t_ind, p_ind in zip(row_inds, col_inds):

                        # if the pairing does not meet the distance metric threshold split it
                        if dists[t_ind, p_ind] < self.distance_threshold:
                            t, age = self.live_tracklets[t_ind]
                            t.append(predictions[p_ind])
                            self.live_tracklets[t_ind] = (t, i)

                        else:
                            tracklet_indices_to_delete.append(t_ind)
                            self.live_tracklets.append(([predictions[p_ind]], i))

                    # delete tracklets split due to not meeting distance threshold
                    for t_ind in sorted(tracklet_indices_to_delete, reverse=True):
                        t, age = self.live_tracklets[t_ind]
                        self.dead_tracklets.append(t)
                        del self.live_tracklets[t_ind]

                    # Unassigned predictions form new tracklets
                    for k in np.setdiff1d(range(len(predictions)), col_inds):
                        self.live_tracklets.append(([predictions[k]], i))

        # Terminate remaining tracklets
        for t, _ in self.live_tracklets:
            self.dead_tracklets.append(t)

    def compute_distance_matrix(
            self,
            tracklets,
            predictions,
            class_file='/home/vision/VTransVision/VTrans-AI/src/data/rawdata/tracker_labels.txt',
            image_dimensions=(224, 224),
    ):

        tracklet_image_list, prediction_image_list = [], []

        class2ind, superclass2ind = get_class_superclass_ind(class_file)

        n_tracklets = len(tracklets)
        features_1 = np.zeros((len(tracklets), 10))
        classes_1 = np.zeros((len(tracklets), 1))
        superclasses_1 = np.zeros((len(tracklets), 1))
        image_datas_1 = np.zeros((len(tracklets), *image_dimensions, 3))
        matrices_1 = np.zeros((len(tracklets), *MATRIX_DIM, 1))
        full_matrices_1 = np.zeros((len(tracklets), *MATRIX_DIM, MATRIX_THIRD_DIM))

        for i, (tracklet, _) in enumerate(tracklets):
            t, _, _ = self.tracklet2sign(tracklet)
            last_detection = tracklet[-1]

            c_lat, c_lon, _ = cam_gps(last_detection['image'])
            c_dom = cam_dom(last_detection['image'])

            features_1[i, :] = np.array([
                last_detection['lat'],
                last_detection['lon'],
                last_detection['xmin'],
                last_detection['ymin'],
                last_detection['xmax'],
                last_detection['ymax'],
                None,
                c_lat,
                c_lon,
                c_dom,
            ])

            t_class = last_detection['class']
            if t_class in class2ind:
                classes_1[i] = class2ind[t_class]
                superclasses_1[i] = superclass2ind[get_superclass(t_class)]
            else:
                print(f"No labels found for class {last_detection['class']}.")
                self.add_to_class_file(t_class, class_file)
                print(class2ind)
                print(class2ind.values())
                classes_1[i] = np.random.choice(list(class2ind.values()))
                superclasses_1[i] = np.random.choice(list(superclass2ind.values()))

            # get image data
            tracklet_image_list.append(last_detection['image'])
            image_data = get_sign_image_data(last_detection['image'],
                                             int(last_detection['xmin']),
                                             int(last_detection['xmax']),
                                             int(last_detection['ymin']),
                                             int(last_detection['ymax']),
                                             image_dimensions)
            image_datas_1[i, :, :, :] = image_data

            # get detection matrices
            matrices_1[i, :, :, :] = self.sample_generator.get_detection_matrix(last_detection['image'],
                                                                                (int(last_detection['xmin']),
                                                                                 int(last_detection['xmax']),
                                                                                 int(last_detection['ymin']),
                                                                                 int(last_detection['ymax'])),
                                                                                csv_path=self.detections_csv_path)
            full_matrices_1[i, :, :, :] = self.sample_generator.get_full_detection_matrix(last_detection['image'], self.detections_csv_path)

        n_predictions = len(predictions)
        features_2 = np.zeros((len(predictions), 10))
        classes_2 = np.zeros((len(predictions), 1))
        superclasses_2 = np.zeros((len(predictions), 1))
        image_datas_2 = np.zeros((len(predictions), *image_dimensions, 3))
        matrices_2 = np.zeros((len(predictions), *MATRIX_DIM, 1))
        full_matrices_2 = np.zeros((len(predictions), *MATRIX_DIM, MATRIX_THIRD_DIM))

        for i, pred in enumerate(predictions):
            c_lat, c_lon, _ = cam_gps(pred['image'])
            c_dom = cam_dom(pred['image'])

            features_2[i, :] = np.array([
                pred['lat'],
                pred['lon'],
                pred['xmin'],
                pred['ymin'],
                pred['xmax'],
                pred['ymax'],
                None,
                c_lat,
                c_lon,
                c_dom,
            ])

            p_class = pred['class']
            if p_class in class2ind:
                classes_2[i] = class2ind[p_class]
                superclasses_2[i] = superclass2ind[get_superclass(p_class)]
            else:
                classes_2[i] = np.random.choice(list(class2ind.values()))
                superclasses_2[i] = np.random.choice(list(superclass2ind.values()))

            # get image data
            prediction_image_list.append(pred['image'])
            image_data = get_sign_image_data(pred['image'],
                                             int(pred['xmin']),
                                             int(pred['xmax']),
                                             int(pred['ymin']),
                                             int(pred['ymax']),
                                             image_dimensions)
            image_datas_2[i, :, :, :] = image_data

            # get detection matrix
            matrices_2[i, :, :, :] = self.sample_generator.get_detection_matrix(pred['image'],
                                                                                (int(pred['xmin']),
                                                                                 int(pred['xmax']),
                                                                                 int(pred['ymin']),
                                                                                 int(pred['ymax'])),
                                                                                self.detections_csv_path)
            full_matrices_2[i, :, :, :] = self.sample_generator.get_full_detection_matrix(pred['image'], self.detections_csv_path)

        # Collect the Cartesian product of tracklet/prediction pairings
        features_1 = np.repeat(features_1, n_predictions, axis=0)
        classes_1 = np.repeat(classes_1, n_predictions, axis=0)
        superclasses_1 = np.repeat(superclasses_1, n_predictions, axis=0)
        image_datas_1 = np.repeat(image_datas_1, n_predictions, axis=0)
        matrices_1 = np.repeat(matrices_1, n_predictions, axis=0)
        full_matrices_1 = np.repeat(full_matrices_1, n_predictions, axis=0)
        tracklet_image_list = np.repeat(tracklet_image_list, n_predictions, axis=0)

        features_2 = np.tile(features_2, (n_tracklets, 1))
        classes_2 = np.tile(classes_2, (n_tracklets, 1))
        superclasses_2 = np.tile(superclasses_2, (n_tracklets, 1))
        image_datas_2 = np.tile(image_datas_2, (n_tracklets, 1, 1, 1))
        matrices_2 = np.tile(matrices_2, (n_tracklets, 1, 1, 1))
        full_matrices_2 = np.tile(full_matrices_2, (n_tracklets, 1, 1, 1))
        prediction_image_list = np.tile(prediction_image_list, (n_tracklets,))

        modded_features = self.sample_generator.get_modded_batch([features_1,
                                                                  classes_1,
                                                                  superclasses_1,
                                                                  image_datas_1,
                                                                  matrices_1,
                                                                  full_matrices_1,
                                                                  features_2,
                                                                  classes_2,
                                                                  superclasses_2,
                                                                  image_datas_2,
                                                                  matrices_2,
                                                                  full_matrices_2])

        # dists = convert_to_distances(self.distance_metric_model.predict(modded_features))
        dists = self.distance_metric_model.predict(modded_features)

        self.store_inputs_outputs(tracklet_image_list,
                                  features_1,
                                  classes_1,
                                  superclasses_1,
                                  image_datas_1,
                                  matrices_1,
                                  full_matrices_1,
                                  prediction_image_list,
                                  features_2,
                                  classes_2,
                                  superclasses_2,
                                  image_datas_2,
                                  matrices_2,
                                  full_matrices_2,
                                  dists)

        return dists.reshape((n_tracklets, n_predictions))

    def store_inputs_outputs(self,
                             tracklet_image_list,
                             features_1,
                             classes_1,
                             superclasses_1,
                             image_datas_1,
                             matrices_1,
                             full_matrices_1,
                             prediction_image_list,
                             features_2,
                             classes_2,
                             superclasses_2,
                             image_datas_2,
                             matrices_2,
                             full_matrices_2,
                             dists):

        for i in range(tracklet_image_list.shape[0]):
            input_list = []
            input_list.append(tracklet_image_list[i])
            input_list += list(features_1[i])
            input_list += list(classes_1[i])
            input_list += list(superclasses_1[i])
            input_list.append("NONE") # don't add the image_datas_1 array
            input_list.append(prediction_image_list[i])
            input_list += list(features_2[i])
            input_list += list(classes_2[i])
            input_list += list(superclasses_2[i])
            input_list.append("NONE")  # don't add the image_datas_2 array
            input_list.append(dists[i, 0])
            self.inputs_outputs.append(input_list)

class PerfectTracker(Tracker):

    def __init__(self, detections_directory):
        super().__init__(detections_directory)

    def generate_tracklets(self):
        for i, img in enumerate(self.images_to_track):

            lat, lon, _ = cam_gps(img)

            # Remove expired tracklets
            for j, (tracklet, age) in enumerate(self.live_tracklets):
                if age < i - self.age_cutoff:
                    self.dead_tracklets.append(tracklet)
                    del self.live_tracklets[j]

            # get the detections
            next_frame = str(img)
            predictions = self.detections.get(next_frame)

            if predictions is not None:

                # Initialize tracklets
                if len(self.live_tracklets) == 0:
                    for p in predictions:
                        self.live_tracklets.append(([p], i))

                # pair any annotations that perfectly match the last entry in the tracklet
                else:
                    new_tracklets = []
                    for new_sign in predictions:
                        paired = False
                        for j, (tracklet, age) in enumerate(self.live_tracklets):
                            prev_sign = tracklet[-1]
                            if self.are_identical(new_sign, prev_sign) and not paired:
                                tracklet.append(new_sign)
                                self.live_tracklets[j] = (tracklet, i)
                                paired = True
                        if not paired:
                            new_tracklets.append(([new_sign], i))

                    for new_tracklet in new_tracklets:
                        self.live_tracklets.append(new_tracklet)

        # Terminate remaining tracklets
        for t, _ in self.live_tracklets:
            self.dead_tracklets.append(t)

    def are_identical(self, sign_1, sign_2, tolerance=.00005):  # .000000001
        """Returns a boolean indicating whether the passed signs the same latitude, longitude, and class."""

        return sign_1['class'] == sign_2['class'] \
               and abs(float(sign_1['lon']) - float(sign_2['lon'])) < tolerance \
               and abs(float(sign_1['lat']) - float(sign_2['lat'])) < tolerance
