#!/usr/bin/env bash

# Run this from VTrans-AI/src
# Runs the object tracking algorithm on annotations, providing a best case
# performance analysis.
# Uses the GNU tool Parallel to speed up execution
# If the tracker uses GPU acceleration you may need to reduce --jobs to 1


#parallel --jobs 100% command "python tracker/track.py {} --tracker_type=ARTS -c=.98" ::: ../data/rawdata/*/*_all.csv

for segment in data/rawdata/*/*_all.csv
do
  python tracker/track.py $segment --tracker_type=PERFECT -d -s -c=.9
done