#!/usr/bin/env python

"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
from pathlib import Path
from multiprocessing import Pool
from functools import partial

import numpy as np
import pandas as pd
from geopy.distance import distance
from scipy.optimize import linear_sum_assignment
from scipy.spatial.distance import cdist
from sklearn.cluster import dbscan

from distance_metric_model import get_superclass


def parse_args():
    parser = argparse.ArgumentParser(
        description='Evaluates predictions output by the object tracker.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        'folder',
        help='Path to a directory containing sign predictions and annotations.',
    )
    parser.add_argument(
        '--gps_threshold',
        help='Threshold on GPS error that localizes detections.',
        default=15.0,
        type=float,
    )

    return parser.parse_args()


def evaluate_road_segment(annotation_path, prediction_path, eps):

    y_true = pd.read_csv(annotation_path)
    y_true.columns = [x.strip() for x in y_true.columns]
    y_true = y_true[['class', 'lat', 'lon']]
    y_true['indicator'] = np.ones(len(y_true))
    # y_true['class'] = y_true['class'].apply(get_superclass)  # convert dataframe to superclass
    # print(y_true.shape)

    y_pred = pd.read_csv(prediction_path)
    y_pred.columns = [x.strip() for x in y_pred.columns]
    y_pred = y_pred[['class', 'lat', 'lon']]
    y_pred['indicator'] = np.zeros(len(y_pred))
    # y_pred['class'] = y_pred['class'].apply(get_superclass)  # convert dataframe to superclass
    # print(y_pred.shape)

    a_count = len(y_true)
    p_count = len(y_pred)

    points = pd.concat([y_true, y_pred], ignore_index=True, sort=False)
    dists = cdist(
        points[['lat', 'lon']],
        points[['lat', 'lon']],
        lambda a, b: float(distance(a, b).meters),
    )
    _, labels = dbscan(dists, eps=eps, min_samples=2, metric='precomputed')

    chunks = []

    for label in np.arange(np.max(labels)):
        cluster = points.iloc[np.where(labels == label)]
        cluster_true = cluster[cluster.indicator == 1]
        cluster_pred = cluster[cluster.indicator == 0]

        cluster_dists = cdist(
            cluster_true[['lat', 'lon', 'class']],
            cluster_pred[['lat', 'lon', 'class']],
            lambda a, b: class_dist(a, b),
        )

        row_ind, col_ind = linear_sum_assignment(cluster_dists)

        cluster_true = cluster_true.iloc[row_ind]
        cluster_true = cluster_true.drop('indicator', axis=1)
        cluster_true.columns = ['true_' + x for x in cluster_true.columns]
        cluster_true.index = np.arange(len(cluster_true))

        cluster_pred = cluster_pred.iloc[col_ind]
        cluster_pred = cluster_pred.drop('indicator', axis=1)
        cluster_pred.columns = ['pred_' + x for x in cluster_pred.columns]
        cluster_pred.index = np.arange(len(cluster_pred))

        cluster_dists = pd.Series(cluster_dists[row_ind, col_ind], name='distance')

        chunks.append(pd.concat([cluster_true, cluster_pred, cluster_dists], axis=1))

    if chunks:
        chunks = pd.concat(chunks, ignore_index=True)
    else:
        chunks = None

    # print(annotation_path)
    # print(f"True Positives: {len(chunks)}")
    # print(f"False Negatives: {a_count - len(chunks)}")
    # print(f"False Positives: {p_count - len(chunks)}")
    # input("Enter")

    return a_count, p_count, chunks


def class_dist(a, b):
    geo_dist = float(distance(a[:2], b[:2]).meters)
    class_dist = 0 if a[2] == b[2] else 1
    # class_dist = 0
    return geo_dist + class_dist


def main():
    args = parse_args()
    path = Path(args.folder)

    job = partial(evaluate_road_segment, eps=args.gps_threshold)
    args = zip(sorted(path.glob('*_unique.csv')), sorted(path.glob('*_all_signs.csv')))

    with Pool() as pool:
        results = pool.starmap(job, args)

    annotation_counts, prediction_counts, chunks = zip(*results)

    a_count = np.sum(annotation_counts)
    p_count = np.sum(prediction_counts)
    results = pd.concat(chunks, ignore_index=True).sort_values('distance')

    # results.drop('pred_class', axis=1).to_csv(path / f'{path.stem}_tracker_evaluation.csv')
    results.to_csv(path / f'{path.stem}_tracker_evaluation.csv')
    with open(path / f'{path.stem}_gps_evaluation.txt', 'a') as f:
        print(pd.Timestamp.now(), file=f)
        print(f'Annotation Count: {a_count}', file=f)
        print(f'Prediction Count: {p_count}', file=f)
        print(f'True Positives:   {len(results)}', file=f)
        print(f'False Negatives:  {a_count - len(results)}', file=f)
        print(f'False Positives:  {p_count - len(results)}', file=f)
        print(results.distance.describe(), file=f)
        print('\n\n', file=f)


if __name__ == '__main__':
    main()
