#!/usr/bin/env bash

# Run this from VTrans-AI/src
# evaluates the GPS coordinates localized from the tracklets using different methods


python tracker/gps_evaluation.py data/gps_comparison/ --gps_threshold=15
