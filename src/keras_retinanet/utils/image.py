"""
Copyright 2017-2018 Fizyr (https://fizyr.com)
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from __future__ import division
import keras
import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image
from pathlib import Path

from .transform import change_transform_origin


def read_image_bgr(path):
    """ Read an image in BGR format.

    Args
        path: Path to the image.
    """
    image = np.asarray(Image.open(path).convert('RGB'))
    return image[:, :, ::-1].copy()


def preprocess_image(x, mode='caffe'):
    """ Preprocess an image by subtracting the ImageNet mean.

    Args
        x: np.array of shape (None, None, 3) or (3, None, None).
        mode: One of "caffe" or "tf".
            - caffe: will zero-center each color channel with
                respect to the ImageNet dataset, without scaling.
            - tf: will scale pixels between -1 and 1, sample-wise.

    Returns
        The input with the ImageNet mean subtracted.
    """
    # mostly identical to "https://github.com/keras-team/keras-applications/blob/master/keras_applications/imagenet_utils.py"
    # except for converting RGB -> BGR since we assume BGR already
    x = x.astype(keras.backend.floatx())
    if mode == 'tf':
        x /= 127.5
        x -= 1.
    elif mode == 'caffe':
        x[..., 0] -= 103.939
        x[..., 1] -= 116.779
        x[..., 2] -= 123.68

    return x


def adjust_transform_for_image(transform, image, relative_translation):
    """ Adjust a transformation for a specific image.

    The translation of the matrix will be scaled with the size of the image.
    The linear part of the transformation will adjusted so that the origin of the transformation will be at the center of the image.
    """
    height, width, channels = image.shape

    result = transform

    # Scale the translation with the image size if specified.
    if relative_translation:
        result[0:2, 2] *= [width, height]

    # Move the origin of transformation.
    result = change_transform_origin(transform, (0.5 * width, 0.5 * height))

    return result


class TransformParameters:
    """ Struct holding parameters determining how to apply a transformation to an image.

    Args
        fill_mode:             One of: 'constant', 'nearest', 'reflect', 'wrap'
        interpolation:         One of: 'nearest', 'linear', 'cubic', 'area', 'lanczos4'
        cval:                  Fill value to use with fill_mode='constant'
        relative_translation:  If true (the default), interpret translation as a factor of the image size.
                               If false, interpret it as absolute pixels.
    """
    def __init__(
        self,
        fill_mode            = 'nearest',
        interpolation        = 'linear',
        cval                 = 0,
        relative_translation = True,
    ):
        self.fill_mode            = fill_mode
        self.cval                 = cval
        self.interpolation        = interpolation
        self.relative_translation = relative_translation

    def cvBorderMode(self):
        if self.fill_mode == 'constant':
            return cv2.BORDER_CONSTANT
        if self.fill_mode == 'nearest':
            return cv2.BORDER_REPLICATE
        if self.fill_mode == 'reflect':
            return cv2.BORDER_REFLECT_101
        if self.fill_mode == 'wrap':
            return cv2.BORDER_WRAP

    def cvInterpolation(self):
        if self.interpolation == 'nearest':
            return cv2.INTER_NEAREST
        if self.interpolation == 'linear':
            return cv2.INTER_LINEAR
        if self.interpolation == 'cubic':
            return cv2.INTER_CUBIC
        if self.interpolation == 'area':
            return cv2.INTER_AREA
        if self.interpolation == 'lanczos4':
            return cv2.INTER_LANCZOS4


def apply_transform(matrix, image, params):
    """
    Apply a transformation to an image.

    The origin of transformation is at the top left corner of the image.

    The matrix is interpreted such that a point (x, y) on the original image is moved to transform * (x, y) in the generated image.
    Mathematically speaking, that means that the matrix is a transformation from the transformed image space to the original image space.

    Args
      matrix: A homogeneous 3 by 3 matrix holding representing the transformation to apply.
      image:  The image to transform.
      params: The transform parameters (see TransformParameters)
    """
    output = cv2.warpAffine(
        image,
        matrix[:2, :],
        dsize       = (image.shape[1], image.shape[0]),
        flags       = params.cvInterpolation(),
        borderMode  = params.cvBorderMode(),
        borderValue = params.cval,
    )
    return output


def add_gaussian_noise(image, loc=0, scale=1):
    """
    Applies gaussian noise to an image.
    Args:
        loc: The center (or mean) value for the gaussian noise.
        scale: The scale (or standard deviation) of the distribution over which values will be selected.
        image: The image to add noise to.

    Returns:
        An image with gaussian noise applied.

    """
    image += np.random.normal(loc, scale, image.shape)
    return (image)


def cutout(image, bbox, p=0.5):
    """
    Peforms cutout on the image.
    Args:
        image: a numpy array representing an image
        bbox: a list of [left_x, top_y, right_x, bottom_y] representing the coordinates of the bounding box
        p: a value from 0 to 1 representing the probability that cutout is performed

    Returns:
        An image with cutout performed.

    """
    img_h, img_w, img_c = image.shape

    #bbox = correct_bounding_box_coordinates(image, bbox)

    p_1 = np.random.rand()

    if p_1 > p:
        return image

    height = np.random.randint(bbox[3] - bbox[1])
    width = np.random.randint(bbox[2] - bbox[0])
    top_left_coordinate = (np.random.randint(bbox[0], bbox[2]), np.random.randint(bbox[1], bbox[3]))

    c = np.random.uniform(0, 255, (height, width, img_c))

    try:
        image[top_left_coordinate[1]:top_left_coordinate[1] + height, top_left_coordinate[0]:top_left_coordinate[0] + width, :] = c
    except Exception as e:
        # sometimes the bounding box coordinates don't make sense which causes an exception
        # in this case, we simply return the unmodified image
        return image

    return image

def correct_bounding_box_coordinates(image, bounding_box):
    """
    Fixes any issues in the bounding box coordinates (such as not being within the bounds of the image)
    returns correct coordinates
    Args:
        image: a numpy array representing image data
        bounding_box: a list of [left_x, top_y, right_x, bottom_y] representing the coordinates of the bounding box

    Returns:
        a corrected list representing the bounding box
    """
    limits = [(0, image.shape[1]-1), (0, image.shape[0]-1), (1, image.shape[1]), (1, image.shape[0])]
    for i in range(len(bounding_box)):
        limit = limits[i]
        if (bounding_box[i] < limit[0]):
            bounding_box[i] = limit[0]
        if (bounding_box[i] > limit[1]):
            bounding_box[i] = limit[1]
    return (bounding_box)


def compute_resize_scale(image_shape, min_side=800, max_side=1333):
    """ Compute an image scale such that the image size is constrained to min_side and max_side.

    Args
        min_side: The image's min side will be equal to min_side after resizing.
        max_side: If after resizing the image's max side is above max_side, resize until the max side is equal to max_side.

    Returns
        A resizing scale.
    """
    (rows, cols, _) = image_shape

    smallest_side = min(rows, cols)

    # rescale the image so the smallest side is min_side
    scale = min_side / smallest_side

    # check if the largest side is now greater than max_side, which can happen
    # when images have a large aspect ratio
    largest_side = max(rows, cols)
    if largest_side * scale > max_side:
        scale = max_side / largest_side

    return scale


def resize_image(img, min_side=800, max_side=1333):
    """ Resize an image such that the size is constrained to min_side and max_side.

    Args
        min_side: The image's min side will be equal to min_side after resizing.
        max_side: If after resizing the image's max side is above max_side, resize until the max side is equal to max_side.

    Returns
        A resized image.
    """
    # compute scale to resize the image
    scale = compute_resize_scale(img.shape, min_side=min_side, max_side=max_side)

    # resize the image with the computed scale
    img = cv2.resize(img, None, fx=scale, fy=scale)

    return img, scale


# saves the augmented images
def save_image(image, path, image_save_name=None):
    if (not image_save_name):
        image_save_name = str(np.random.randint(0, 10000)) + "augmented.jpeg"
    out = image / 1
    out[..., 0] += 103.939
    out[..., 1] += 116.779
    out[..., 2] += 123.68
    out = np.clip(out, a_min=0, a_max=255)
    out = out / 255
    swap_channels(out, 0, 2)
    plt.imsave(path / image_save_name, out)


def swap_channels(image, channel_1, channel_2):
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            value_1 = image[i, j, channel_1]
            value_2 = image[i, j, channel_2]
            image[i, j, channel_1] = value_2
            image[i, j, channel_2] = value_1
