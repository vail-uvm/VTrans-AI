"""
Copyright 2017-2018 Fizyr (https://fizyr.com)
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from .colors import label_color
from .anchors import calc_dist


def draw_box(image, box, color=None, thickness=2):
    """ Draws a box on an image with a given color.

    # Arguments
        image     : The image to draw on.
        box       : A list of 4 elements (x1, y1, x2, y2).
        color     : The color of the box.
        thickness : The thickness of the lines to draw a box with.
    """
    b = np.array(box).astype(int)
    if color is None:  # draw ground truth (white)
        cv2.rectangle(image, (b[0], b[1]), (b[2], b[3]), (255, 255, 255), thickness, cv2.LINE_AA)
    else:  # draw predictions (colors)
        cv2.rectangle(image, (b[0], b[1]), (b[2], b[3]), color, thickness, cv2.LINE_AA)


def draw_caption(image, box, caption, color=None, thickness=2):
    """ Draws a caption above the box in an image.

    # Arguments
        image      : The image to draw on.
        box        : A list of 4 elements (x1, y1, x2, y2).
        caption    : String containing the text to draw.
    """
    b = np.array(box).astype(int)

    if color is None:  # draw ground truth (white)
        cv2.putText(image, caption, (b[0], b[1] - 10), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 0), thickness)
        cv2.putText(image, caption, (b[0], b[1] - 10), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), thickness)
    else:  # draw predictions (colors)
        cv2.putText(image, caption, (b[0], b[1] - 25), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 0), thickness)
        cv2.putText(image, caption, (b[0], b[1] - 25), cv2.FONT_HERSHEY_PLAIN, 1, color, thickness)


def draw_dist_tag(image, box, caption, color=None, thickness=2):
    """ Draws a caption below the box in an image.

    # Arguments
        image   : The image to draw on.
        box     : A list of 4 elements (x1, y1, x2, y2).
        caption : String containing the text to draw.
    """
    b = np.array(box).astype(int)

    if color is None:  # draw ground truth (white)
        cv2.putText(image, caption, (b[0], b[3] + 15), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 0), thickness)
        cv2.putText(image, caption, (b[0], b[3] + 15), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), thickness)
    else:  # draw predictions (colors)
        cv2.putText(image, caption, (b[0], b[3] + 30), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 0), thickness)
        cv2.putText(image, caption, (b[0], b[3] + 30), cv2.FONT_HERSHEY_PLAIN, 1, color, thickness)


def draw_detections(image, boxes, distances, scores, labels, label_to_name=None, score_threshold=0.5):
    """ Draws detections in an image.

    # Arguments
        image           : The image to draw on.
        boxes           : A [N, 4] matrix (x1, y1, x2, y2).
        distances       : A [N, 2] matrix (dlat, dlon) distances between camera & object.
        scores          : A list of N classification scores.
        labels          : A list of N labels.
        label_to_name   : (optional) Functor for mapping a label to a name.
        score_threshold : Threshold used for determining what detections to draw.
    """
    # sort detections based on classification scores
    inds = np.argsort(scores)
    scores = scores[inds]
    boxes = boxes[inds]
    distances = distances[inds]
    labels = labels[inds]

    selection = np.where(scores > score_threshold)[0]

    for i in selection:
        c = label_color(labels[i])
        draw_box(image, boxes[i, :], color=c)

        caption = (label_to_name(labels[i]) if label_to_name else labels[i]) + ': {0:.2f}'.format(scores[i])
        draw_caption(image, boxes[i, :], caption, color=c)

        tag = '{0:.2f}m'.format(calc_dist(distances[i, :]))
        draw_dist_tag(image, boxes[i, :], tag, color=c)


def draw_annotations(image, annotations, label_to_name=None):
    """ Draws annotations in an image.

    # Arguments
        image         : The image to draw on.
        annotations   : A dictionary containing:
                'bboxes' - (shaped [N, 4])
                'glocs'  - (shaped [N, 2])
                'labels' - (shaped [N])
        label_to_name : (optional) Functor for mapping a label to a name.
    """
    assert('bboxes' in annotations)
    assert('glocs' in annotations)
    assert('labels' in annotations)
    assert(annotations['bboxes'].shape[0] == annotations['labels'].shape[0])
    assert(annotations['glocs'].shape[0] == annotations['labels'].shape[0])

    for i in range(annotations['bboxes'].shape[0]):
        label = annotations['labels'][i]
        draw_box(image, annotations['bboxes'][i])

        caption = '{}'.format(label_to_name(label) if label_to_name else label)
        draw_caption(image, annotations['bboxes'][i], caption)

        tag = '{0:.2f}m'.format(calc_dist(annotations['glocs'][i]))
        draw_dist_tag(image, annotations['bboxes'][i], tag)


def plot_hist(data, xlabel, bins=None):
    """ Generic function to plot histograms """
    plt.style.use('bmh')
    fig, ax = plt.subplots(figsize=(10, 8))
    plt.hist(data, bins=bins)
    plt.xlabel(xlabel, fontsize=22)
    plt.ylabel('Proportion of classes', fontsize=22)
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
