"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


import math
import numpy as np
from PIL import Image, ExifTags


def rotate_coordinates(cam_dom, x, y):
    new_x = x * math.cos(cam_dom) + y * math.sin(cam_dom)
    new_y = y * math.cos(cam_dom) - x * math.sin(cam_dom)

    return new_x, new_y


def get_cam_gps(img_path):
    """ Extract GPS info (latitude, longitude & altitude) from a given image """

    img = Image.open(img_path)
    metadata = {ExifTags.TAGS[k]: v for k,
                v in img._getexif().items() if k in ExifTags.TAGS}

    # calculate latitude & longitude for the given image
    if metadata.get('GPSInfo') is not None:
        gps = {ExifTags.GPSTAGS[k]: v for k, v in metadata['GPSInfo'].items(
        ) if k in ExifTags.GPSTAGS}
        if gps['GPSLatitude'] and gps['GPSLatitudeRef'] and gps['GPSLongitude'] and gps['GPSLongitudeRef']:
            lat = convert_to_degrees(gps.get('GPSLatitude'))
            if gps.get('GPSLatitudeRef') != 'N':
                lat = 0 - lat

            lon = convert_to_degrees(gps.get('GPSLongitude'))
            print(lon)
            if gps.get('GPSLongitudeRef') != 'E':
                lon = 0 - lon

    return lat, lon


def get_cam_dom(image_path):
    """ Return the direction of movement for the camera in degrees """
    dom = None
    img = Image.open(image_path)
    metadata = {
        ExifTags.TAGS[k]: v
        for k, v in img._getexif().items()
        if k in ExifTags.TAGS
    }

    if metadata.get('GPSInfo') is not None:
        gps = {
            ExifTags.GPSTAGS[k]: v
            for k, v in metadata['GPSInfo'].items()
            if k in ExifTags.GPSTAGS
        }

        if 'GPSTrack' in gps:
            dom, scale = gps['GPSTrack']
            dom /= scale

    return dom


def convert_to_degrees(gps):
    """ Convert the GPS coordinates stored in the EXIF to degrees in float format """

    return float(gps[0] + gps[1] / 60 + gps[2] / 3600)


def get_sign_gps(x, y, cam_lat, cam_lon, cam_dom):
    """
    :param x: The x sign offset from the image (in log meters).
    :param y: The y sign offset from the image (in log meters).
    :param cam_lat: The latitude of the camera.
    :param cam_lon: The longitude of the camera.
    :param cam_dom: The direction (in degrees) the camera is facing.
    :return: The GPS coordinates of the sign projected to the lon-lat coordinate system.
    """

    # transform the log meters to meters
    # x = np.sign(x) * 10 ** abs(x)
    # y = np.sign(y) * 10 ** abs(y)

    # rotate the meters offsets to align them with the lon, lat axis
    rot_x, rot_y = rotate_coordinates(math.pi / 180 * cam_dom, x, y)

    # convert the meters to lon, lat offsets
    R = 6378137
    lat_offset = rot_y / R
    lon_offset = rot_x / (R * math.cos(math.pi * cam_lat / 180))

    # add the offsets to find the sign lon, lat
    sign_lat = cam_lat + lat_offset * 180 / math.pi
    sign_lon = cam_lon + lon_offset * 180 / math.pi

    return sign_lat, sign_lon
