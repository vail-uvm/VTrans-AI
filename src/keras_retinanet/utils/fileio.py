"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import csv
import os

import numpy as np

from .gps_transform import *


def save_detections(save_path, file_header, generator, all_detections, img_gps, img_pth):
    """  Save predictions to a csv file """

    if not os.path.exists(save_path):
        with open(save_path, 'w') as log:
            detections = csv.writer(log, delimiter=',')
            detections.writerow(file_header)

    with open(save_path, 'a') as log:
        cam_dom = get_cam_dom(img_pth)
        detections = csv.writer(log, delimiter=',')

        if type(all_detections) is dict:  # ground-truth annotation
            for i in range(all_detections['bboxes'].shape[0]):
                sign_gps = get_sign_gps(all_detections['glocs'][i][0],
                                        all_detections['glocs'][i][1],
                                        img_gps[0],
                                        img_gps[1],
                                        cam_dom)
                detections.writerow(
                    np.concatenate((
                        [
                            str(img_pth),  # image abs-path
                            generator.label_to_name(all_detections['labels'][i])  # sign type
                        ],
                        all_detections['bboxes'][i].astype(int),  # bounding box
                        [
                            sign_gps[0],  # lat
                            sign_gps[1]   # lon
                        ],
                    ), axis=None)
                )
        else:  # prediction from the network
            for i in range(all_detections.shape[0]):
                sign_gps = get_sign_gps(all_detections[i, 4],
                                        all_detections[i, 5],
                                        img_gps[0],
                                        img_gps[1],
                                        cam_dom)
                detections.writerow(
                    np.concatenate((
                        [
                            str(img_pth),  # image abs-path
                            generator.label_to_name(all_detections[i, -1])  # sign type
                        ],
                        all_detections[i, :4].astype(int),  # bounding box
                        [
                            sign_gps[0],  # lat
                            sign_gps[1]   # lon
                        ],
                        all_detections[i, 6],  # conf. score
                        cam_dom,
                        all_detections[i, 4:6]
                    ), axis=None)
                )
