#!/usr/bin/env bash

# Run this from VTrans-AI/src


for f in data/rawdata/12/12*/; do
    python keras_retinanet/bin/predict.py --convert-model --detections_only aran "${f%/}" models/meters_offset.h5;
done


for f in data/rawdata/13/13*/; do
    python keras_retinanet/bin/predict.py --convert-model --detections_only aran "${f%/}" models/meters_offset.h5;
done


for f in data/rawdata/14/14*/; do
    python keras_retinanet/bin/predict.py --convert-model --detections_only aran "${f%/}" models/meters_offset.h5;
done

#for f in ../../ARTS-Dataset/rawdata/12/12*/; do
#    python keras_retinanet/bin/predict.py --convert-model --detections_only aran "${f%/}" models/resnet50_arts-challenging_baseline.h5;
#done

#for f in ../../ARTS-Dataset/test_map_3/13/13*/; do
#    python keras_retinanet/bin/predict.py --convert-model --detections_only aran "${f%/}" models/local_offset_best.h5;
#done

# /home/vision/Downloads/resnet50_arts_easy_baseline.h5;