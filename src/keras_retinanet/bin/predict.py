#!/usr/bin/env python

"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
import os
import sys
import cv2
import keras
import tensorflow as tf
import numpy as np
import progressbar
from pathlib import Path
assert(callable(progressbar.progressbar)), "Using wrong progressbar module, install 'progressbar2' instead."


# Allow relative imports when being executed as script.
if __name__ == "__main__" and __package__ is None:
    sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', '..'))
    import keras_retinanet.bin  # noqa: F401
    __package__ = "keras_retinanet.bin"

# Change these to absolute imports if you copy this script outside the keras_retinanet package.
from .. import models
from ..preprocessing.arts import ArtsGenerator
from ..preprocessing.aran import AranGenerator
from ..utils.keras_version import check_keras_version
from ..utils.config import read_config_file, parse_anchor_parameters
from ..utils.visualization import draw_detections, draw_annotations
from ..utils.fileio import save_detections


def get_session():
    """ Construct a modified tf session """
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)


def create_generator(args):
    """ Create generators for evaluation """
    if args.dataset_type == 'arts':
        prediction_generator = ArtsGenerator(
            args.data_path,
            ['train', 'val', 'test'],
            image_min_side=args.image_min_side,
            image_max_side=args.image_max_side,
            config=args.config
        )
    elif args.dataset_type == 'aran':
        prediction_generator = AranGenerator(
            args.data_path,
            save_lookup_list=(not args.detections_only),
            image_min_side=args.image_min_side,
            image_max_side=args.image_max_side,
            config=args.config
        )
    else:
        raise ValueError('Invalid data type received: {}'.format(args.dataset_type))

    return prediction_generator


def parse_args(args):
    """ Parse the arguments """
    parser = argparse.ArgumentParser(
        description='Prediction script for a RetinaNet network.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    subparsers = parser.add_subparsers(
        help='Arguments for specific dataset types.',
        dest='dataset_type',
    )
    subparsers.required = True

    arts_parser = subparsers.add_parser('arts')
    arts_parser.add_argument(
        'data_path',
        help='Path to dataset directory (ie. /tmp/ARTS).',
    )

    aran_parser = subparsers.add_parser('aran')
    aran_parser.add_argument(
        'data_path',
        help='Path to data directory (ie. /aran/videologs).',
    )

    parser.add_argument(
        'model',
        help='Path to RetinaNet model.',
    )
    parser.add_argument(
        '--convert-model',
        help='Convert the model to an inference model (ie. the input is a training model).',
        action='store_true',
    )
    parser.add_argument(
        '--visualization',
        help='Save images with detections (defaults to False).',
        action='store_true',
    )
    parser.add_argument(
        '--detections_only',
        help='If true, only saved files are CSV files with detections (defaults to False).',
        action='store_true',
    )
    parser.add_argument(
        '--backbone',
        help='The backbone of the model.',
        default='resnet50',
    )
    parser.add_argument(
        '--gpu',
        help='Id of the GPU to use (as reported by nvidia-smi).',
    )
    parser.add_argument(
        '--score-threshold',
        help='Threshold on score to filter detections with (defaults to 0.25).',
        default=0.25,
        type=float,
    )
    parser.add_argument(
        '--max-detections',
        help='Max Detections per image (defaults to 100).',
        default=100,
        type=int,
    )
    parser.add_argument(
        '--image-min-side',
        help='Rescale the image so the smallest side is min_side (defaults to 1080).',
        type=int,
        default=1080,
    )
    parser.add_argument(
        '--image-max-side',
        help='Rescale the image if the largest side is larger than max_side (defaults to 1920).',
        type=int,
        default=1920,
    )
    parser.add_argument(
        '--config',
        help='Path to a configuration parameters .ini file (only used with --convert-model).',
    )

    return parser.parse_args(args)


def predict(raw_image, generator, model, score_threshold=0.05, max_detections=100):
    """ Get predictions of a given image """

    image = generator.preprocess_image(raw_image.copy())
    image, scale = generator.resize_image(image)

    if keras.backend.image_data_format() == 'channels_first':
        image = image.transpose((2, 0, 1))

    # run network
    boxes, scores, labels, distances = model.predict_on_batch(np.expand_dims(image, axis=0))

    # correct boxes for image scale
    boxes /= scale

    # select indices which have a score above the threshold
    indices = np.where(scores[0, :] > score_threshold)[0]

    # select those scores
    scores = scores[0][indices]

    # find the order with which to sort the scores
    scores_sort = np.argsort(-scores)[:max_detections]

    # select detections
    image_boxes = boxes[0, indices[scores_sort], :]
    image_scores = scores[scores_sort]
    image_labels = labels[0, indices[scores_sort]]
    image_distances = distances[0, indices[scores_sort], :]

    return np.concatenate(
        [
            image_boxes,
            image_distances,
            np.expand_dims(image_scores, axis=1),
            np.expand_dims(image_labels, axis=1),
        ], axis=1
    )


def main(args=None):
    # parse arguments
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)

    # make sure keras is the minimum required version
    check_keras_version()

    # optionally choose specific GPU
    if args.gpu:
        os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    keras.backend.tensorflow_backend.set_session(get_session())

    save_path = Path(args.data_path) / 'detections'
    if not args.detections_only:
        save_path.mkdir(exist_ok=True, parents=True)

    # optionally load config parameters
    if args.config:
        args.config = read_config_file(args.config)

    # create the generator
    generator = create_generator(args)

    # optionally load anchor parameters
    anchor_params = None
    if args.config and 'anchor_parameters' in args.config:
        anchor_params = parse_anchor_parameters(args.config)

    # load the model
    print('Loading model, this may take a second...')
    model = models.load_model(args.model, backbone_name=args.backbone)

    # optionally convert the model
    if args.convert_model:
        model = models.convert_model(model, anchor_params=anchor_params)

    print(model.summary())

    # create csv file paths for predictions, ground-truth annotations
    header = ['image', 'class', 'xmin', 'ymin', 'xmax', 'ymax', 'lat', 'lon', 'score', 'DOM', 'X Offset', 'Y Offset']
    if args.detections_only:
        detection_fpath = str(save_path.parent.parent / f'{Path(args.data_path).stem}_detections.csv')
        if os.path.exists(detection_fpath):
            os.remove(detection_fpath)
    else:
        detection_fpath = str(save_path.parent / f'{Path(args.data_path).stem}_detections.csv')
    annotations_fpath = str(save_path.parent / f'{Path(args.data_path).stem}_annotations.csv')

    for i in progressbar.progressbar(range(generator.size()), prefix='Running network: '):
        raw_image = generator.load_image(i)
        cam_gps = generator.image_gps(i)
        img_path = generator.image_path(i)

        image_predictions = predict(
            raw_image=raw_image,
            generator=generator,
            model=model,
            score_threshold=args.score_threshold,
            max_detections=args.max_detections
        )

        save_detections(detection_fpath, header, generator, image_predictions, cam_gps, img_path)

        if args.visualization and not args.detections_only:
            draw_detections(
                raw_image,
                image_predictions[:, :4].astype(int),     # box
                image_predictions[:, 4:6].astype(float),  # dist
                image_predictions[:, 6].astype(float),    # score
                image_predictions[:, 7].astype(int),      # label
                label_to_name=generator.label_to_name,
                score_threshold=args.score_threshold
            )

            # load ground-truth data when available
            if args.dataset_type == 'arts':
                image_annotations = generator.load_annotations(i)

                draw_annotations(
                    raw_image,
                    image_annotations,
                    label_to_name=generator.label_to_name
                )

                save_detections(annotations_fpath, header[:-1], generator, image_annotations, cam_gps, img_path)

            # save image with detections
            img_name = Path(img_path).stem
            img_name = img_name.replace('/', '_')
            cv2.imwrite(str(save_path / '{}.png'.format(img_name)), raw_image)


if __name__ == '__main__':
    main()
