#!/usr/bin/env python

"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import argparse
import os
import sys
import csv
import keras
import tensorflow as tf

# Allow relative imports when being executed as script.
if __name__ == "__main__" and __package__ is None:
    sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', '..'))
    import keras_retinanet.bin  # noqa: F401
    __package__ = "keras_retinanet.bin"

# Change these to absolute imports if you copy this script outside the keras_retinanet package.
from .. import models
from ..preprocessing.arts import ArtsGenerator
from ..utils.config import read_config_file, parse_anchor_parameters
from ..utils.eval import evaluate
from ..utils.keras_version import check_keras_version


def get_session():
    """ Construct a modified tf session """
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)


def create_generator(args):
    """ Create generators for evaluation """
    if args.dataset_type == 'arts':
        validation_generator = ArtsGenerator(
            args.arts_path,
            'test',
            image_min_side=args.image_min_side,
            image_max_side=args.image_max_side,
            config=args.config
        )
    else:
        raise ValueError('Invalid data type received: {}'.format(args.dataset_type))

    return validation_generator


def parse_args(args):
    """ Parse the arguments """
    parser     = argparse.ArgumentParser(description='MultiEvaluation script for a RetinaNet network.')
    subparsers = parser.add_subparsers(help='Arguments for specific dataset types.', dest='dataset_type')
    subparsers.required = True

    arts_parser = subparsers.add_parser('arts')
    arts_parser.add_argument('arts_path', help='Path to dataset directory (ie. /tmp/ARTS).')

    parser.add_argument('models',             help='Path to RetinaNet models folder.')
    parser.add_argument('--convert-model',    help='Convert the model to an inference model (ie. the input is a training model).', action='store_true')
    parser.add_argument('--backbone',         help='The backbone of the model.', default='resnet50')
    parser.add_argument('--gpu',              help='Id of the GPU to use (as reported by nvidia-smi).')
    parser.add_argument('--score-threshold',  help='Threshold on score to filter detections with (defaults to 0.05).', default=0.05, type=float)
    parser.add_argument('--iou-threshold',    help='IoU Threshold to count for a positive detection (defaults to 0.5).', default=0.5, type=float)
    parser.add_argument('--max-detections',   help='Max Detections per image (defaults to 100).', default=100, type=int)
    parser.add_argument('--image-min-side',   help='Rescale the image so the smallest side is min_side (defaults to 1080).', type=int, default=1080)
    parser.add_argument('--image-max-side',   help='Rescale the image if the largest side is larger than max_side (defaults to 1920).', type=int, default=1920)
    parser.add_argument('--config',           help='Path to a configuration parameters .ini file (only used with --convert-model).')

    return parser.parse_args(args)


def main(args=None):
    # parse arguments
    if args is None:
        args = sys.argv[1:]
    args = parse_args(args)

    # make sure keras is the minimum required version
    check_keras_version()

    # optionally choose specific GPU
    if args.gpu:
        os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    keras.backend.tensorflow_backend.set_session(get_session())

    # optionally load config parameters
    if args.config:
        args.config = read_config_file(args.config)

    # create the generator
    generator = create_generator(args)

    # optionally load anchor parameters
    anchor_params = None
    if args.config and 'anchor_parameters' in args.config:
        anchor_params = parse_anchor_parameters(args.config)

    for root, dirs, files in os.walk(args.models, topdown=True):
        for fname in sorted(files):
            pth = os.path.join(root, fname)
            if pth.endswith('.h5'):
                print('\nEvaluating: ', pth)

                # load the model
                model = models.load_model(pth, backbone_name=args.backbone)

                # optionally convert the model
                if args.convert_model:
                    model = models.convert_model(model, anchor_params=anchor_params)

                # start evaluation
                average_precisions = evaluate(
                    generator,
                    model,
                    iou_threshold=args.iou_threshold,
                    score_threshold=args.score_threshold,
                    max_detections=args.max_detections
                )

                pth_to_save = os.path.join(
                    args.models, 
                    '{backbone}_{dataset_type}_AP{iou}.csv'.format(
                        basename=os.path.basename(args.models), 
                        backbone=args.backbone, 
                        dataset_type=args.dataset_type,
                        iou=args.iou_threshold
                    )
                )

                mAPs = {generator.label_to_name(k): p for k, (p,n) in average_precisions.items()}

                if not os.path.exists(pth_to_save):
                    with open(pth_to_save, 'w') as csvfile:
                        dict_writer = csv.DictWriter(csvfile, fieldnames=mAPs.keys())
                        dict_writer.writeheader()
                        dict_writer.writerow(mAPs)
                else:
                    with open(pth_to_save, 'a') as csvfile:
                        dict_writer = csv.DictWriter(csvfile, fieldnames=mAPs.keys())
                        dict_writer.writerow(mAPs)
                
                # clear memory
                keras.backend.clear_session()
                 

if __name__ == '__main__':
    main()
