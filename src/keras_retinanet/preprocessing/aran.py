"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from ..preprocessing.generator import Generator
from ..utils.image import read_image_bgr

import os
import csv
from PIL import Image, ExifTags


class AranGenerator(Generator):
    """ Generate data for a ARAN raw data """

    def __init__(
        self,
        data_dir,
        save_lookup_list=True,
        image_extension='.jpg',
        **kwargs
    ):
        """ Initialize a ARAN data generator.

        Args
            base_dir: Directory w.r.t. where the files are to be searched (defaults to the directory containing the csv_data_file).
            csv_class_file: Path to the CSV classes file.
        """
        self.data_dir             = data_dir
        self.save_lookup_list     = save_lookup_list
        self.image_extension      = image_extension

        if os.path.exists(os.path.join(data_dir, 'lookup.txt')):
            self.image_names = [l.strip().split(None, 1)[0] for l in
                                open(os.path.join(self.data_dir, 'lookup.txt')).readlines()]
        else:
            self.image_names = self.create_lookup_list()

        self.classes = {}
        self.labels = {}
        labels_path = os.path.join(os.path.dirname(os.path.dirname(data_dir)), 'labels.txt')
        with open(labels_path, mode='r') as log:
            reader = csv.reader(log, delimiter=' ')
            for i, k in enumerate(reader):
                self.classes[k[0]] = int(i)
                self.labels[int(i)] = k[0]

        super(AranGenerator, self).__init__(**kwargs)

    def size(self):
        """ Size of the dataset """
        return len(self.image_names)

    def num_classes(self):
        """ Number of classes in the dataset """
        return len(self.classes)

    def has_label(self, label):
        """ Return True if label is a known label """
        return label in self.labels

    def has_name(self, name):
        """ Returns True if name is a known class """
        return name in self.classes

    def name_to_label(self, name):
        """ Map name to label """
        return self.classes[name]

    def label_to_name(self, label):
        """ Map label to name """
        return self.labels[label]

    def image_path(self, image_index):
        """ Return path for a given image """
        return self.image_names[image_index]

    def image_gps(self, image_index):
        """ Return gps coordinates for the camera """

        def _convert_to_degress(gps):
            """ Convert the GPS coordinates stored in the EXIF to degress in float format """
            d = float(gps[0][0]) / float(gps[0][1])
            m = float(gps[1][0]) / float(gps[1][1])
            s = float(gps[2][0]) / float(gps[2][1])
            return d + (m / 60.0) + (s / 3600.0)

        img = Image.open(self.image_path(image_index))

        metadata = {ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS}

        # calculate latitude & longitude for the given image
        if metadata.get('GPSInfo') is not None:
            gps = {ExifTags.GPSTAGS[k]: v for k, v in metadata['GPSInfo'].items() if k in ExifTags.GPSTAGS}

            if gps['GPSLatitude'] and gps['GPSLatitudeRef'] and gps['GPSLongitude'] and gps['GPSLongitudeRef']:
                lat = _convert_to_degress(gps.get('GPSLatitude'))
                if gps.get('GPSLatitudeRef') != 'N':
                    lat = 0 - lat

                lon = _convert_to_degress(gps.get('GPSLongitude'))
                if gps.get('GPSLongitudeRef') != 'E':
                    lon = 0 - lon

                alt = gps.get('GPSAltitude')[0] / gps.get('GPSAltitude')[1]
                if ord(gps.get('GPSAltitudeRef')) != 0:
                    alt = 0 - alt

        return lat, lon, alt

    def image_aspect_ratio(self, image_index):
        """ Compute the aspect ratio for an image with image_index """
        image = Image.open(self.image_path(image_index))
        return float(image.width) / float(image.height)

    def load_image(self, image_index):
        """ Load an image at the image_index """
        return read_image_bgr(self.image_path(image_index))

    def create_lookup_list(self):
        """ Lookup all images in the given dir and store them in a text file """
        lookup_list = []
        for root, folders, files in os.walk(self.data_dir):
            for f in sorted(files):
                if f.endswith(self.image_extension):
                    lookup_list.append(str(os.path.join(root, f)))

        # store lookup list
        if self.save_lookup_list:
            with open(os.path.join(self.data_dir, 'lookup.txt'), 'w') as f:
                for img in lookup_list:
                    f.write('{}\n'.format(img))

        return lookup_list

