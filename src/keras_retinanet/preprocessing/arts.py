"""
Copyright 2018-2019 VaiL

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from ..preprocessing.generator import Generator
from ..utils.image import read_image_bgr

import os
import csv
import math
import numpy as np
from six import raise_from
from PIL import Image
from lxml import etree as ET


def _findNode(parent, name, debug_name=None, parse=None):
    if debug_name is None:
        debug_name = name

    result = parent.find(name)
    if result is None:
        raise ValueError('missing element \'{}\''.format(debug_name))
    if parse is not None:
        try:
            return parse(result.text)
        except ValueError as e:
            raise_from(ValueError('illegal value for \'{}\': {}'.format(debug_name, e)), None)
    return result


class ArtsGenerator(Generator):
    """ Generate data for a ARTS dataset """
    def __init__(
        self,
        data_dir,
        set_name,
        use_attributes_as_class=False,
        image_extension='.jpg',
        skip_truncated=False,
        skip_difficult=False,
        **kwargs
    ):
        """ Initialize a ARTS data generator.

        Args
            base_dir: Directory w.r.t. where the files are to be searched (defaults to the directory containing the csv_data_file).
            csv_class_file: Path to the CSV classes file.
        """
        self.data_dir             = data_dir
        self.set_name             = set_name
        self.use_attributes_as_class = use_attributes_as_class
        self.image_extension      = image_extension
        self.skip_truncated       = skip_truncated
        self.skip_difficult       = skip_difficult

        if type(set_name) is list:
            self.image_names = []
            for s in set_name:
                for file in open(os.path.join(data_dir, 'ImageSets', 'Layout', s + '.txt')).readlines():
                    if file not in self.image_names:
                        self.image_names.append(file.strip().split(None, 1)[0])
        else:
            self.image_names = [l.strip().split(None, 1)[0] for l in
                                open(os.path.join(data_dir, 'ImageSets', 'Layout', set_name + '.txt')).readlines()]

        self.classes = {}
        self.labels = {}
        labels_path = os.path.join(data_dir, 'ImageSets', 'Layout', 'labels.txt')
        with open(labels_path, mode='r') as log:
            reader = csv.reader(log, delimiter=' ')
            for i,k in enumerate(reader):
                self.classes[k[0]] = int(i)
                self.labels[int(i)] = k[0]

        # temporarily overwrite classes and labels
        if use_attributes_as_class:
            self.classes = {'Not Assembly': 0, 'Assembly': 1}
            self.labels = {0: 'Not Assembly', 1: 'Assembly'}

        super(ArtsGenerator, self).__init__(**kwargs)

    def size(self):
        """ Size of the dataset """
        return len(self.image_names)

    def num_classes(self):
        """ Number of classes in the dataset """
        return len(self.classes)

    def has_label(self, label):
        """ Return True if label is a known label """
        return label in self.labels

    def has_name(self, name):
        """ Returns True if name is a known class """
        return name in self.classes

    def name_to_label(self, name):
        """ Map name to label """
        return self.classes[name]

    def label_to_name(self, label):
        """ Map label to name """
        return self.labels[label]

    def image_path(self, image_index):
        """ Return path for a given image """
        return os.path.join(self.data_dir, 'JPEGImages', self.image_names[image_index] + self.image_extension)

    def xml_path(self, image_index):
        """ Return path for a given xml file """
        return os.path.join(self.data_dir, 'Annotations', self.image_names[image_index] + '.xml')

    def image_gps(self, image_index):
        """ Return gps coordinates for the camera """
        filename = self.image_names[image_index] + '.xml'

        try:
            tree = ET.parse(os.path.join(self.data_dir, 'Annotations', filename))
            loc = _findNode(tree.getroot(), 'Location')
            lat = _findNode(loc, 'Latitude', 'Location.Latitude', parse=float)
            lon = _findNode(loc, 'Longitude', 'Location.Longitude', parse=float)
            alt = _findNode(loc, 'Altitude', 'Location.Altitude', parse=float)
            return lat, lon, alt

        except ET.ParseError as e:
            raise_from(ValueError('invalid annotations file: {}: {}'.format(filename, e)), None)
        except ValueError as e:
            raise_from(ValueError('invalid annotations file: {}: {}'.format(filename, e)), None)

    def image_aspect_ratio(self, image_index):
        """ Compute the aspect ratio for an image with image_index """
        path = self.image_path(image_index)
        image = Image.open(path)
        return float(image.width) / float(image.height)

    def load_image(self, image_index):
        """ Load an image at the image_index """
        path = self.image_path(image_index)
        return read_image_bgr(path)

    def __parse_annotation(self, element):
        """ Parse an annotation given an XML element """
        truncated = _findNode(element, 'truncated', parse=int)
        difficult = _findNode(element, 'difficult', parse=int)

        if self.use_attributes_as_class:
            attributes = _findNode(element, 'attributes')
            assembly = _findNode(attributes, 'assembly').text
            label = int(assembly == "True")
        else:
            class_name = _findNode(element, 'name').text
            if class_name not in self.classes:
                raise ValueError('class name \'{}\' not found in classes: {}'.format(class_name, list(self.classes.keys())))
            label = self.name_to_label(class_name)

        box = np.zeros((4,))
        bndbox = _findNode(element, 'bndbox')
        box[0] = _findNode(bndbox, 'xmin', 'bndbox.xmin', parse=float) - 1
        box[1] = _findNode(bndbox, 'ymin', 'bndbox.ymin', parse=float) - 1
        box[2] = _findNode(bndbox, 'xmax', 'bndbox.xmax', parse=float) - 1
        box[3] = _findNode(bndbox, 'ymax', 'bndbox.ymax', parse=float) - 1

        # gps coordinates for the camera
        cam_loc = _findNode(element.getparent(), 'Location')
        cam_lat = _findNode(cam_loc, 'Latitude', 'Location.Latitude', parse=float)
        cam_lon = _findNode(cam_loc, 'Longitude', 'Location.Longitude', parse=float)
        cam_dom = _findNode(cam_loc, 'Cam_DOM', 'Location.Cam_DOM', parse=float)

        # gps coordinates for the object
        obj_loc = _findNode(element, 'location')
        obj_lat = _findNode(obj_loc, 'latitude', 'location.latitude', parse=float)
        obj_lon = _findNode(obj_loc, 'longitude', 'location.longitude', parse=float)

        geoloc = np.zeros((2,))
        x, y = self.get_x_y(obj_lat, obj_lon, cam_lat, cam_lon, cam_dom)
        geoloc[0] = x
        geoloc[1] = y

        return truncated, difficult, box, geoloc, label

    def __parse_annotations(self, xml_root):
        """ Parse all annotations under the xml_root """
        annotations = {
            'labels': np.empty((len(xml_root.findall('object')),)),
            'bboxes': np.empty((len(xml_root.findall('object')), 4)),
            'glocs': np.empty((len(xml_root.findall('object')), 2))
        }

        for i, element in enumerate(xml_root.iter('object')):
            try:
                truncated, difficult, box, geoloc, label = self.__parse_annotation(element)
            except ValueError as e:
                raise_from(ValueError('could not parse object #{}: {}'.format(i, e)), None)

            if truncated and self.skip_truncated:
                continue
            if difficult and self.skip_difficult:
                continue

            annotations['bboxes'][i, :] = box
            annotations['glocs'][i, :]  = geoloc
            annotations['labels'][i]    = label

        return annotations

    def load_annotations(self, image_index):
        """ Load annotations for an image_index """
        filename = self.image_names[image_index] + '.xml'
        try:
            tree = ET.parse(os.path.join(self.data_dir, 'Annotations', filename))
            return self.__parse_annotations(tree.getroot())
        except ET.ParseError as e:
            raise_from(ValueError('invalid annotations file: {}: {}'.format(filename, e)), None)
        except ValueError as e:
            raise_from(ValueError('invalid annotations file: {}: {}'.format(filename, e)), None)

    def rotate_coordinates(self, cam_dom, x, y):

        new_x = x * math.cos(cam_dom) + y * math.sin(cam_dom)
        new_y = y * math.cos(cam_dom) - x * math.sin(cam_dom)

        return new_x, new_y

    def get_x_y(self, sign_lat, sign_lon, cam_lat, cam_lon, cam_dom):
        """
        :param sign_lat: The latitude of the sign.
        :param sign_lon: The longitude of the sign.
        :param cam_lat: The latitude of the camera.
        :param cam_lon: The longitude of the camera.
        :param cam_dom: The direction (in degrees) the camera is facing.
        :return: The x, y sign offset from the image (in meters).
        """

        # convert to lat, lon offsets
        dLat = (sign_lat - cam_lat) * math.pi / 180
        dLon = (sign_lon - cam_lon) * math.pi / 180

        # convert the lat, lon offsets to meters
        R = 6378137
        y = dLat * R
        x = dLon * (R * math.cos(math.pi * cam_lat / 180))

        # rotate the meters offsets to align with the local image coordinate system
        rot = -1 * (math.pi / 180 * cam_dom)
        img_x, img_y = self.rotate_coordinates(rot, x, y)

        return img_x, img_y
