# GPS-RetinaNet 

Keras implementation of GPS-RetinaNet and FLe.


## Installation

Clone this repository, then navigate to `/src` and run `python setup.py build_ext --inplace`


## Training

`GPS-retinanet` can be trained using [train.py](src/keras_retinanet/bin/train.py) script. The default backbone is `resnet50`. You can change this using the `--backbone=xxx` argument in the running script.
`xxx` can be one of the backbones in resnet models (`resnet50`, `resnet101`, `resnet152`), mobilenet models (`mobilenet128_1.0`, `mobilenet128_0.75`, `mobilenet160_1.0`, etc), densenet models or vgg models. The different options are defined by each model in their corresponding python scripts (`resnet.py`, `mobilenet.py`, etc).

For more information run
```shell
python keras_retinanet/bin/train.py -h
```

Default configuration: 
```python
model.compile(
    loss={
        'classification': losses.focal_e(),
            'regression': losses.smooth_l1(),
                   'gps': losses.smooth_l1(),
    },
    optimizer=keras.optimizers.adam(lr=1e-4, clipnorm=0.001)
)
```

Default Callbacks:
```python
keras.callbacks.TensorBoard(
    log_dir                = args.tensorboard_dir,
    histogram_freq         = 0,
    batch_size             = args.batch_size,
    write_graph            = True,
    write_grads            = False,
    write_images           = False,
    embeddings_freq        = 0,
    embeddings_layer_names = None,
    embeddings_metadata    = None
)
```

```python
keras.callbacks.ModelCheckpoint(
    os.path.join(
        args.snapshot_path,
        '{backbone}_{dataset_type}_{{epoch:02d}}.h5'.format(
            backbone=args.backbone, dataset_type=args.dataset_type
        )
    ),
    verbose=1,
    save_best_only=True,
    monitor="mAP",
    mode='max'
)
```

```python
keras.callbacks.ReduceLROnPlateau(
    monitor  = 'loss',
    factor   = 0.1,
    patience = 5,
    verbose  = 1,
    mode     = 'auto',
    epsilon  = 0.0001,
    cooldown = 0,
    min_lr   = 0
)
```


## Evaluation

You can use [evaluate.py](src/keras_retinanet/bin/evaluate.py) to evaluate one model 
or [multi_eval.py](src/keras_retinanet/bin/multi_eval.py) to evaluate several models at once 
and save the results to a csv file. 

For more information run
```shell
python keras_retinanet/bin/eval.py -h
```


## Prediction

If you have a trained model you can run it using [predict.py](src/keras_retinanet/bin/predict.py) script to get 
predictions for a given folder. The script saves the network predictions to a CSV file `#detections.csv`. 
This script supports the ARTS dataset format and ARAN data (raw video-logs). 

For more information run
```shell
python keras_retinanet/bin/predict.py -h
```


## Tracking

Currently, object tracking is a postprocessing step. 
You can run the tracker using [track.py](src/tracker/track.py) script. 
The script saves the tracker output to a CSV file `#signs.csv`. 

For more information run
```shell
python tracker/track.py -h
```

Please cite 

  O. Tange (2011): GNU Parallel - The Command-Line Power Tool,
  ;login: The USENIX Magazine, February 2011:42-47.
  
in any academic works that utilize the object tracking system.